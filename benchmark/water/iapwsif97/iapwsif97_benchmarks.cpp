#include <benchmark/benchmark.h>
#include <iostream>
#include <types/types.hpp>
#include <water/iapwsif97/basic_equations/supercritical.hpp>
#include <water/iapwsif97/iapwsif97.hpp>

namespace fluid_props::benchmarking::water
{

    static void SuperCriticalRegionLazyEvaluationStateSet(benchmark::State &state)
    {
        for (auto _ : state) // NOLINT(clang-analyzer-deadcode.DeadStores)
        {
            auto superCritical =
                fluid_props::water::BasicSuperCritical{PressureT<MegaPascal>{25.5837018}, TemperatureT<Kelvin>{650.0}};
        }
    }

    static void SuperCriticalRegionLazyEvaluationPropertyCalculation(benchmark::State &state)
    {
        auto superCritical =
            fluid_props::water::BasicSuperCritical{PressureT<MegaPascal>{25.5837018}, TemperatureT<Kelvin>{650.0}};
        for (auto _ : state) // NOLINT(clang-analyzer-deadcode.DeadStores)
        {
            [[maybe_unused]] auto enthalpy = superCritical.Enthalpy();
        }
    }

    static void IAPWSIF97LazyEvaluation(benchmark::State &state)
    {
        auto water = fluid_props::water::IAPWSIF97{PressureT<MegaPascal>{3.0}, TemperatureT<Kelvin>{300.0}};

        // Evaluate, will do long calculation
        [[maybe_unused]] auto enthalpy = water.Enthalpy();

        // Should no longer evaluate only return previously calculated value
        for (auto _ : state) // NOLINT(clang-analyzer-deadcode.DeadStores)
        {
            enthalpy = water.Enthalpy();
        }
    }
    static void IAPWSIF97NonLazyEvaluation(benchmark::State &state)
    {

        auto water = fluid_props::water::IAPWSIF97{PressureT<MegaPascal>{3.0}, TemperatureT<Kelvin>{300.0}};
        // Evaluate, every cycle with long calculation
        for (auto _ : state) // NOLINT(clang-analyzer-deadcode.DeadStores)
        {
            water.SetState(PressureT<MegaPascal>{3.0}, TemperatureT<Kelvin>{300.0});
            [[maybe_unused]] auto enthalpy = water.Enthalpy();
        }
    }

    BENCHMARK(SuperCriticalRegionLazyEvaluationStateSet)->Unit(benchmark::kNanosecond);
    BENCHMARK(SuperCriticalRegionLazyEvaluationPropertyCalculation);
    BENCHMARK(IAPWSIF97LazyEvaluation)->Unit(benchmark::kNanosecond);
    BENCHMARK(IAPWSIF97NonLazyEvaluation);

} // namespace fluid_props::benchmarking::water
