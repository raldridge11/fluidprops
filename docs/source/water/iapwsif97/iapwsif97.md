# IAPWSIF97

This is the implementation of the International Association of the Properties of Water and Steam of 1997,
the industrial formulation (IAPWSIF97 for short).
The IAPWS considers this formulation to be appropriate for use in engineering applications where speed of calculation is paramount.
IAPWSIF97 consists of a set of equations for different regions which cover the following range of validity:

| Temperature Range                | Pressure Range         |
| ---------------------------------| ---------------------- |
| $ 273.15 \text{ K} \le T \le 1073.15 \text{ K} $ | $ 0 \lt p \lt 100 \text{ MPa} $|
| $ 1073.15\text{ K} \le T \le 2273.15 \text{ K} $ | $ 0 \lt p \lt 50 \text{ MPa} $ |

{numref}`iapwsif97_map` gives the overall regional specification graphically.

```{figure} ./img/region_map.png
---
scale: 100
name: iapwsif97_map
---
Regions of the IAPWSIF97 formulation.
```

For `FluidProps++` we give more descriptive naming to the regions provided by the IAPWSIF97, seen in {numref}`fluidprops_mapping`.

```{table} IAPWSIF97 Region to FluidProps++ naming
---
widths: grid
align: center
name: fluidprops_mapping
---
| IAPWSIF97 Region | `FluidProps++`   |
| ---------------- | ---------------- |
| 1                | Liquid           |
| 2                | Vapor            |
| 3                | Supercritical    |
| 4                | Saturation       |
| 5                | High Temperature |
```

Each phase of the IAPWSIF97 is described by a basic equation.
For every phase but the supercritical region,
the basic equations is the specific Gibbs free energy as a function of pressure and temperature.
For the supercritical region,
the basic equation is the specific Helmholtz free energy as a function of density and temperature.
The specific properties (e.g. enthalpy) for a region is described by a combination of partial derivatives with respect to various state variables.
For a more complete description see [`IAPWSFIF97`](../../generated/class/classfluid__props_1_1water_1_1IAPWSIF97.rst) and the standard description {cite}`2008:Wagner`.

For states not expressed by the basic equations (e.g. state from pressure and enthalpy),
the IAPWSIF97 provides so called backwards equations which allows for calculation of temperature from the state.
As far as the implementation in `FluidProps++`,
the backwards equations are used to get the temperature which then sets the state of the fluid appropriately to use the basic equation formulation.

Transport properties are calculated given by the standard {cite}`2008:Wagner`.

For a more complete overview and understanding of the whole standard please see {cite}`2008:Wagner`.

## Notes on Supercritical Region Usage

```{admonition} Performance
---
class: important
---
Beware of poorer performance if using Supercritical properties!
```

The performance of property calculation in the Supercritical region is worse than the other phases.
This is due to the basic equation being a function of density and temperature.
As a result,
in order to obtain the density from pressure and temperature,
an iterative method is performed to calculate the density from pressure and temperature using the appropriate derivatives of the Helmholtz free energy equation.
The secant method is used to find the roots of the equation,
but has fixed initial guesses for the density.
Depending on where in the Supercritical region the state is located,
it may take a long time to converge to a solution.
The number of iterations is capped to a maximum of 10 million iterations,
so there is a chance the solution may not actually converge.

```{admonition} Convergence
---
class: important
---
The number of iterations is capped to a maximum of 10 million iterations, so there is a chance the solution may not actually converge!
```

The IAPWSIF97 does provide a backwards formulation to calculated density from pressure and temperature.
However,
a decision was made to not implement the formulation as it is a convoluted set of 26 equations along with 26 equations describing the boundaries between each region.
Future releases *may* provide an implementation.

## Unit Systems

### `IAPWSIF97` class

The basic IAPWSIF97 (`IAPWSIF97`) is implemented in the unit system of the standard, basic SI where pressure is in MPa and temperature in Kelvin.

For example,

```c++
#include <water/iapwsif97/iapwsif97.hpp>

using namespace fluid_props = fp;

// Going to create a water object at the P-T point of 3.0 MPa and 500.0 K.
auto water = fp::water::IAPWSIF97(fp::PressureT<MegaPascal>{3.0}, fp::TemperaturT<Kelvin>{500.0});

// Get enthalpy at the state of 3.0 MPa and 500.0 K
auto enthalpy = water.Enthalpy(); // Will return 975.542239 kJ/kg
// all other properties to get will be at this state

// Let's get the saturation temperature at 1.0 MPa
water.Pressure(fp::PressureT<MegaPascal>{1.0});

auto saturation_temperature = water.Saturation()->Temperature(); // Will return 453.035632 K

```

Base units for dimension are give in {numref}`iapwsif97_units`

```{table} IAPWSIF97 Base Units
---
widths: grid
align: center
name: iapwsif97_units
---
| Property | Unit |
| ---------| ---- |
| Pressure | MPa |
| Temperature | K |
| Mass | kg |
| Length | m |
| Energy | kJ |
```

## Metastable-Vapor Region

Specific properties are given for the metastable-vapor region of water in which the basic equation provided by the ordinary vapor region is not sufficient {cite}`2008:Wagner`.
Metastable-vapor occurs in unique situation where a liquid is superheated above saturation,such as in cases where no nucleation sites for bubble formation are present,
and the liquid's enthalpy is much higher than the saturation enthalpy of liquid at equilibrium conditions.

The metastable-vapor region is not included in the `IAPWSIF97` implementation as it is difficult to predict applicability based on the fluid state alone (e.g. from pressure and temperature).
Take note of the `M` designation in the class name denoting that these are metastable properties.
`IAPWSIF97M` has the same interface as a `Fluid`, with an additional method `InValidRange` that checks if the fluid state is in the valid range of applicability for the state equations.
The valid range for the metastable-vapor region is pressures from the triple-point pressure up to 10 MPa, and between the saturated-vapor line to the 5% equilibrium moisture line
(corresponding to a static quality of 0.95).
It is left to the consumer to correctly determine whether or not the metastable-vapor region is appropriate for application.

### Performance Considerations

You can expect that `IAPWSIF97M` will have similar performance as `IAPWSIF97` when state is given as pressure and temperature.
However, be aware that if state is given with any other combinations (e.g. pressure and enthalpy), iterative methods are used to find a solution of the state in terms of pressure and temperature.

```{admonition} Performance
---
class: important
---
Beware of iterative method used for given provided outside of pressure and temperature!
```

## Bibliography

```{bibliography}
---
style: unsrt
cited: true
---
```
