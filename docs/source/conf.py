import pathlib
import subprocess as sp
import time

config_directory = pathlib.Path(__file__).parents[0]
generated_doxygen = config_directory / "doxygen" / "xml"

author = "Aldridge Software Designs"
project = "FluidProps++"
copyright = f"{time.localtime()[0]}, {author}"

extensions = [
    "breathe",
    "myst_parser",
    "sphinx_design",
    "sphinxcontrib.mermaid",
    "sphinxcontrib.bibtex",
    "sphinx.ext.autosectionlabel",
]

highlight_language = 'c++'

html_theme = 'sphinx_book_theme'
html_title = "FluidProps++"
html_logo = str(config_directory / ".." / ".." / "pages" / "drop.svg")
html_favicon = html_logo
html_theme_options = {"home_page_in_toc": True}

breathe_projects = {project: str(generated_doxygen)}
breathe_default_project = project
breathe_default_members = ('members', 'undoc-members')

myst_enable_extensions = ["dollarmath", "amsmath"]
myst_fence_as_directive = ["mermaid"]
myst_heading_anchors = 3

numfig = True

bibtex_bibfiles = [
    str(config_directory / "refs.bib"),
]

sp.call("doxygen Doxyfile.in", shell=True)
sp.call(
    "breathe-apidoc doxygen/xml -o generated -f -m --generate class,interface,struct,union,file",
    shell=True)
