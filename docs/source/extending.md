# Extending and Adding New Fluids

`FluidProps++` is intended to be extended and adding additional fluids is as simple as defining the unit system and implementing the `Fluid` interface.
The `Fluid` interface is a basic fluid (think of it as a single phase) fluid,
that has a state.
For a two phase fluid, the interface `TwoPhaseFluid` is available which subclasses the `Fluid` interface and also has a member to the `Saturated` interface which would need to be implemented.
Of course,
the interface defines the minimum implementation.
Any other properties available for your fluid can and should be added.
Please see the API documentation for documentation of the [`Fluid` interface](./generated/class/classfluid__props_1_1Fluid.rst),
[`TwoPhaseFluid` interface](./generated/class/classfluid__props_1_1TwoPhaseFluid.rst),
and [`Saturated` interface](./generated/class/classfluid__props_1_1Saturated.rst).
Take note that if the material needs to take advantage of lazy evaluation,
it is the job of the implemented class to handle it.

## Defining the Unit System

The `fluid` interface is a templated class where the template parameter is a templated struct that contains type definitions for the various thermodynamic properties.


The templated struct is given below,

```c++
template <
        typename PressureUnit,
        typename TemperatureUnit,
        typename EnthalpyUnit,
        typename EntropyUnit,
        typename SpecificVolumeUnit,
        typename DensityUnit,
        typename VelocityUnit,
        typename ThermalExpansionUnit,
        typename CompressibilityUnit,
        typename ViscosityUnit,
        typename ThermalConductivityUnit>
    struct FluidUnits
    {
        using Pressure = PressureUnit;
        using Temperature = TemperatureUnit;
        using Enthalpy = EnthalpyUnit;
        using Entropy = EntropyUnit;
        using SpecificVolume = SpecificVolumeUnit;
        using Density = DensityUnit;
        using Velocity = VelocityUnit;
        using ThermalExpansion = ThermalExpansionUnit;
        using Compressibility = CompressibilityUnit;
        using Viscosity = ViscosityUnit;
        using ThermalConductivity = ThermalConductivityUnit;
    };
```

There is a subsequent templated struct for `TwoPhaseFluid` that includes definitions needed for quality and void.

For example,
the `IAPWS` implementation of water properties has the following type alias to define the unit system to be used for `IAPWS` fluids,

```c++
#pragma once
#include "interfaces/fluid.hpp"
#include "units/units.hpp"

namespace fluid_props::water
{
    /**
     * @brief Units used for IAPWS standard.
     *
     */
    using IAPWSUnits = TwoPhaseFluidUnits<
        MegaPascal,
        Kelvin,
        KiloJoule_KiloGram,
        KiloJoule_KiloGram_Kelvin,
        CubicMeter_KiloGram,
        KiloGram_CubicMeter,
        Meter_Second,
        IKelvin,
        IMegaPascal,
        PascalSecond,
        Watts_Meter_Kelvin,
        Dimensionless,
        Dimensionless>;
} // namespace fluid_props::water
```

The unit system defines the internal unit system used by the fluid along with the return types and units of the calculated thermodynamic property.
If the `PressureUnit` is defined as `MegaPascal`,
internally to the fluid pressure will be in `MegaPascal`,
and when calling the `Pressure` getter,
the return value will be a `PressureT<MegaPascal>`.
Any pressure unit type can be supplied to the property setters or state setters.
The `fluid` interface provides the conversion to the correct unit system.
For example,
pressure could be passed to the `Pressure` setter as a `PressureT<PSI>`.
`fluid` will convert the provided pressure to the correct unit system internally.
