# Units Mini-Library

Include within `FluidProps++` is a mini units library.
The units library consists of types and units that handle conversion between different unit systems.
Effectively, the units library provides a mechanism to allow for strong typing of thermodynamic properties and,
most importantly,
allow for strong typing of the unit of measure associated with the property.
In the documentation,
we will refer to this concept as a strongly-typed unit.

The base type for any thermodynamic property is the [`PropertyT` type](./generated/class/classfluid__props_1_1PropertyT.rst).
`PropertyT` is a wrapper around a `double` precision number that gives meaning to the number.
It handles retrieval of the raw value,
conversion to another strongly-type unit,
comparison operations,
printing,
and arithmetic operations.
All operations on a `PropertyT` are `constexpr`,
so this abstraction is nearly zero-cost.

## Property Type Specialization

`PropertyT` is further sub-typed in a way that makes individual thermodynamic properties their own type without using an inheritance hierarchy.
Using `concepts` from the C++20 standard,
specific template specializations are provided that create special types.

The architecture specifies that a `PropertyT` have a template parameter of `Unit`.
See the following example to create a new unit type for a physical unit of length.

```c++
#pragma once
#include "units/property.hpp"
#include <concepts>
#include <string_view>

// Define some length units with their conversion factors and symbols
struct Meter
{
    static constexpr double kConversionFactor = 1.0; // Meter will be considered the base unit for all length conversions
    static constexpr double kOffset = 0.0; // An additive constant if needed for unit conversion, like in temperature conversions.
    static constexpr std::string_view kSymbol = "m"; // Used when converting the unit to a string
}

struct Foot
{
    static constexpr double kConversionFactor = 1.0/3.280839895013; // Conversion factor needed to convert to the base unit, in this case Meter
    static constexpr double kOffset = 0.0;
    static constexpr std::string_view kSymbol = "ft";
}

// Define the length unit concept which limits what a length unit type can be only a Meter or Foot.
template<typename Unit>
concept LengthUnit = std::same_as<Unit, Meter> || std::same_as<Unit, Foot>;

// Define the length type
template<LengthUnit Unit>
using LengthT = PropertyT<Unit>; // A length type that can only have a Unit of Meter or Foot

```

## Naming Convention

Defined types,
such as for pressure and temperature,
are named based on their dimensionality,
not their unit.
The naming convention is in Pascal case and is denoted with a `T` for type at the end,
`<dimensionality>T`.
For example,
a we would not have a `MeterT`,
since a meter is a unit.
A meter has dimensionality of length,
so the type name would be `LengthT`.

Units themselves have a naming convention,
and should use Pascal case also.
Units can have a magnitude specifier (e.g. kilo, mega, etc)
and the unit itself.
For example, `kPa` would have the type name, `KiloPascal`.
The magnitude specifier and unit name are treated as separate words and therefore the starting letter of each word is capitalized.

Unit can be compounded.
For example, velocity is a length per time.
The "per" signifier is treated as an `_` in the unit name.
So for the velocity type,
in SI units,
would be `Meter_Second`.
If the units compound through multiplication,
just treat as another word like `NewtonMeter`.
For inverse units, like $\frac{1}{K}$,
the name is preceded with an `I` specifier,
`IKelvin`.

The naming convention is somewhat of a compromise.
There really is not a "good" convention per se,
that balances readability and being explicit.
There are complex units out there.
Take for example the SAE unit for thermal conductivity, $\frac{BTU}{hr*ft*R}$,
this would be written as `BTU_Hour_Foot_Rankine` in `FluidProps++` naming conventions.
Other naming conventions that are in the wild would explicitly add the `per` in the name,
`BTUPerHourPerFootPerRankine`,
which seems much too verbose.
Another convention could look like,
`Btu_Hr_Ft_R`,
but that looks and feels odd,
and gets complicated with magnitude specifiers.

## Working With Unit Types

`PropertyT` provides an API that allows for manipulation of the unit types.
Let's initialize a pressure variable in kiloPascal's and work with it.

```c++
#include "units/units.hpp"

PressureT<KiloPascal> pressure{100.0};

// Get the raw value
double value = pressure.Value();

// Get the raw value converted to PSI
double valueAsPSI = pressure.ValueAs(PSI{});

// Get a new PressureT, but converted to PSI
PressureT<PSI> newPressure = pressure.As(PSI{});

// Check for equality within a given tolerance
bool equal = pressure.AreEqual(newPressure, 1.0e-8);

// Update the value in pressure
pressure.Value(200.0);

// Check if there's a non-zero value
bool nonZero = bool(pressure);

// Convert to string
auto asString = std::string{pressure}; // Will be "100.0 kPa"

```

## Arithmetic Operations

Arithmetic operators are defined for `PropertyT`,
but with some caveats:

```{admonition} caveats
---
class: important
---
* No checks for unit compatibility
* Does not propagate arithmetic for the units themselves
```

This library is not,
as of release 2025.01,
intended for correct dimensional analysis.
In other words,
it is possible to subtract and enthalpy from a pressure,
if so desired even though it makes no sense physically.

When adding or subtracting,
the unit type is preserved with the unit type favoring the left hand side unit.

For example,

```c++
#include "units/units.hpp"

PressureT<KiloPascal> pressure{100.0};
PressureT<MegaPascal> otherPressure{0.1};

// Add two pressures, units will be the left hand sides
auto added = pressure + otherPressure; // return PressureT<KiloPascal>{200.0};
auto addedRev = otherPressure + pressure; // return PressureT<MegaPascal>{0.2};

// Add scalar value
auto addedScalar = 100.0 + pressure; // return PressureT<KiloPascal>{200.0};

TemperatureT<Kelvin> temperature{273.15};

// WARNING!! This is allowed but not physical!
auto notPhysical = temperature + pressure; // Will return TemperatureT<Kelvin> with a value;

```

When multiplying or subtracting,
unit types are erased and a `double` value is returned.
You also need to be careful when multiplying unlike dimensions.

For example,

```c++
#include "units/units.hpp"

PressureT<KiloPascal> pressure{1.0};
PressureT<KiloPascal> otherPressure{1.0};

auto result = pressure*otherPressure; // returns 1.0

TemperatureT<Kelvin> temperature{273.15};

// INCORRECT usage!!
// you'll return a value, but first temperature will get converted to KiloPascal!
result = pressure*temperature;

// CORRECT usage
result = pressure*temperature.Value(); // returns 273.15


```
