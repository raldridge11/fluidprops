# Architecture and Usage

The usage within code is fairly straightforward.
We'll limit the discussion to using the water properties implemented via `IAPWSIF97` for illustrative purposes.
All other properties will follow the same usage.

## Namespacing

Namespacing is used for each different species to ensure no name clashing (i.e. water and heavy-water may have an `IAPWS95` implementation).
The namespaces follow the naming convention `fluid_props::<species>`.
So for standard water the namespace would be `fluid_props::water`,
for heavy water `fluid_props::d20`,
and so on for other species.

## Strongly Unit Typed

All fluid thermodynamic properties are strongly typed to their dimensionality and to their unit.
This is done to insure correct state setting,
but to also reduce the burden of converting between different unit systems.
Each fluid is defined to have a specific unit system that it is implemented in.
See [Units](./unit_types.md#units-mini-library) and [Extending](./extending.md#extending-and-adding-new-fluids) for more information.

## Example

Below is an example of water with the `IAPWSIF97` implementation,
to set the pressure and temperature,
and then get the enthalpy at that state.

```c++
#include <water/iapwsif97/iapwsif97.hpp>

using namespace fluid_props = fp;

// Going to create a water object at the P-T point of 3.0 MPa and 500.0 K.
auto water = fp::water::IAPWSIF97(fp::PressureT<MegaPascal>{3.0}, fp::TemperatureT<Kelvin>{500.0});

// Get enthalpy at the state of 3.0 MPa and 500.0 K
auto enthalpy = water.Enthalpy(); // Will return 975.542239 kJ/kg
// all other properties to get will be at this state

// Let's get the saturation temperature at 1.0 MPa
water.Pressure(fp::PressureT<MegaPascal>{1.0});

auto saturation_temperature = water.Saturation()->Temperature(); // Will return 453.035632 K

// Can even convert between unit systems
auto enthalpySAE = enthalpy.As(BTU_PoundMass{});

```

## Lazy Evaluation

As an optimization,
all properties are lazy evaluated.
So they are only calculated when they need to be.
There are two conditions for evaluation,
the first is the property has not been calculated yet,
the second is if there has been a state change.
In other words,
for a given state,
the property only needs to be calculated once,
any subsequent retrieval will just return the already calculated value.
This is best illustrated by reworking the previous example.

```c++
#include <water/iapwsif97/iapwsif97.hpp>

using namespace fluid_props = fp;

// Going to create a water object at the P-T point of 3.0 MPa and 500.0 K.
// No evaluation of properties is done, only telling the properties that they will
// need to be evaluated.
auto water = fp::water::IAPWSIF97(fp::PressureT<MegaPascal>{3.0}, fp::TemperatureT<Kelvin>{500.0});

// Get enthalpy at the state of 3.0 MPa and 500.0 K
// Enthalpy will be calculated and value stored internally
auto enthalpy = water.Enthalpy(); // Will return 975.542239 kJ/kg

// Retrieve enthalpy again. Will not re-evaluate enthalpy, only
// return the previously stored value
enthalpy = water.Enthalpy();

// State has change again, so properties will need to evaluated.
water.SetState(fp::PressureType<MegaPascal>{1.0}, fp::TemperatureT<Kelvin>{500.0});
```

### Proof of Performance

So, how much does lazy evaluation buy us?
Quite a bit,
but take the following result from the CI/CD run of the benchmarks as qualitative to show the relative performance of lazy evaluation versus not lazy evaluating.

```sh
$ ./$BUILD_DIR/bin/BenchmarkFluidProps++ --benchmark_time_unit=us
2023-12-25T23:44:19+00:00
Running ./FluidProps/bin/BenchmarkFluidProps++
Run on (2 X 2250 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x1)
  L1 Instruction 32 KiB (x1)
  L2 Unified 512 KiB (x1)
  L3 Unified 16384 KiB (x1)
Load Average: 2.55, 0.88, 0.32
-----------------------------------------------------------------------------------------------
Benchmark                                                     Time             CPU   Iterations
-----------------------------------------------------------------------------------------------
SuperCriticalRegionLazyEvaluationStateSet                  14.1 ns         14.1 ns     49554215
SuperCriticalRegionLazyEvaluationPropertyCalculation       2.04 us         2.04 us       341913
IAPWSIF97LazyEvaluation                                   0.630 ns        0.630 ns   1000000000
IAPWSIF97NonLazyEvaluation                                 1.20 us         1.20 us       582267
```

Orders of magnitude difference between CPU time and number of iterations in the given time.
However,
in most CFD or simulation usage of the library,
the optimization of lazy evaluation may not be achievable.
It will just depend on the usage.
But it's there and should be.
