# FluidProps++ Documentation

Welcome to the FluidProps++ Documentation.

FluidProps++ aims to create a fully open source implementation of various fluids used for Computation Fluid Dynamics (CFD),
fluid systems analysis and simulation,
or just general needs for looking up fluid properties given state variables.
The idea is to make an easily extensible framework and architecture in which the end-user could relatively easily add any custom fluids that they need for their purposes.
Another aim of FluidProps++ is to be computationally efficient by leveraging lazy evaluation of properties after state variables are set and subsequent queries of a property with the same state variables does not recalculate the return value,
see discussion on [architecture](./usage.md).
The library aims also to be cross-platform and is built as a distributable with shared libraries for Linux and Windows.
It is up to the end user to deal with any other platforms or compilers they want to use it on.
But there is no platform specific code in the library itself (may not be true for the dependencies, however).

```{toctree}
---
maxdepth: 1
caption: Contents
---
installation
unit_types
usage
fluids
extending
api
```
