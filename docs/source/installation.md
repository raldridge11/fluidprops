# Installation and Building

## Installing

If you are integrating the library into a Linux or Windows application,
the installation is fairly straight forward.

* Download build package for the platform
* Setup include directories and link libraries for your project's build system

The headers for `FluidProps++` are included in the `include` directory.
The relevant shared libraries are contained in the `lib` directory.
And the `bin` directory contains tests and benchmark executables for the project.
Yes, we include and distribute the tests and benchmarks with the library.

## Building

Dependency management is done through the `conan` package manager.
There is included in the project a `conan` recipe to build the project.
You can directly use `conan` to build `FluidProps++` or use the AldridgeSoftwareDesigns tool `cau` for managing `c++` projects.
The `cau` project is located [here](https://gitlab.com/aldridgesoftwaredesigns/cau).

Using `cau`,
you can one-shot build `FluidProps++`,

```sh
cau build -b build -t Debug -p linux
```

where the `-b` flag is the build output directory,
`-t` flag is the build type (Debug or Release),
and the `-p` is the platform (linux or win64).
`cau` has a light-weight wrapper around `conan` to manage dependencies and build the project.
There are included build profiles which `conan` will use for linux or win64.
The win64 profile is a cross-compilation profile for compiling on Linux for Windows.
You must have the `mingw` toolchain installed for cross-compiling to work.
If compiling on Windows for Windows,
we suggest creating your own profile (or opening a MR to include for the project).
See the `conan` [documentation](https://docs.conan.io/2/) for how to create profiles.

Once gitlab gets their act together,
there will be a `conan` recipe just available to consume for a `conan` managed project.

Once built,
you should verify that the library correctly built by running the tests that will be located at `$BUILD_DIRECTORY/bin/TestFluidProps++` and the benchmark located at `$BUILD_DIRECTORY/bin/BenchmarkFluidProps++`.
