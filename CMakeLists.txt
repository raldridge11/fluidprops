cmake_minimum_required(VERSION 3.29 FATAL_ERROR)

project(FluidProps++ LANGUAGES CXX)

find_package(spdlog REQUIRED)
include_directories(SYSTEM ${fmt_INCLUDE_DIR} ${spdlog_INCLUDE_DIR})

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake ${CMAKE_MODULE_PATH})

include(GNUInstallDirs)

set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)
set(LLVM_BUILD_INSTRUMENTED_COVERAGE ON)
set(CMAKE_CXX_SCAN_FOR_MODULES OFF)
add_compile_options(-fPIC -Wall -Werror)

message(STATUS "My Build Type: ${CMAKE_BUILD_TYPE} Cross Building To: $ENV{PLATFORM}")

IF(CMAKE_BUILD_TYPE MATCHES Debug AND NOT "$ENV{PLATFORM}" STREQUAL "win64")
    add_compile_options(-fprofile-instr-generate -fcoverage-mapping -gdwarf-4 -O0)
    add_link_options(-fprofile-instr-generate)
ENDIF()

# Bins and libs to correct folder.
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_BINDIR})

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
add_subdirectory(src/logger)
add_subdirectory(src/math_methods)
add_subdirectory(src/water)
add_subdirectory(tests)
add_subdirectory(benchmark)

add_library(${CMAKE_PROJECT_NAME} SHARED $<TARGET_OBJECTS:logger> $<TARGET_OBJECTS:math_methods> $<TARGET_OBJECTS:water> $<TARGET_OBJECTS:iapwsif97>)
target_link_libraries(${CMAKE_PROJECT_NAME} PRIVATE spdlog::spdlog)
