#!/bin/bash
source $WORKON_HOME/fluids-docs/bin/activate
sphinx-autobuild --port=0 --open-browser ./docs/source ./docs/build/html --re-ignore doxygen --re-ignore generated --watch ./include
