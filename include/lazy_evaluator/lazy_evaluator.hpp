#pragma once
#include <functional>
#include <utility>
namespace fluid_props
{
    /**
     * @brief `LazyEvaluator` is used to determine if a property has been set or already calculated.
     * The idea of `LazyEvaluator` is only allow calculations of a property if it is needed. For example,
     * let's say pressure and temperature were to stay constant, but enthalpy needs to be used or accessed
     * x amount of times. The calculation of enthalpy may be computationally intensive, so calculating x amount of
     * times is expensive and unnecessary. With the `LazyEvaluator` the enthalpy calculation should only run once
     * and then return the calculated value on subsequent calls.
     * @tparam T
     */
    template <typename T> class LazyEvaluator
    {
    public:
        LazyEvaluator() = default;

        /**
         * @brief Construct a new Lazy Evaluator object with a reference to a function/method
         * to base evaluation on
         *
         * @param method
         */
        explicit LazyEvaluator(const std::function<T()> method) : value_(T{}), method_(std::move(method)){};

        ~LazyEvaluator() = default;

    public:
        /**
         * @brief Lazily evaluates the function/method if available
         *
         * @return T
         */
        auto Value() -> T
        {
            if (this->IsSet())
            {
                return this->value_;
            }

            if (this->method_)
            {
                this->value_ = this->method_();
            }

            return this->value_;
        };

        /**
         * @brief Assigns function/method to evaluate lazily
         *
         * @param method
         */
        void SetMethod(const std::function<T()> &method) { this->method_ = method; };

        /**
         * @brief Resets value to default
         *
         */
        void Reset() { this->value_ = T{}; }

        /**
         * @brief Flag indicating if this property has already been evaluated
         *
         * @return true if there is a non-default value
         * @return false if the value is default
         */
        [[nodiscard]] auto IsSet() const -> bool { return static_cast<bool>(this->value_); }

        /**
         * @brief Assigns a value to the evaluator
         *
         * @param value
         */
        void operator=(const T value) { this->value_ = value; }

    private:
        T value_;
        std::function<T()> method_;
    };
} // namespace fluid_props
