#pragma once
#include <functional>

/**
 * @brief Collection of mathematical methods for computing fluid properties
 *
 */
namespace fluid_props::math_methods
{
    /**
     * @brief The default tolerance in error for all iterative methods
     *
     */
    // NOLINTNEXTLINE(clang-diagnostic-unneeded-internal-declaration)
    static const double kDefaultTolerance = 1.0e-7;

    /**
     * @brief The default maximum number of iterations for an iterative method
     *
     */
    static const int kDefaultMaxIterations = 100;

    /**
     * @brief Secant method for finding roots of one-dimensional functions
     *
     * @param func function to find the root of
     * @param x_0 ith - 1 guess for the root
     * @param x_1  ith guess for the root
     * @param tolerance tolerance in error for finding the root
     * @param max_iteration maximum iteration until we've deemed the solution is converging
     * @return double root of provided function
     */
    auto SecantMethod(
        const std::function<double(double)> &func,
        double x_0,
        double x_1,
        const double tolerance = kDefaultTolerance,
        const int max_iteration = kDefaultMaxIterations
    ) -> double;

    /**
     * @brief Bisection method for finding roots of one-dimensional functions
     * @param func function to find the root of
     * @param x_0 left hand side to bracket root of
     * @param x_1 right hand side to bracket root of
     * @param tolerance tolerance in error for finding the root
     * @param max_iteration maximum iteration until we've deemed the solution is converging
     * @return double root of provided function
     */
    auto BisectionMethod(
        const std::function<double(double)> &func,
        double x_0,
        double x_1,
        const double tolerance = kDefaultTolerance,
        const int max_iteration = kDefaultMaxIterations
    ) -> double;
} // namespace fluid_props::math_methods
