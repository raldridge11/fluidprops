#pragma once
#include "types/types.hpp"
#include "units/units.hpp"
#include <cstdint>
#include <memory>
namespace fluid_props
{
    template <
        typename PressureUnit,
        typename TemperatureUnit,
        typename EnthalpyUnit,
        typename EntropyUnit,
        typename SpecificVolumeUnit,
        typename DensityUnit,
        typename VelocityUnit,
        typename ThermalExpansionUnit,
        typename CompressibilityUnit,
        typename ViscosityUnit,
        typename ThermalConductivityUnit>
    struct FluidUnits
    {
        using Pressure = PressureUnit;
        using Temperature = TemperatureUnit;
        using Enthalpy = EnthalpyUnit;
        using Entropy = EntropyUnit;
        using SpecificVolume = SpecificVolumeUnit;
        using Density = DensityUnit;
        using Velocity = VelocityUnit;
        using ThermalExpansion = ThermalExpansionUnit;
        using Compressibility = CompressibilityUnit;
        using Viscosity = ViscosityUnit;
        using ThermalConductivity = ThermalConductivityUnit;
    };

    /**
     * @brief Defines the interface for a fluid
     *
     * @tparam UnitSystem the unit system associated with the fluid implementation.
     */
    template <typename UnitSystem> class Fluid
    {
    public:
        using PressureUnit = UnitSystem::Pressure;
        using TemperatureUnit = UnitSystem::Temperature;
        using EnthalpyUnit = UnitSystem::Enthalpy;
        using EntropyUnit = UnitSystem::Entropy;
        using SpecificVolumeUnit = UnitSystem::SpecificVolume;
        using DensityUnit = UnitSystem::Density;
        using VelocityUnit = UnitSystem::Velocity;
        using ThermalExpansionUnit = UnitSystem::ThermalExpansion;
        using CompressibilityUnit = UnitSystem::Compressibility;
        using ViscosityUnit = UnitSystem::Viscosity;
        using ThermalConductivityUnit = UnitSystem::ThermalConductivity;
        virtual ~Fluid() = default;

    public: // Mutable properties
        /**
         * @brief Pressure of fluid
         *
         * @return PressureT<MegaPascal>
         */
        virtual auto Pressure() -> PressureT<PressureUnit> = 0;

        /**
         * @brief Sets the fluid pressure
         *
         * @param pressure
         */
        virtual void Pressure(PressureT<PressureUnit> pressure) = 0;

        template <typename OtherPressureUnit> void Pressure(PressureT<OtherPressureUnit> pressure)
        {
            this->Pressure(pressure.As(PressureUnit{}));
        }

        /**
         * @brief Enthalpy of fluid
         *
         * @return SpecificEnergyT<EnthalpyUnit>
         */
        virtual auto Enthalpy() -> SpecificEnergyT<EnthalpyUnit> = 0;

        /**
         * @brief Sets fluid enthalpy
         *
         * @param enthalpy
         */
        virtual void Enthalpy(SpecificEnergyT<EnthalpyUnit> enthalpy) = 0;

        template <typename OtherEnthalpyUnit> void Enthalpy(SpecificEnergyT<OtherEnthalpyUnit> enthalpy)
        {
            this->Enthalpy(enthalpy.As(EnthalpyUnit{}));
        }

        /**
         * @brief Temperature of fluid
         *
         * @return TemperatureT<TemperatureUnit>
         */
        virtual auto Temperature() -> TemperatureT<TemperatureUnit> = 0;

        /**
         * @brief Sets fluid temperature
         *
         * @param temperature
         */
        virtual void Temperature(TemperatureT<Kelvin> temperature) = 0;

        template <typename OtherTemperatureUnit> void Temperature(TemperatureT<OtherTemperatureUnit> temperature)
        {
            this->Temperature(temperature.As(TemperatureUnit{}));
        }

        /**
         * @brief Entropy of fluid
         *
         * @return SpecificEntropyT<EntropyUnit>
         */
        virtual auto Entropy() -> SpecificEntropyT<EntropyUnit> = 0;

        /**
         * @brief Sets entropy of fluid
         *
         * @param entropy
         */
        virtual void Entropy(SpecificEntropyT<EntropyUnit> entropy) = 0;

        template <typename OtherEntropyUnit> void Entropy(SpecificEntropyT<OtherEntropyUnit> entropy)
        {
            this->Entropy(entropy.As(EntropyUnit{}));
        }

        /**
         * @brief Sets property combination of pressure and temperature
         *
         * @param pressure
         * @param temperature
         */
        virtual void SetState(PressureT<PressureUnit> pressure, TemperatureT<TemperatureUnit> temperature) = 0;

        template <typename OtherPressureUnit, typename OtherTemperatureUnit>
            requires(!std::is_same_v<OtherPressureUnit, PressureUnit> && !std::is_same_v<OtherTemperatureUnit, TemperatureUnit>)
        void SetState(PressureT<OtherPressureUnit> pressure, TemperatureT<OtherTemperatureUnit> temperature)
        {
            this->SetState(pressure.As(PressureUnit{}), temperature.As(TemperatureUnit{}));
        }

        /**
         * @brief Sets property combination of pressure and enthalpy
         *
         * @param pressure
         * @param enthalpy
         */
        virtual void SetState(PressureT<PressureUnit> pressure, SpecificEnergyT<EnthalpyUnit> enthalpy) = 0;

        template <typename OtherPressureUnit, typename OtherEnthalpyUnit>
            requires(!std::is_same_v<OtherPressureUnit, PressureUnit> && !std::is_same_v<OtherEnthalpyUnit, EnthalpyUnit>)
        void SetState(PressureT<OtherPressureUnit> pressure, SpecificEnergyT<OtherEnthalpyUnit> enthalpy)
        {
            this->SetState(pressure.As(PressureUnit{}), enthalpy.As(EnthalpyUnit{}));
        }

        /**
         * @brief Sets property combination of pressure and enthalpy
         *
         * @param pressure
         * @param entropy
         */
        virtual void SetState(PressureT<PressureUnit> pressure, SpecificEntropyT<EntropyUnit> entropy) = 0;

        template <typename OtherPressureUnit, typename OtherEntropyUnit>
            requires(!std::is_same_v<OtherPressureUnit, PressureUnit> && !std::is_same_v<OtherEntropyUnit, EntropyUnit>)
        void SetState(PressureT<OtherPressureUnit> pressure, SpecificEntropyT<OtherEntropyUnit> entropy)
        {
            this->SetState(pressure.As(PressureUnit{}), entropy.As(EntropyUnit{}));
        }

        /**
         * @brief Sets property combination of enthalpy and entropy
         *
         * @param enthalpy
         * @param entropy
         */
        virtual void SetState(SpecificEnergyT<EnthalpyUnit> enthalpy, SpecificEntropyT<EntropyUnit> entropy) = 0;

        template <typename OtherEnthalpyUnit, typename OtherEntropyUnit>
            requires(!std::is_same_v<OtherEnthalpyUnit, EnthalpyUnit> && !std::is_same_v<OtherEntropyUnit, EntropyUnit>)
        void SetState(SpecificEnergyT<OtherEnthalpyUnit> enthalpy, SpecificEntropyT<OtherEntropyUnit> entropy)
        {
            this->SetState(enthalpy.As(EnthalpyUnit{}), entropy.As(EntropyUnit{}));
        }

    public: // Derived properties
        /**
         * @brief Specific Volume of fluid
         *
         * @return SpecificVolumeT<SpecificVolumeUnit>
         */
        virtual auto SpecificVolume() -> SpecificVolumeT<SpecificVolumeUnit> = 0;

        /**
         * @brief Internal energy of fluid
         *
         * @return SpecificEnergyT<EnthalpyUnit>
         */
        virtual auto InternalEnergy() -> SpecificEnergyT<EnthalpyUnit> = 0;

        /**
         * @brief Isobaric heat capacity \f$C_p\f$ of fluid
         *
         * @return HeatCapacityT<EntropyUnit>
         */
        virtual auto IsobaricHeatCapacity() -> HeatCapacityT<EntropyUnit> = 0;

        /**
         * @brief Isochoric heat capacity \f$C_v\f$
         *
         * @return HeatCapacityT<EntropyUnit>
         */
        virtual auto IsochoricHeatCapacity() -> HeatCapacityT<EntropyUnit> = 0;

        /**
         * @brief Speed of sound of fluid
         *
         * @return VelocityT<VelocityUnit>
         */
        virtual auto SpeedOfSound() -> VelocityT<VelocityUnit> = 0;

        /**
         * @brief Isobaric Cubic Expansion Coefficient
         *
         * @return ThermalExpansionT<ThermalExpansionUnit>
         */
        virtual auto CubicExpansion() -> ThermalExpansionT<ThermalExpansionUnit> = 0;

        /**
         * @brief Fluid isothermal compressibility
         *
         * @return CompressibilityT<CompressibilityUnit>
         */
        virtual auto Compressibility() -> CompressibilityT<CompressibilityUnit> = 0;

        /**
         * @brief Fluid viscosity
         *
         * @return ViscosityT<ViscosityUnit>
         */
        virtual auto Viscosity() -> ViscosityT<ViscosityUnit> = 0;

        /**
         * @brief Fluid thermal conductivity
         *
         * @return ThermalConductivityT<ThermalConductivityUnit>
         */
        virtual auto ThermalConductivity() -> ThermalConductivityT<ThermalConductivityUnit> = 0;

        /**
         * @brief Density of fluid
         *
         * @return DensityT<DensityUnit>
         */
        virtual auto Density() -> DensityT<DensityUnit> = 0;
    };

    /**
     * @brief Phase of fluid enumeration
     *
     */
    enum class Phase : std::uint8_t
    {
        /**
         * @brief Liquid phase
         *
         */
        Liquid,
        /**
         * @brief Vapor phase
         *
         */
        Vapor
    };

    /**
     * @brief Defines the interface for a saturated, two-phase fluid
     *
     * @tparam UnitSystem the unit system associated with the fluid implementation.
     */
    template <typename UnitSystem> class Saturated
    {
    public:
        using PressureUnit = UnitSystem::Pressure;
        using TemperatureUnit = UnitSystem::Temperature;
        using EnthalpyUnit = UnitSystem::Enthalpy;
        using EntropyUnit = UnitSystem::Entropy;
        using SpecificVolumeUnit = UnitSystem::SpecificVolume;
        using DensityUnit = UnitSystem::Density;
        using VelocityUnit = UnitSystem::Velocity;
        using ThermalExpansionUnit = UnitSystem::ThermalExpansion;
        using CompressibilityUnit = UnitSystem::Compressibility;
        virtual ~Saturated() = default;

    public: // mutable properties
        /**
         * @brief Pressure of fluid
         *
         * @return PressureT<MegaPascal>
         */
        virtual auto Pressure() -> PressureT<PressureUnit> = 0;

        /**
         * @brief Sets the fluid pressure
         *
         * @param pressure
         */
        virtual void Pressure(PressureT<PressureUnit> pressure) = 0;

        template <typename OtherPressureUnit> void Pressure(PressureT<OtherPressureUnit> pressure)
        {
            this->Pressure(pressure.As(PressureUnit{}));
        }

        /**
         * @brief Temperature of fluid
         *
         * @return TemperatureT<TemperatureUnit>
         */
        virtual auto Temperature() -> TemperatureT<TemperatureUnit> = 0;

        /**
         * @brief Sets fluid temperature
         *
         * @param temperature
         */
        virtual void Temperature(TemperatureT<TemperatureUnit> temperature) = 0;

        template <typename OtherTemperatureUnit> void Temperature(TemperatureT<OtherTemperatureUnit> temperature)
        {
            this->Temperature(temperature.As(TemperatureUnit{}));
        }

    public: // Derived properties
        /**
         * @brief Saturated Enthalpy
         *
         * @param Phase phase to get saturated value of
         * @return SpecificEnergyT<EnthalpyUnit>
         */
        virtual auto Enthalpy(Phase phase) -> SpecificEnergyT<EnthalpyUnit> = 0;

        /**
         * @brief Saturated Entropy
         *
         * @param Phase phase to get saturated value of
         * @return SpecificEntropyT<EntropyUnit>
         */
        virtual auto Entropy(Phase phase) -> SpecificEntropyT<EntropyUnit> = 0;

        /**
         * @brief Saturated Specific Volume
         *
         * @param Phase phase to get saturated value of
         * @return SpecificVolumeT<SpecificVolumeUnit>
         */
        virtual auto SpecificVolume(Phase phase) -> SpecificVolumeT<SpecificVolumeUnit> = 0;

        /**
         * @brief Saturated Density
         *
         * @param Phase phase to get saturated value of
         * @return DensityT<DensityUnit>
         */
        virtual auto Density(Phase phase) -> DensityT<DensityUnit> = 0;

        /**
         * @brief Saturated Internal Energy
         *
         * @param Phase phase to get saturated value of
         * @return SpecificEnergyT<EnthalpyUnit>
         */
        virtual auto InternalEnergy(Phase phase) -> SpecificEnergyT<EnthalpyUnit> = 0;

        /**
         * @brief Saturated Isobaric Heat Capacity
         *
         * @param Phase phase to get saturated value of
         * @return HeatCapacityT<EntropyUnit>
         */
        virtual auto IsobaricHeatCapacity(Phase phase) -> HeatCapacityT<EntropyUnit> = 0;

        /**
         * @brief Saturated Isochoric Heat Capacity
         *
         * @param Phase phase to get saturated value of
         * @return HeatCapacityT<EntropyUnit>
         */
        virtual auto IsochoricHeatCapacity(Phase phase) -> HeatCapacityT<EntropyUnit> = 0;

        /**
         * @brief Saturated Speed of Sound
         *
         * @param Phase phase to get saturated value of
         * @return VelocityT<VelocityUnit>
         */
        virtual auto SpeedOfSound(Phase phase) -> VelocityT<VelocityUnit> = 0;

        /**
         * @brief Saturation Cubic Expansion coefficient
         *
         * @param phase phase to get saturated value of
         * @return ThermalExpansionT<ThermalExpansionUnit>
         */
        virtual auto CubicExpansion(Phase phase) -> ThermalExpansionT<ThermalExpansionUnit> = 0;

        /**
         * @brief Fluid isothermal compressibility
         *
         * @param phase phase to get saturated value of
         * @return CompressibilityT<CompressibilityUnit>
         */
        virtual auto Compressibility(Phase phase) -> CompressibilityT<CompressibilityUnit> = 0;
    };

    template <
        typename PressureUnit,
        typename TemperatureUnit,
        typename EnthalpyUnit,
        typename EntropyUnit,
        typename SpecificVolumeUnit,
        typename DensityUnit,
        typename VelocityUnit,
        typename ThermalExpansionUnit,
        typename CompressibilityUnit,
        typename ViscosityUnit,
        typename ThermalConductivityUnit,
        typename QualityUnit,
        typename VoidUnit>
    struct TwoPhaseFluidUnits : public FluidUnits<
                                    PressureUnit,
                                    TemperatureUnit,
                                    EnthalpyUnit,
                                    EntropyUnit,
                                    SpecificVolumeUnit,
                                    DensityUnit,
                                    VelocityUnit,
                                    ThermalExpansionUnit,
                                    CompressibilityUnit,
                                    ViscosityUnit,
                                    ThermalConductivityUnit>
    {
        using Quality = QualityUnit;
        using Void = VoidUnit;
    };

    /**
     * @brief Defines the interface of a two phase fluid
     *
     * @tparam UnitSystem the unit system associated with the fluid implementation.
     */
    template <typename UnitSystem> class TwoPhaseFluid : public Fluid<UnitSystem>
    {
    public:
        using PressureUnit = UnitSystem::Pressure;
        using TemperatureUnit = UnitSystem::Temperature;
        using EnthalpyUnit = UnitSystem::Enthalpy;
        using EntropyUnit = UnitSystem::Entropy;
        using SpecificVolumeUnit = UnitSystem::SpecificVolume;
        using DensityUnit = UnitSystem::Density;
        using VelocityUnit = UnitSystem::Velocity;
        using ThermalExpansionUnit = UnitSystem::ThermalExpansion;
        using CompressibilityUnit = UnitSystem::Compressibility;
        using ViscosityUnit = UnitSystem::Viscosity;
        using ThermalConductivityUnit = UnitSystem::ThermalConductivity;
        using QualityUnit = typename UnitSystem::Quality;
        using VoidUnit = typename UnitSystem::Void;
        using SaturationType = Saturated<UnitSystem>;
        using Fluid<UnitSystem>::SetState;
        /**
         * @brief Sets property combination of pressure and quality
         *
         * @param pressure
         * @param quality
         */
        virtual void SetState(PressureT<PressureUnit> pressure, QualityT<QualityUnit> quality) = 0;

        template <typename OtherPressureUnit, typename OtherQualityUnit>
            requires(!std::is_same_v<OtherPressureUnit, PressureUnit> && !std::is_same_v<OtherQualityUnit, QualityUnit>)
        void SetState(PressureT<OtherPressureUnit> pressure, QualityT<OtherQualityUnit> quality)
        {
            this->SetState(pressure.As(PressureUnit{}), quality.As(QualityUnit{}));
        }

        /**
         * @brief Sets property combination of temperature and quality
         *
         * @param temperature
         * @param quality
         */
        virtual void SetState(TemperatureT<Kelvin> temperature, QualityT<Dimensionless> quality) = 0;

        template <typename OtherTemperatureUnit, typename OtherQualityUnit>
            requires(!std::is_same_v<OtherTemperatureUnit, TemperatureUnit> && !std::is_same_v<OtherQualityUnit, QualityUnit>)
        void SetState(TemperatureT<OtherTemperatureUnit> temperature, QualityT<OtherQualityUnit> quality)
        {
            this->SetState(temperature.As(TemperatureUnit{}), quality.As(QualityUnit{}));
        }

    public: // Mutable
        /**
         * @brief Vapor phase quality
         *
         * @return QualityT<QualityUnit>
         */
        virtual auto Quality() -> QualityT<QualityUnit> = 0;

        /**
         * @brief Sets vapor phase quality
         *
         * @param QualityT<QualityUnit>
         */
        virtual void Quality(QualityT<QualityUnit> quality) = 0;

    public: // Derived
        /**
         * @brief Saturation properties of fluid
         *
         * @return std::shared_ptr<Saturated>
         */
        virtual auto Saturation() -> std::shared_ptr<SaturationType> = 0;

        /**
         * @brief Void Fraction of mixture
         *
         * @return VoidT<VoidUnit>
         */
        virtual auto VoidFraction() -> VoidT<VoidUnit> = 0;
    };

} // namespace fluid_props
