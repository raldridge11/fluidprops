#pragma once
#include "lazy_evaluator/lazy_evaluator.hpp"
#include <concepts>
#include <valarray>
namespace fluid_props
{
    using Property = double;
    using LazyProperty = LazyEvaluator<Property>;
    using Equation = std::function<double()>;
    using Validator = std::function<bool()>;
    using Coefficient = std::valarray<double>;
    using Exponent = std::valarray<double>;
    using Normalization = double;
    using NormalizedProperty = double;
    using LazyNormalizedProperty = LazyEvaluator<NormalizedProperty>;

    template <class Base, class T>
    concept Derived = std::is_base_of_v<Base, T>;
} // namespace fluid_props
