#pragma once
#include <cmath>
#include <fmt/core.h>
#include <fmt/format.h>
#include <iostream>

namespace fluid_props
{
    /**
     * @brief Describes a thermodynamic property.
     *
     * @tparam Unit of property
     */
    template <typename Unit> class PropertyT
    {
    public:
        /**
         * @brief Construct a new PropertyT object with a value of 0.0
         *
         */
        explicit constexpr PropertyT() : value_(0.0) {}

        /**
         * @brief Construct a new Property T object with a non-zero value
         *
         * @param value initial value
         */
        explicit constexpr PropertyT(double value) : value_(value) {}

    public:
        /**
         * @brief Gets raw value of property, no typing protection.
         *
         * @return double
         */
        [[nodiscard]] constexpr auto Value() const -> double { return this->value_; }

        /**
         * @brief Sets value of property, assuming same unit system
         *
         * @param value
         */
        constexpr void Value(double value) { this->value_ = value; }

        /**
         * @brief Calculates value of property in requested unit system.
         * This is a raw value with no typing protection
         *
         * @tparam ToUnit Unit to convert to
         * @param unit Unit to convert to
         * @return double
         */
        template <typename ToUnit> [[nodiscard]] constexpr auto ValueAs(const ToUnit &unit) const -> double
        {
            auto base_value = (this->value_ + Unit::kOffset) * Unit::kConversionFactor;
            return base_value / ToUnit::kConversionFactor - ToUnit::kOffset;
        }

        /**
         * @brief New property in the requested unit system.
         *
         * @tparam ToUnit Unit to convert to
         * @param unit Unit to convert to
         * @return PropertyT<ToUnit>
         */
        template <typename ToUnit> [[nodiscard]] constexpr auto As(const ToUnit &unit) const -> PropertyT<ToUnit>
        {
            return PropertyT<ToUnit>{this->ValueAs(unit)};
        }

        /**
         * @brief Checks if two properties are equal with a certain tolerance
         *
         * @tparam OtherUnit Unit of property comparing to
         * @param other other property
         * @param tolerance tolerance that determines if the two values of the property are close
         * @return true if the values of the properties are close
         * @return false if they are not close
         */
        template <typename OtherUnit>
        [[nodiscard]] constexpr auto AreEqual(const PropertyT<OtherUnit> &other, const double &tolerance = 1.0e-6) const
            -> bool
        {
            auto converted = other.ValueAs(Unit{});
            return std::abs(this->value_ - converted) < tolerance;
        }

    public:
        explicit operator const std::string() const { return fmt::format("{:.4g} {}", this->Value(), Unit::kSymbol); }
        explicit operator bool() const { return this->value_ != 0.0; }

    private:
        double value_;
    };

    template <typename Unit> auto operator<<(std::ostream &os, const PropertyT<Unit> &property) -> std::ostream &
    {
        os << property.Value() << " " << Unit::kSymbol;
        return os;
    }

    template <typename LHSUnit, typename RHSUnit>
    constexpr auto operator==(const PropertyT<LHSUnit> &lhs, const PropertyT<RHSUnit> &rhs) -> bool
    {
        return rhs.AreEqual(lhs);
    }

    template <typename LHSUnit, typename RHSUnit>
    constexpr auto operator!=(const PropertyT<LHSUnit> &lhs, const PropertyT<RHSUnit> &rhs) -> bool
    {
        return !(lhs == rhs);
    }

    template <typename LHSUnit, typename RHSUnit>
    constexpr auto operator<(const PropertyT<LHSUnit> &lhs, const PropertyT<RHSUnit> &rhs) -> bool
    {
        return lhs.Value() < rhs.ValueAs(LHSUnit{});
    }

    template <typename LHSUnit, typename RHSUnit>
    constexpr auto operator>(const PropertyT<LHSUnit> &lhs, const PropertyT<RHSUnit> &rhs) -> bool
    {
        return lhs.Value() > rhs.ValueAs(LHSUnit{});
    }

    template <typename LHSUnit, typename RHSUnit>
    constexpr auto operator<=(const PropertyT<LHSUnit> &lhs, const PropertyT<RHSUnit> &rhs) -> bool
    {
        return !(lhs > rhs);
    }

    template <typename LHSUnit, typename RHSUnit>
    constexpr auto operator>=(const PropertyT<LHSUnit> &lhs, const PropertyT<RHSUnit> &rhs) -> bool
    {
        return !(lhs < rhs);
    }

    template <typename LHSUnit, typename RHSUnit>
    constexpr auto operator+(const PropertyT<LHSUnit> &lhs, const PropertyT<RHSUnit> &rhs) -> PropertyT<LHSUnit>
    {
        auto converted = rhs.As(LHSUnit{});
        return PropertyT<LHSUnit>{lhs.Value() + converted.Value()};
    }

    template <typename LHSUnit>
    constexpr auto operator+(const PropertyT<LHSUnit> &lhs, const double &rhs) -> PropertyT<LHSUnit>
    {
        return lhs + PropertyT<LHSUnit>{rhs};
    }

    template <typename RHSUnit>
    constexpr auto operator+(const double &lhs, const PropertyT<RHSUnit> &rhs) -> PropertyT<RHSUnit>
    {
        return rhs + lhs;
    }

    template <typename LHSUnit, typename RHSUnit>
    constexpr auto operator-(const PropertyT<LHSUnit> &lhs, const PropertyT<RHSUnit> &rhs) -> PropertyT<LHSUnit>
    {
        auto converted = rhs.As(LHSUnit{});
        return PropertyT<LHSUnit>{lhs.Value() - converted.Value()};
    }

    template <typename LHSUnit>
    constexpr auto operator-(const PropertyT<LHSUnit> &lhs, const double &rhs) -> PropertyT<LHSUnit>
    {
        return lhs - PropertyT<LHSUnit>{rhs};
    }

    template <typename RHSUnit>
    constexpr auto operator-(const double &lhs, const PropertyT<RHSUnit> &rhs) -> PropertyT<RHSUnit>
    {
        return PropertyT<RHSUnit>{lhs} - rhs;
    }

    template <typename LHSUnit, typename RHSUnit>
    constexpr auto operator*(const PropertyT<LHSUnit> &lhs, const PropertyT<RHSUnit> &rhs) -> double
    {
        auto converted = rhs.As(LHSUnit{});
        return lhs.Value() * converted.Value();
    }

    template <typename LHSUnit> constexpr auto operator*(const PropertyT<LHSUnit> &lhs, const double &rhs) -> double
    {
        return lhs * PropertyT<LHSUnit>{rhs};
    }

    template <typename RHSUnit> constexpr auto operator*(const double &lhs, const PropertyT<RHSUnit> &rhs) -> double
    {
        return rhs * lhs;
    }

    template <typename LHSUnit, typename RHSUnit>
    constexpr auto operator/(const PropertyT<LHSUnit> &lhs, const PropertyT<RHSUnit> &rhs) -> double
    {
        auto converted = rhs.As(LHSUnit{});
        return lhs.Value() / converted.Value();
    }

    template <typename LHSUnit> constexpr auto operator/(const PropertyT<LHSUnit> &lhs, const double &rhs) -> double
    {
        return lhs / PropertyT<LHSUnit>{rhs};
    }

    template <typename RHSUnit> constexpr auto operator/(const double &lhs, const PropertyT<RHSUnit> &rhs) -> double
    {
        return PropertyT<RHSUnit>{lhs} / rhs;
    }

} // namespace fluid_props

template <typename Unit> struct fmt::formatter<fluid_props::PropertyT<Unit>> : fmt::formatter<std::string_view>
{
    auto format(fluid_props::PropertyT<Unit> property, fmt::format_context &ctx) const -> fmt::format_context::iterator
    {
        return fmt::formatter<std::string_view>::format(std::string{property}, ctx);
    }
};
