#pragma once
#include "units/property.hpp"
#include <concepts>
#include <string_view>

namespace fluid_props
{
    struct Pascal
    {
        static constexpr double kConversionFactor = 1.0;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "Pa";
    };

    struct MegaPascal
    {
        static constexpr double kConversionFactor = 1.0e6;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "MPa";
    };

    struct KiloPascal
    {
        static constexpr double kConversionFactor = 1.0e3;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "kPa";
    };

    struct PSI
    {
        static constexpr double kConversionFactor = 6894.75728034;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "psi";
    };

    struct Kelvin
    {
        static constexpr double kConversionFactor = 1.0;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "K";
    };

    struct Celsius
    {
        static constexpr double kConversionFactor = 1.0;
        static constexpr double kOffset = 273.15;
        static constexpr std::string_view kSymbol = "°C";
    };

    struct Fahrenheit
    {
        static constexpr double kConversionFactor = 5.0 / 9.0;
        static constexpr double kOffset = 459.67;
        static constexpr std::string_view kSymbol = "°F";
    };

    struct Rankine
    {
        static constexpr double kConversionFactor = 5.0 / 9.0;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "R";
    };

    struct KiloJoule_KiloGram
    {
        static constexpr double kConversionFactor = 1.0;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "kJ/kg";
    };

    struct BTU_PoundMass
    {
        static constexpr double kConversionFactor = 1.0 / 0.4299226139;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "BTU/lbm";
    };

    struct KiloJoule_KiloGram_Kelvin
    {
        static constexpr double kConversionFactor = 1.0;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "kJ/kg/K";
    };

    struct BTU_PoundMass_Rankine
    {
        static constexpr double kConversionFactor = 1.0 / 0.238845896627;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "BTU/lbm/°F";
    };

    struct CubicMeter_KiloGram
    {
        static constexpr double kConversionFactor = 1.0;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "m³/kg";
    };

    struct CubicFoot_PoundMass
    {
        static constexpr double kConversionFactor = 1.0 / 16.01846353;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "ft³/lbm";
    };

    struct KiloGram_CubicMeter
    {
        static constexpr double kConversionFactor = 1.0;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "kg/m³";
    };

    struct PoundMass_CubicFoot
    {
        static constexpr double kConversionFactor = 16.01846353;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "lbm/ft³";
    };

    struct Meter_Second
    {
        static constexpr double kConversionFactor = 1.0;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "m/s";
    };

    struct Feet_Second
    {
        static constexpr double kConversionFactor = 1.0 / 3.280839895013;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "ft/s";
    };

    struct IKelvin
    {
        static constexpr double kConversionFactor = 1.0;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "1/K";
    };

    struct IRankine
    {
        static constexpr double kConversionFactor = 1.0 / Rankine::kConversionFactor;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "1/R";
    };

    struct IPascal
    {
        static constexpr double kConversionFactor = 1.0;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "1/Pa";
    };

    struct IMegaPascal
    {
        static constexpr double kConversionFactor = 1.0 / MegaPascal::kConversionFactor;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "1/MPa";
    };

    struct IKiloPascal
    {
        static constexpr double kConversionFactor = 1.0 / KiloPascal::kConversionFactor;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "1/kPa";
    };

    struct IPSI
    {
        static constexpr double kConversionFactor = 1.0 / PSI::kConversionFactor;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "1/psi";
    };

    struct Watts_Meter_Kelvin
    {
        static constexpr double kConversionFactor = 1.0;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "W/m/K";
    };

    struct BTU_Hour_Foot_Rankine
    {
        // Clang-tidy thinks this is std::numbers::inv_sqrt3, which it is not.
        // NOLINTNEXTLINE(modernize-use-std-numbers)
        static constexpr double kConversionFactor = 1.0 / 0.577789316543;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "BTU/hr/ft/R";
    };

    struct Newton_Meter
    {
        static constexpr double kConversionFactor = 1.0;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "N/m";
    };

    struct PoundForce_Foot
    {
        static constexpr double kConversionFactor = 1.0 / (0.005710147098 * 12.0);
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "lbf/ft";
    };

    struct PascalSecond
    {
        static constexpr double kConversionFactor = 1.0;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "Pa-s";
    };

    struct PoundMass_Foot_Hour
    {
        static constexpr double kConversionFactor = 1.0 / 2419.088310502;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "lbm/ft/hr";
    };

    struct Dimensionless
    {
        static constexpr double kConversionFactor = 1.0;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "-";
    };

    struct Percent
    {
        static constexpr double kConversionFactor = 1.0 / 100.0;
        static constexpr double kOffset = 0.0;
        static constexpr std::string_view kSymbol = "%";
    };

    template <typename Unit>
    concept PressureUnit = std::same_as<Unit, Pascal> || std::same_as<Unit, MegaPascal> ||
        std::same_as<Unit, KiloPascal> || std::same_as<Unit, PSI>;

    template <typename Unit>
    concept TemperatureUnit = std::same_as<Unit, Kelvin> || std::same_as<Unit, Celsius> ||
        std::same_as<Unit, Fahrenheit> || std::same_as<Unit, Rankine>;

    template <typename Unit>
    concept SpecificEnergyUnit = std::same_as<Unit, KiloJoule_KiloGram> || std::same_as<Unit, BTU_PoundMass>;

    template <typename Unit>
    concept SpecificEntropyUnit =
        std::same_as<Unit, KiloJoule_KiloGram_Kelvin> || std::same_as<Unit, BTU_PoundMass_Rankine>;

    template <typename Unit>
    concept SpecificVolumeUnit = std::same_as<Unit, CubicMeter_KiloGram> || std::same_as<Unit, CubicFoot_PoundMass>;

    template <typename Unit>
    concept DensityUnit = std::same_as<Unit, KiloGram_CubicMeter> || std::same_as<Unit, PoundMass_CubicFoot>;

    template <typename Unit>
    concept VelocityUnit = std::same_as<Unit, Meter_Second> || std::same_as<Unit, Feet_Second>;

    template <typename Unit>
    concept ThermalExpansionUnit = std::same_as<Unit, IKelvin> || std::same_as<Unit, IRankine>;

    template <typename Unit>
    concept CompressibilityUnit = std::same_as<Unit, IPascal> || std::same_as<Unit, IMegaPascal> ||
        std::same_as<Unit, IKiloPascal> || std::same_as<Unit, IPSI>;

    template <typename Unit>
    concept ThermalConductivityUnit =
        std::same_as<Unit, Watts_Meter_Kelvin> || std::same_as<Unit, BTU_Hour_Foot_Rankine>;

    template <typename Unit>
    concept SurfaceTensionUnit = std::same_as<Unit, Newton_Meter> || std::same_as<Unit, PoundForce_Foot>;

    template <typename Unit>
    concept ViscosityUnit = std::same_as<Unit, PascalSecond> || std::same_as<Unit, PoundMass_Foot_Hour>;

    template <typename Unit>
    concept QualityUnit = std::same_as<Unit, Dimensionless> || std::same_as<Unit, Percent>;

    template <PressureUnit Unit> using PressureT = PropertyT<Unit>;
    template <TemperatureUnit Unit> using TemperatureT = PropertyT<Unit>;
    template <SpecificEnergyUnit Unit> using SpecificEnergyT = PropertyT<Unit>;
    template <SpecificEntropyUnit Unit> using SpecificEntropyT = PropertyT<Unit>;
    template <SpecificEntropyUnit Unit> using HeatCapacityT = PropertyT<Unit>;
    template <SpecificVolumeUnit Unit> using SpecificVolumeT = PropertyT<Unit>;
    template <DensityUnit Unit> using DensityT = PropertyT<Unit>;
    template <VelocityUnit Unit> using VelocityT = PropertyT<Unit>;
    template <ThermalExpansionUnit Unit> using ThermalExpansionT = PropertyT<Unit>;
    template <CompressibilityUnit Unit> using CompressibilityT = PropertyT<Unit>;
    template <SurfaceTensionUnit Unit> using SurfaceTensionT = PropertyT<Unit>;
    template <QualityUnit Unit> using QualityT = PropertyT<Unit>;
    template <QualityUnit Unit> using VoidT = PropertyT<Unit>;
    template <ViscosityUnit Unit> using ViscosityT = PropertyT<Unit>;
    template <ThermalConductivityUnit Unit> using ThermalConductivityT = PropertyT<Unit>;

    /**
     * @brief Calculates the mixture property from a mixture fraction
     *
     * @tparam Unit unit of property to mix
     * @param vapor_fraction vapor fraction, either mass fraction or volume fraction
     * @param liquid_phase liquid phase property
     * @param vapor_phase vapor phase property
     * @return PropertyT<Unit> mixed property
     */
    template <typename Unit>
    auto MixProperties(
        QualityT<Dimensionless> vapor_fraction, PropertyT<Unit> liquid_phase, PropertyT<Unit> vapor_phase
    ) -> PropertyT<Unit>
    {
        auto result = vapor_fraction.Value() * vapor_phase + (1.0 - vapor_fraction).Value() * liquid_phase;
        return PropertyT<Unit>{result};
    }

} // namespace fluid_props
