#include "exceptions/exceptions.hpp"
#include "interfaces/fluid.hpp"
#include "water/iapwsif97/backwards_equations/equations.hpp"
#include "water/iapwsif97/basic_equations/equations.hpp"
#include "water/iapwsif97/iapws_units.hpp"

namespace fluid_props::water
{

    /**
     * @brief A fluid region definition
     *
     * @tparam BasicEquationType basic equation type
     */
    template <class BasicEquationType, class BackwardsEquationType>
        requires Derived<BasicEquation, BasicEquationType> && Derived<BackwardEquation, BackwardsEquationType>
    class Region : public Fluid<IAPWSUnits>
    {
    public:
        Region() = default;

    public: // Mutable properties
        /**
         * @brief Pressure of fluid
         *
         * @return PressureT<MegaPascal>
         */
        auto Pressure() -> PressureT<MegaPascal> override { return this->pressure_; };

        /**
         * @brief Sets the fluid pressure
         *
         * @param pressure
         */
        void Pressure(PressureT<MegaPascal> pressure) override { this->pressure_ = pressure; };

        /**
         * @brief Enthalpy of fluid
         *
         * @return SpecificEnergyT<KiloJoule_KiloGram>
         */
        auto Enthalpy() -> SpecificEnergyT<KiloJoule_KiloGram> override { return this->basic_.Enthalpy(); };

        /**
         * @brief Sets fluid enthalpy
         *
         * @param enthalpy
         */
        void Enthalpy(SpecificEnergyT<KiloJoule_KiloGram> enthalpy) override { this->enthalpy_ = enthalpy; };

        /**
         * @brief Temperature of fluid
         *
         * @return TemperatureT<Kelvin>
         */
        auto Temperature() -> TemperatureT<Kelvin> override { return this->temperature_; };

        /**
         * @brief Sets fluid temperature
         *
         * @param temperature
         */
        void Temperature(TemperatureT<Kelvin> temperature) override
        {
            this->temperature_ = temperature;
            this->basic_.SetState(PressureT<MegaPascal>(this->pressure_), temperature);
        };

        /**
         * @brief Entropy of fluid
         *
         * @return SpecificEntropyT<KiloJoule_KiloGram_Kelvin>
         */
        auto Entropy() -> SpecificEntropyT<KiloJoule_KiloGram_Kelvin> override { return this->basic_.Entropy(); };

        /**
         * @brief Sets entropy of fluid
         *
         * @param entropy
         */
        void Entropy(SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) override {};

        /**
         * @brief Sets property combination of pressure and temperature
         *
         * @param pressure
         * @param temperature
         */
        void SetState(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature) override
        {
            this->Pressure(pressure);
            this->Temperature(temperature);
            this->basic_.SetState(pressure, temperature);
            this->validator_ = [this]() { return this->basic_.WithinValidRange(); };
        }

        /**
         * @brief Sets property combination of pressure and enthalpy
         *
         * @param pressure
         * @param enthalpy
         */
        void SetState(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) override
        {
            this->Pressure(pressure);
            this->Enthalpy(enthalpy);
            this->backwards_.SetState(pressure, enthalpy);
            this->validator_ = [this]() { return this->backwards_.WithinValidRange(); };
            this->Temperature(TemperatureT<Kelvin>(this->backwards_.Temperature()));
        }

        /**
         * @brief Sets property combination of pressure and enthalpy
         *
         * @param pressure
         * @param entropy
         */
        void SetState(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) override
        {
            this->Pressure(pressure);
            this->Entropy(entropy);
            this->backwards_.SetState(pressure, entropy);
            this->validator_ = [this]() { return this->backwards_.WithinValidRange(); };
            this->Temperature(TemperatureT<Kelvin>(this->backwards_.Temperature()));
        };

        /**
         * @brief Sets property combination of enthalpy and entropy
         *
         * @param enthalpy
         * @param entropy
         */
        void SetState(SpecificEnergyT<KiloJoule_KiloGram> enthalpy, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            override {};

        /**
         * @brief Checks that properties are withing the valid range
         *
         * @return true
         * @return false
         */
        auto WithinValidRange() -> bool { return this->validator_(); };

    public: // Derived properties
        /**
         * @brief Specific Volume of fluid
         *
         * @return Property
         */
        auto SpecificVolume() -> SpecificVolumeT<CubicMeter_KiloGram> override
        {
            return this->basic_.SpecificVolume();
        };

        /**
         * @brief Internal energy of fluid
         *
         * @return SpecificEnergyT<KiloJoule_KiloGram>
         */
        auto InternalEnergy() -> SpecificEnergyT<KiloJoule_KiloGram> override { return this->basic_.InternalEnergy(); };

        /**
         * @brief Isobaric heat capacity \f$C_p\f$ of fluid
         *
         * @return HeatCapacityT<KiloJoule_KiloGram_Kelvin>
         */
        auto IsobaricHeatCapacity() -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> override
        {
            return this->basic_.IsobaricHeatCapacity();
        };

        /**
         * @brief Isochoric heat capacity \f$C_v\f$
         *
         * @return HeatCapacityT<KiloJoule_KiloGram_Kelvin>
         */
        auto IsochoricHeatCapacity() -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> override
        {
            return this->basic_.IsochoricHeatCapacity();
        };

        /**
         * @brief Speed of sound of fluid
         *
         * @return VelocityT<Meter_Second>
         */
        auto SpeedOfSound() -> VelocityT<Meter_Second> override { return this->basic_.SpeedOfSound(); };

        /**
         * @brief Isobaric Cubic Expansion Coefficient
         *
         * @return ThermalExpansionT<IKelvin>
         */
        auto CubicExpansion() -> ThermalExpansionT<IKelvin> override { return this->basic_.CubicExpansion(); };

        /**
         * @brief Isothermal Compressibility
         *
         * @return CompressibilityT<IMegaPascal>
         */
        auto Compressibility() -> CompressibilityT<IMegaPascal> override { return this->basic_.Compressibility(); };

        /**
         * @brief Viscosity not implemented on a regional level. Will throw PropertyOutOfBounds
         *
         * @return ViscosityT<PascalSecond>
         */
        auto Viscosity() -> ViscosityT<PascalSecond> override
        {
            auto message = "Viscosity is not implemented on a regional level!";
            throw PropertyOutOfBounds(message);
        };

        auto ThermalConductivity() -> ThermalConductivityT<Watts_Meter_Kelvin> override
        {
            auto message = "Thermal conductivity is not implemented on a regional level!";
            throw PropertyOutOfBounds(message);
        };

        /**
         * @brief Density of fluid
         *
         * @return Property
         */
        auto Density() -> DensityT<KiloGram_CubicMeter> override { return DensityT<KiloGram_CubicMeter>{}; };

    private:
        PressureT<MegaPascal> pressure_;
        TemperatureT<Kelvin> temperature_;
        SpecificEnergyT<KiloJoule_KiloGram> enthalpy_;
        BasicEquationType basic_;
        BackwardsEquationType backwards_;
        Validator validator_;
    };

} // namespace fluid_props::water
