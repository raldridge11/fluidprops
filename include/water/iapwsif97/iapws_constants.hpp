#pragma once
#include "units/units.hpp"
namespace fluid_props::water
{
    /**
     * @brief Specific gas constant of ordinary water in \f$\frac{\text{kJ}}{\text{kgK}}\f$
     *
     */
    constexpr const SpecificEntropyT<KiloJoule_KiloGram_Kelvin> kSpecificGas{0.461526};

    /**
     * @brief Maximum temperature for subcooled liquid \f$\text{K}\f$
     *
     */
    constexpr const TemperatureT<Kelvin> kSubcooledMaxTemperature{623.15};

    /**
     * @brief Maximum pressure for non-high temperature regions \f$\text{MPa}\f$
     *
     */
    constexpr const PressureT<MegaPascal> kMaxPressure{100.0};

    /**
     * @brief Maximum pressure for high temperature region \f$\text{MPa}\f$
     *
     */
    constexpr const PressureT<MegaPascal> KHighMaxPressure{50.0};

    /**
     * @brief Critical point temperature in \f$\text{K}\f$
     *
     */
    constexpr const TemperatureT<Kelvin> kCriticalTemperature{647.096};

    /**
     * @brief Critical point pressure in \f$\text{MPa}\f$
     *
     */
    constexpr const PressureT<MegaPascal> kCriticalPressure{22.064};

    /**
     * @brief Minimum critical point pressure \f$\text{MPa}\f$
     *
     */
    constexpr const PressureT<MegaPascal> kMinCriticalPressure{16.5291643};

    /**
     * @brief Critical point density in \f$\frac{kg}{m^3}\f$
     *
     */
    constexpr const DensityT<KiloGram_CubicMeter> kCriticalDensity{322.0};

    /**
     * @brief Triple point temperature of water \f$K\f$
     *
     */
    constexpr const TemperatureT<Kelvin> kTriplePointTemperature{273.15};

    /**
     * @brief Triple point pressure of water \f$MPa\f$
     *
     */
    constexpr const PressureT<MegaPascal> kTriplePointPressure{611.213 / 1.0e6};

    /**
     * @brief Maximum range of validity for non-high temperature steam \f$K\f$
     *
     */
    constexpr const TemperatureT<Kelvin> kMaxTemperature{1073.15};

    /**
     * @brief Maximum range of validity for high temperature steam \f$K\f$
     *
     */
    constexpr const TemperatureT<Kelvin> kHighMaxTemperature{2273.15};

    /**
     * @brief Maximum temperature for super critical curve \f$K\f$
     *
     */
    constexpr const TemperatureT<Kelvin> kMaxTemperatureSuperCritical{863.15};

    /**
     * @brief Entropy at critical point \f$kJ/kgK\f$
     *
     */
    constexpr const SpecificEntropyT<KiloJoule_KiloGram_Kelvin> kCriticalEntropy{4.41202148223476};

} // namespace fluid_props::water
