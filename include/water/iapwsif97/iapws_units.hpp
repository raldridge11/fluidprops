#pragma once
#include "interfaces/fluid.hpp"
#include "units/units.hpp"

namespace fluid_props::water
{
    /**
     * @brief Units used for IAPWS standard.
     *
     */
    using IAPWSUnits = TwoPhaseFluidUnits<
        MegaPascal,
        Kelvin,
        KiloJoule_KiloGram,
        KiloJoule_KiloGram_Kelvin,
        CubicMeter_KiloGram,
        KiloGram_CubicMeter,
        Meter_Second,
        IKelvin,
        IMegaPascal,
        PascalSecond,
        Watts_Meter_Kelvin,
        Dimensionless,
        Dimensionless>;
} // namespace fluid_props::water
