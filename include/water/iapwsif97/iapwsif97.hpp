#pragma once
#include "interfaces/fluid.hpp"
#include "lazy_evaluator/lazy_evaluator.hpp"
#include "water/iapwsif97/backwards_equations/high_steam.hpp"
#include "water/iapwsif97/backwards_equations/liquid.hpp"
#include "water/iapwsif97/backwards_equations/steam.hpp"
#include "water/iapwsif97/backwards_equations/supercritical.hpp"
#include "water/iapwsif97/basic_equations/high_steam.hpp"
#include "water/iapwsif97/basic_equations/liquid.hpp"
#include "water/iapwsif97/basic_equations/steam.hpp"
#include "water/iapwsif97/basic_equations/supercritical.hpp"
#include "water/iapwsif97/iapws_units.hpp"
#include "water/iapwsif97/regions.hpp"
#include "water/iapwsif97/saturation.hpp"
#include <memory>

namespace fluid_props::water
{
    /**
     * @brief Implementation of the IAPWS Industrial Formulation 1997 properties
     * of ordinary water.
     *
     */
    class IAPWSIF97 : public TwoPhaseFluid<IAPWSUnits>
    {
    public:
        /**
         * @brief Construct a new IAPWSIF97 object
         *
         */
        IAPWSIF97() : saturation_(std::make_shared<IAPWSIF97Saturation>()) {};

        /**
         * @brief Construct a new IAPWSIF97 object from pressure and temperature.
         *
         * @param pressure
         * @param temperature
         */
        IAPWSIF97(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature);

        /**
         * @brief Construct a new IAPWSIF97 object from pressure and enthalpy.
         *
         * @param pressure
         * @param enthalpy
         */
        IAPWSIF97(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy);

        /**
         * @brief Construct a new IAPWSIF97 object from pressure and entropy.
         *
         * @param pressure
         * @param entropy
         */
        IAPWSIF97(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy);

        /**
         * @brief Construct a new IAPWSIF97 object from pressure and quality.
         *
         * @param pressure
         * @param quality
         */
        IAPWSIF97(PressureT<MegaPascal> pressure, QualityT<Dimensionless> quality);

        /**
         * @brief Construct a new IAPWSIF97 object from temperature and quality.
         *
         * @param temperature
         * @param quality
         */
        IAPWSIF97(TemperatureT<Kelvin> temperature, QualityT<Dimensionless> quality);

        /**
         * @brief Construct a new IAPWSIF97 object from enthalpy and entropy.
         *
         * @param enthalpy
         * @param entropy
         */
        IAPWSIF97(SpecificEnergyT<KiloJoule_KiloGram> enthalpy, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy);

    public: // Mutable Properties
        auto Pressure() -> PressureT<MegaPascal> override { return this->pressure_.Value(); };

        /**
         * @brief Sets pressure for saturation properties
         *
         * @param pressure
         */
        void Pressure(PressureT<MegaPascal> pressure) override;

        auto Enthalpy() -> SpecificEnergyT<KiloJoule_KiloGram> override { return this->enthalpy_.Value(); };

        /**
         * @brief Sets Enthalpy
         *
         * @param enthalpy
         */
        void Enthalpy(SpecificEnergyT<KiloJoule_KiloGram> enthalpy) override;

        auto Temperature() -> TemperatureT<Kelvin> override { return this->temperature_.Value(); };

        /**
         * @brief Sets temperature for saturation properties
         *
         * @param temperature
         */
        void Temperature(TemperatureT<Kelvin> temperature) override;

        auto Entropy() -> SpecificEntropyT<KiloJoule_KiloGram_Kelvin> override { return this->entropy_.Value(); };
        void Entropy(SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) override;

        auto Quality() -> QualityT<Dimensionless> override { return this->quality_.Value(); };
        void Quality(QualityT<Dimensionless> quality) override;

        void SetState(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature) override;
        void SetState(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) override;
        void SetState(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) override;
        void SetState(PressureT<MegaPascal> pressure, QualityT<Dimensionless> quality) override;
        void SetState(TemperatureT<Kelvin> temperature, QualityT<Dimensionless> quality) override;
        void SetState(SpecificEnergyT<KiloJoule_KiloGram> enthalpy, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            override;

    public: // Derived Properties
        auto SpecificVolume() -> SpecificVolumeT<CubicMeter_KiloGram> override
        {
            return this->specific_volume_.Value();
        };
        auto Density() -> DensityT<KiloGram_CubicMeter> override;
        auto InternalEnergy() -> SpecificEnergyT<KiloJoule_KiloGram> override
        {
            return this->internal_energy_.Value();
        };
        auto IsobaricHeatCapacity() -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> override
        {
            return this->isobaric_heat_capacity_.Value();
        };
        auto IsochoricHeatCapacity() -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> override
        {
            return this->isochoric_heat_capacity_.Value();
        };
        auto SpeedOfSound() -> VelocityT<Meter_Second> override { return this->speed_of_sound_.Value(); };
        auto CubicExpansion() -> ThermalExpansionT<IKelvin> override { return this->cubic_expansion_.Value(); };
        auto Compressibility() -> CompressibilityT<IMegaPascal> override { return this->compressibility_.Value(); };
        auto Viscosity() -> ViscosityT<PascalSecond> override { return this->viscosity_.Value(); };
        auto ThermalConductivity() -> ThermalConductivityT<Watts_Meter_Kelvin> override
        {
            return this->thermal_conductivity_.Value();
        };
        auto VoidFraction() -> VoidT<Dimensionless> override { return this->void_.Value(); };

    public: // Saturation Properties
        auto Saturation() -> std::shared_ptr<SaturationType> override { return this->saturation_; }

    private: // Members
        LazyEvaluator<PressureT<MegaPascal>> pressure_{};
        LazyEvaluator<TemperatureT<Kelvin>> temperature_{};
        LazyEvaluator<SpecificEnergyT<KiloJoule_KiloGram>> enthalpy_, internal_energy_;
        LazyEvaluator<SpecificEntropyT<KiloJoule_KiloGram_Kelvin>> entropy_{};
        LazyEvaluator<QualityT<Dimensionless>> quality_{};
        LazyEvaluator<VoidT<Dimensionless>> void_{};
        LazyEvaluator<SpecificVolumeT<CubicMeter_KiloGram>> specific_volume_{};
        LazyEvaluator<HeatCapacityT<KiloJoule_KiloGram_Kelvin>> isobaric_heat_capacity_{}, isochoric_heat_capacity_{};
        LazyEvaluator<VelocityT<Meter_Second>> speed_of_sound_{};
        LazyEvaluator<ThermalExpansionT<IKelvin>> cubic_expansion_{};
        LazyEvaluator<CompressibilityT<IMegaPascal>> compressibility_{};
        LazyEvaluator<ViscosityT<PascalSecond>> viscosity_{};
        LazyEvaluator<ThermalConductivityT<Watts_Meter_Kelvin>> thermal_conductivity_{};

        std::shared_ptr<IAPWSIF97Saturation> saturation_;

        Region<BasicLiquid, BackwardsLiquid> liquid_;
        Region<BasicSteam, BackwardsSteam> steam_;
        Region<BasicHighSteam, BackwardsHighSteam> highTemperature_;
        Region<BasicSuperCritical, BackwardsSuperCritical> supercritical_;
        Fluid *calculator_;

    private: // Helpers
        void reset();
        void set_calculator();
        void setup_mixture_calculations();
        void setup_transport_calculations();
        [[nodiscard]] auto get_valid_region() -> Fluid *;
    };

} // namespace fluid_props::water
