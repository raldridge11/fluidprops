#pragma once
#include "interfaces/fluid.hpp"
#include "lazy_evaluator/lazy_evaluator.hpp"
#include "water/iapwsif97/basic_equations/metastable.hpp"
#include "water/iapwsif97/iapws_units.hpp"

namespace fluid_props::water
{
    /**
     * @brief Implementation of the IAPWS Industrial Formulation 1997 properties
     * of metastable water.
     *
     * Base units are:
     *
     */
    class IAPWSIF97M : public Fluid<IAPWSUnits>
    {
    public:
        /**
         * @brief Construct a new IAPWSIF97M object
         *
         */
        IAPWSIF97M();

        /**
         * @brief Construct a new IAPWSIF97M object from pressure and temperature.
         *
         * @param pressure
         * @param temperature
         */
        IAPWSIF97M(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature);

        /**
         * @brief Construct a new IAPWSIF97M object from pressure and enthalpy.
         *
         * @param pressure
         * @param enthalpy
         */
        IAPWSIF97M(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy);

        /**
         * @brief Construct a new IAPWSIF97M object from pressure and entropy
         *
         * @param pressure
         * @param entropy
         */
        IAPWSIF97M(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy);

    public: // Mutable properties
        auto Pressure() -> PressureT<MegaPascal> override { return this->pressure_.Value(); };
        void Pressure(PressureT<MegaPascal> pressure) override;

        auto Enthalpy() -> SpecificEnergyT<KiloJoule_KiloGram> override { return this->enthalpy_.Value(); };
        void Enthalpy(SpecificEnergyT<KiloJoule_KiloGram> enthalpy) override;

        auto Temperature() -> TemperatureT<Kelvin> override { return this->temperature_.Value(); };
        void Temperature(TemperatureT<Kelvin> temperature) override;

        auto Entropy() -> SpecificEntropyT<KiloJoule_KiloGram_Kelvin> override { return this->entropy_.Value(); };
        void Entropy(SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) override;

        void SetState(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature) override;
        void SetState(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) override;
        void SetState(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) override;
        void SetState(SpecificEnergyT<KiloJoule_KiloGram> enthalpy, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            override {};

        /**
         * @brief Returns whether or not the set properties are in the valid range of the metastable equation.
         * The valid range is pressure from the triple-point pressure to 10 MPa and from the saturated vapor line
         * to the 5% equilibrium moisture line corresponding to a quality of 0.95.
         *
         * @return true if in valid range
         * @return false if outside of valid range
         */
        auto InValidRange() -> bool;

    public: // Derived properties
        auto SpecificVolume() -> SpecificVolumeT<CubicMeter_KiloGram> override
        {
            return this->specific_volume_.Value();
        };
        auto InternalEnergy() -> SpecificEnergyT<KiloJoule_KiloGram> override
        {
            return this->internal_energy_.Value();
        };
        auto IsobaricHeatCapacity() -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> override
        {
            return this->isobaric_heat_capacity_.Value();
        };
        auto IsochoricHeatCapacity() -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> override
        {
            return this->isochoric_heat_capacity_.Value();
        };
        auto SpeedOfSound() -> VelocityT<Meter_Second> override { return this->speed_of_sound_.Value(); };
        auto CubicExpansion() -> ThermalExpansionT<IKelvin> override { return this->cubic_expansion_.Value(); };
        auto Compressibility() -> CompressibilityT<IMegaPascal> override { return this->compressibility_.Value(); };
        auto Viscosity() -> ViscosityT<PascalSecond> override { return ViscosityT<PascalSecond>{}; };
        auto ThermalConductivity() -> ThermalConductivityT<Watts_Meter_Kelvin> override
        {
            return ThermalConductivityT<Watts_Meter_Kelvin>{};
        };
        auto Density() -> DensityT<KiloGram_CubicMeter> override;

    private:
        LazyEvaluator<PressureT<MegaPascal>> pressure_{};
        LazyEvaluator<TemperatureT<Kelvin>> temperature_{};
        LazyEvaluator<SpecificEnergyT<KiloJoule_KiloGram>> enthalpy_, internal_energy_;
        LazyEvaluator<SpecificEntropyT<KiloJoule_KiloGram_Kelvin>> entropy_{};
        LazyEvaluator<SpecificVolumeT<CubicMeter_KiloGram>> specific_volume_{};
        LazyEvaluator<HeatCapacityT<KiloJoule_KiloGram_Kelvin>> isobaric_heat_capacity_{}, isochoric_heat_capacity_{};
        LazyEvaluator<VelocityT<Meter_Second>> speed_of_sound_{};
        LazyEvaluator<ThermalExpansionT<IKelvin>> cubic_expansion_{};
        LazyEvaluator<CompressibilityT<IMegaPascal>> compressibility_{};
        MetaStable metastable_;

    private:
        void reset();
    };

} // namespace fluid_props::water
