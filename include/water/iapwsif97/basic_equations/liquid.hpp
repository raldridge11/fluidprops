#pragma once
#include "water/iapwsif97/basic_equations/equations.hpp"
#include "water/iapwsif97/curves/saturation.hpp"
namespace fluid_props::water
{
    /**
     * @brief The formulation for subcooled liquid region
     * The basic equation for the subcooled liquid region take on the form
     * of the specific Gibbs free energy \f$g\f$. In a dimensionless form, the
     * specific Gibbs free energy is given as,
     * \f[
     *  \frac{g(p,T)}{RT} = \gamma(\pi, \tau) = \sum_{i=1}^{34}
     * n_i\left(7.1-\pi\right)^{I_i}\left(\tau-1.222\right)^{J_i} \f] where \f$\pi=p/p^{*}\f$ and \f$\tau=T^{*}/T\f$ All
     * thermodynamic properties are the derived by using the appropriate combinations of dimensionless Gibbs free energy
     * and its derivatives:
     * \f[
     *  \gamma_{\pi} = \left[\frac{\partial \gamma}{\partial \pi}\right]_{\tau} = \sum_{i=1}^{34}
     * -n_iI_i\left(7.1-\pi\right)^{I_i-1}\left(\tau-1.222\right)^{J_i} \f] \f[ \gamma_{\tau} = \left[\frac{\partial
     * \gamma}{\partial \tau}\right]_{\pi} = \sum_{i=1}^{34}
     * n_i\left(7.1-\pi\right)^{I_i}J_{i}\left(\tau-1.222\right)^{J_i-1} \f]
     *
     */
    class BasicLiquid final : public BasicEquation
    {
    public:
        BasicLiquid() = default;
        BasicLiquid(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature)
        {
            this->SetState(pressure, temperature);
        }
        ~BasicLiquid() = default;

    public:
        void SetState(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature) override;

        /**
         * @brief Enthalpy of sub-cooled liquid
         * \f[
         *  h = g - T\left(\frac{\partial g}{\partial T}\right)_p = h(\pi, \tau) = RT\tau\gamma_{\tau}
         * \f]
         *
         * @return SpecificEnergyT<KiloJoule_KiloGram>
         */
        [[nodiscard]] auto Enthalpy() const -> SpecificEnergyT<KiloJoule_KiloGram> override;

        /**
         * @brief Entropy of sub-cooled liquid
         * \f[
         * s = -\left(\frac{\partial g}{\partial T}\right)_p = s(\pi, \tau) = R(\tau\gamma_{\tau} - \gamma)
         * \f]
         *
         * @return SpecificEntropyT<KiloJoule_KiloGram_Kelvin>
         */
        [[nodiscard]] auto Entropy() const -> SpecificEntropyT<KiloJoule_KiloGram_Kelvin> override;

        /**
         * @brief Internal energy of sub-cooled liquid
         * \f[
         * u = g - T\left(\frac{\partial g}{\partial T}\right)_p - p\left(\frac{\partial g}{\partial P}\right)_T =
         * RT(\tau\gamma_{\tau} - \pi\gamma_{\pi}) \f]
         * @return SpecificEnergyT<KiloJoule_KiloGram>
         */
        [[nodiscard]] auto InternalEnergy() const -> SpecificEnergyT<KiloJoule_KiloGram> override;

        /**
         * @brief Specific volume of sub-cooled liquid
         * \f[
         * \nu = \left(\frac{\partial g}{\partial p}\right)_{T} = \nu(\pi, \tau) = \frac{RT\pi\gamma_{\pi}}{P}
         * \f]
         *
         * @return SpecificVolumeT<CubicMeter_KiloGram>
         */
        [[nodiscard]] auto SpecificVolume() const -> SpecificVolumeT<CubicMeter_KiloGram> override;

        /**
         * @brief Heat capacity at constant pressure for subcooled liquid
         * \f[
         *  c_p = \left(\frac{\partial h}{\partial T}\right)_p = c_p(\pi, \tau) = -R\tau^2\gamma_{\tau\tau}
         * \f]
         *
         * @return HeatCapacityT<KiloJoule_KiloGram_Kelvin>
         */
        [[nodiscard]] auto IsobaricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> override;

        /**
         * @brief Heat capacity at constant volume for subcooled liquid
         * \f[
         *  c_v = \left(\frac{\partial u}{\partial T}\right)_v = c_v(\pi, \tau) = -R\tau^2\gamma_{\tau\tau} +
         * R\frac{(\gamma_{\pi} - \tau\gamma_{\pi\tau})^2}{\gamma_{\pi\pi}} \f]
         *
         * @return HeatCapacityT<KiloJoule_KiloGram_Kelvin>
         */
        [[nodiscard]] auto IsochoricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> override;

        /**
         * @brief Speed of sound for subcooled liquid
         * \f[
         * w = \nu \sqrt{-\frac{\partial p}{\partial \nu}_s}
         * w^2(\pi, \tau) = \frac{RT\gamma_{\pi}^2}{\frac{(\gamma_{\pi} -
         * \tau\gamma_{\pi\tau})^2}{\tau^2\gamma_{\tau\tau}} - \gamma_{\pi\pi}} \f]
         * @return VelocityT<Meter_Second>
         */
        [[nodiscard]] auto SpeedOfSound() const -> VelocityT<Meter_Second> override;

        /**
         * @brief Isobaric cubic expansion coefficient for subcooled liquid
         * \f[
         *  \alpha_v = \frac{1}{v}\left(\frac{\partial v}{\partial T}\right)_p = \alpha_v(\pi, \tau) =
         * \frac{1}{T}\left(1-\frac{\tau \gamma_{\pi \tau}}{\gamma_{\pi}}\right) \f]
         *
         * @return ThermalExpansionT<IKelvin>
         */
        [[nodiscard]] auto CubicExpansion() const -> ThermalExpansionT<IKelvin> override;

        /**
         * @brief Isothermal compressibility for subcooled liquid
         * \f[
         * \kappa_T = \frac{-1}{v}\left(\frac{\partial v}{\partial p}\right)_T = \kappa_T(\pi, \tau) =
         * \frac{-1}{p}\frac{\pi \gamma_{\pi \pi}}{\gamma_{\pi}} \f]
         *
         * @return CompressibilityT<IMegaPascal>
         */
        [[nodiscard]] auto Compressibility() const -> CompressibilityT<IMegaPascal> override;

        /**
         * @brief Checks if the provided temperature and pressure are in the valid range for the subcooled region
         * The range of validity is:
         * \f[
         * 273.15 K \le T \le 623.15 K
         * P_{sat}(T) \le P \le 100.0 MPa
         * \f]
         *
         * @return true
         * @return false
         */
        [[nodiscard]] auto WithinValidRange() const -> bool override;

    private:
        [[nodiscard]] auto gamma() const -> Property;
        [[nodiscard]] auto gamma_tau() const -> Property;
        [[nodiscard]] auto gamma_pi() const -> Property;
        [[nodiscard]] auto gamma_tau_tau() const -> Property;
        [[nodiscard]] auto gamma_pi_pi() const -> Property;
        [[nodiscard]] auto gamma_pi_tau() const -> Property;

    private:
        PressureT<MegaPascal> pressure_;
        TemperatureT<Kelvin> temperature_;
        NormalizedProperty pi_, tau_;
        NormalizedProperty pi_diff_, tau_diff_;
        SaturationCurve saturation_;

        static const Normalization kPStar, kTStar;

        static const Coefficient kN;
        static const Exponent kI, kJ;
    };

} // namespace fluid_props::water
