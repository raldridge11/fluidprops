#pragma once
#include "lazy_evaluator/lazy_evaluator.hpp"
#include "water/iapwsif97/basic_equations/equations.hpp"
#include "water/iapwsif97/iapws_constants.hpp"

namespace fluid_props::water
{
    /**
     * @brief The formulation for the supercritical region
     * The basic equation for the supercritical region takes on the form
     * of the specific Helmholtz free energy \f$f\f$. In a dimensionless form, the
     * specific Helmholtz free energy is given as,
     * \f[
     * \frac{f(\rho, T))}{RT} = \phi(\delta, \tau) = n_1 \ln\delta + \sum_{i=2}^{40} n_i\delta^{I_i}\tau^{J_i}
     * \f]
     * where \f$\delta=\rho/rho^{*}\f$ and \f$\tau=T^{*}/T\f$ and the starred quantities are the critical density
     * and temperature respectively. All thermodynamic properties are derived by using the appropriate combinations of
     * dimensionless Helmholtz free energy and its derivatives.
     */
    class BasicSuperCritical final : public BasicEquation
    {
    public:
        BasicSuperCritical() = default;
        BasicSuperCritical(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature)
        {
            this->SetState(pressure, temperature);
        }
        ~BasicSuperCritical() = default;

    public:
        /**
         * @brief Sets thermodynamic state pressure and temperature
         *
         * @param pressure pressure to set to
         * @param temperature  temperature to set to
         */
        void SetState(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature) override;

        /**
         * @brief Enthalpy of supercritical water
         * \f[
         * h = f - T\left(\frac{\partial f}{\partial T}\right)_{\rho} + \rho\left(\frac{\partial f}{\partial
         * \rho}\right)_{T} = RT(\tau \phi_{\tau} + \delta \phi_{\delta}) \f]
         *
         * @return SpecificEnergyT<KiloJoule_KiloGram>
         */
        [[nodiscard]] auto Enthalpy() const -> SpecificEnergyT<KiloJoule_KiloGram> override;

        /**
         * @brief Entropy of supercritical water
         * \f[
         * s = -\left(\frac{\partial f}{\partial T}\right)_{\rho} = R\left(\tau \phi_{\tau} - \phi \right)
         * \f]
         *
         * @return SpecificEntropyT<KiloJoule_KiloGram_Kelvin>
         */
        [[nodiscard]] auto Entropy() const -> SpecificEntropyT<KiloJoule_KiloGram_Kelvin> override;

        /**
         * @brief Internal Energy of supercritical water
         * \f[
         * u = f - T\left(\frac{\partial f}{\partial T}\right)_{\rho} = RT \tau \phi_{\tau}
         * \f]
         *
         * @return SpecificEnergyT<KiloJoule_KiloGram>
         */
        [[nodiscard]] auto InternalEnergy() const -> SpecificEnergyT<KiloJoule_KiloGram> override;

        /**
         * @brief Specific Volume of supercritical water
         *
         * Solved via iteration with the secant method from pressure and temperature
         * @return SpecificVolumeT<CubicMeter_KiloGram>
         */
        [[nodiscard]] auto SpecificVolume() const -> SpecificVolumeT<CubicMeter_KiloGram> override;

        /**
         * @brief Isobaric heat capacity calculation
         *
         * \f[
         * c_p = \left(\frac{\partial h}{\partial T}\right)_p = R\left[ -\tau^2\phi_{\tau\tau} +
         * \frac{\left(\delta\phi_{\delta}-\delta\tau\phi_{\delta\tau}\right)^2}{2\delta\tau_{\delta} +
         * \delta^2\phi_{\delta\delta}} \right]
         * \f]
         * @return HeatCapacityT<KiloJoule_KiloGram_Kelvin>
         */
        [[nodiscard]] auto IsobaricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> override;

        /**
         * @brief Isochoric heat capacity calculation
         *
         * \f[
         * c_v = \left(\frac{\partial h}{\partial T}\right)_p = -R\tau^2\phi_{\tau\tau}
         * \f]
         * @return HeatCapacityT<KiloJoule_KiloGram_Kelvin>
         */
        [[nodiscard]] auto IsochoricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> override;

        /**
         * @brief Speed of sound calculation
         * \f[
         *   w^2 = \left(\frac{\partial p}{\partial \rho}\right)_s = RT\left[2\delta\phi_{\delta} +
         * \delta^2\phi_{\delta\delta} -
         * \frac{\left(\delta\phi_{\delta}-\delta\tau\phi_{\delta\tau}\right)^2}{tau^2\phi_{\tau\tau}}\right]
         * \f]
         * @return VelocityT<Meter_Second>
         */
        [[nodiscard]] auto SpeedOfSound() const -> VelocityT<Meter_Second> override;

        /**
         * @brief Isobaric cubic expansion coefficient
         * \f[
         *  \alpha_v = \frac{1}{v}\left(\frac{\partial v}{\partial T}\right)_p = \alpha_v(\pi, \tau) =
         * \frac{1}{T}\left(\frac{\phi_{\delta} - \tau \phi_{\delta \tau}}{2\phi_{\delta} + \delta \phi_{\delta
         * \delta}}\right) \f]
         *
         * @return ThermalExpansionT<IKelvin>
         */
        [[nodiscard]] auto CubicExpansion() const -> ThermalExpansionT<IKelvin> override;

        /**
         * @brief Isothermal compressibility
         * \f[
         * \kappa_T = \frac{-1}{v}\left(\frac{\partial v}{\partial p}\right)_T = \kappa_T(\pi, \tau) =
         * \frac{-1}{\rho R T}\frac{1}{2\delta \phi_{\delta} + \delta^2 \phi_{\delta \delta}} \f]
         *
         * @return CompressibilityT<IMegaPascal>
         */
        [[nodiscard]] auto Compressibility() const -> CompressibilityT<IMegaPascal> override;

        /**
         * @brief Checks if provided temperature and pressure are in the valid range for the supercritical region
         * \f[
         *  623.15 K \le T \le 863.15 K
         *  p_{B,steam,super}(T) \le p \le 100 MPa
         * \f]
         *
         * @return true
         * @return false
         */
        [[nodiscard]] auto WithinValidRange() const -> bool override;

    private:
        [[nodiscard]] auto phi() const -> Property;

        [[nodiscard]] auto phi_delta() const -> Property;

        [[nodiscard]] auto phi_delta_delta() const -> Property;

        [[nodiscard]] auto phi_tau() const -> Property;

        [[nodiscard]] auto phi_tau_tau() const -> Property;

        [[nodiscard]] auto phi_delta_tau() const -> Property;

    private:
        PressureT<MegaPascal> pressure_;
        TemperatureT<Kelvin> temperature_;
        mutable LazyProperty density_;
        NormalizedProperty tau_;
        mutable LazyNormalizedProperty delta_{[this]() { return this->density_.Value() / kCriticalDensity; }};

        static const Coefficient kN1;
        static const Coefficient kN;
        static const Exponent kI, kJ;
    };
} // namespace fluid_props::water
