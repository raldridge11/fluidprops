#pragma once
#include "water/iapwsif97/basic_equations/equations.hpp"
#include "water/iapwsif97/curves/saturation.hpp"
#include "water/iapwsif97/curves/supercritical.hpp"

namespace fluid_props::water
{
    /**
     * @brief The basic equation for the steam region is a fundamental equation for the specific Gibbs
     * free energy \f$g\f$. It is expressed in a dimensionless form and is separated into an ideal gas part
     * and a residual part given as,
     * \f[
     *  \frac{g(p,T)}{RT} = \gamma(\pi, \tau) = \gamma^{o}(\pi, \tau) + \gamma^{r}(\pi, \tau)
     * \f]
     *
     * The equation for the ideal gas part of the Gibbs free energy is given as,
     * \f[
     * \gamma^{o} = \ln{\pi} + \sum_{i=1}^9 n^{o}_{i}\tau^{J^{o}_{i}}
     * \f]
     * where \f$\pi=\frac{p}{p^*}\f$ and \f$\tau=\frac{T^*}{T} \f$ with p* = 1 MPa and T* = 540 K.
     *
     * The equation for the residual part of the Gibbs free energy is given as,
     * \f[
     *  \gamma^{r} = \sum_{i=1}^{49} n_i \pi^{I_i}(\tau - 0.5)^{J_i}
     * \f]
     *
     * All thermodynamic properties can be derived from the Gibbs free energy by using combinations of the
     * ideal gas part and residual part along with their respective derivatives.
     */
    class BasicSteam final : public BasicEquation
    {
    public:
        BasicSteam() = default;
        BasicSteam(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature)
        {
            this->SetState(pressure, temperature);
        }
        ~BasicSteam() = default;

    public:
        void SetState(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature) override;

        /**
         * @brief Enthalpy of steam
         * \f[
         *   h = g - T\left(\frac{\partial g}{\partial T}\right)_p = h(\pi, \tau) = RT\tau(\gamma^{0}_{\tau} -
         * \gamma^{r}_{\tau})
         * \f]
         *
         * @return SpecificEnergyT<KiloJoule_KiloGram>
         */
        [[nodiscard]] auto Enthalpy() const -> SpecificEnergyT<KiloJoule_KiloGram> override;

        /**
         * @brief Entropy of steam
         * * \f[
         * s = -\left(\frac{\partial g}{\partial T}\right)_p = s(\pi, \tau) = R[(\tau(\gamma^{o}_{\tau} +
         * \gamma^{r}_{\tau}) - (\gamma^{o} + \gamma^{r})] \f]
         *
         * @return SpecificEntropyT<KiloJoule_KiloGram_Kelvin>
         */
        [[nodiscard]] auto Entropy() const -> SpecificEntropyT<KiloJoule_KiloGram_Kelvin> override;

        /**
         * @brief Internal energy of steam
         * \f[
         * u = g - T\left(\frac{\partial g}{\partial T}\right)_p - p\left(\frac{\partial g}{\partial P}\right)_T =
         * RT[\tau(\gamma^{o}_{\tau} + \gamma^{r}_{\tau}) - \pi(\gamma^{o}_{\pi} + \gamma^{r}_{\pi})] \f]
         * @return SpecificEnergyT<KiloJoule_KiloGram>
         */
        [[nodiscard]] auto InternalEnergy() const -> SpecificEnergyT<KiloJoule_KiloGram> override;

        /**
         * @brief Specific volume of steam
         * \f[
         * \nu = \left(\frac{\partial g}{\partial p}\right)_{T} = \nu(\pi, \tau) =
         * \frac{RT\pi(\gamma^{o}_{\pi}+\gamma^{r}_{\pi})}{P} \f]
         * @return SpecificVolumeT<CubicMeter_KiloGram>
         */
        [[nodiscard]] auto SpecificVolume() const -> SpecificVolumeT<CubicMeter_KiloGram> override;

        /**
         * @brief Heat capacity at constant pressure for steam
         * \f[
         *  c_p = \left(\frac{\partial h}{\partial T}\right)_p = c_p(\pi, \tau) = -R\tau^2(\gamma^{o}_{\tau\tau} +
         * \gamma^{r}_{\tau\tau}) \f]
         *
         * @return HeatCapacityT<KiloJoule_KiloGram_Kelvin>
         */
        [[nodiscard]] auto IsobaricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> override;

        /**
         * @brief Heat capacity at constant volume for steam
         * \f[
         *  c_v = \left(\frac{\partial u}{\partial T}\right)_v = c_v(\pi, \tau) = -R\tau^2(\gamma^{o}_{\tau\tau} +
         * \gamma^{r}_{\tau\tau}) - R\frac{(1+\pi\gamma^{r}_{\pi} -
         * \tau\pi\gamma^{r}_{\pi\tau})^2}{1-\pi^{2}\gamma^{r}_{\pi\pi}} \f]
         *
         * @return HeatCapacityT<KiloJoule_KiloGram_Kelvin>
         */
        [[nodiscard]] auto IsochoricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> override;

        /**
         * @brief Speed of sound for steam
         * \f[
         * w = \nu \sqrt{-\frac{\partial p}{\partial \nu}_s}
         * \f]
         * \f[
         * w^2(\pi, \tau) = \frac{1 + 2\pi\gamma^{r}_{\pi} + \pi^2(\gamma^{r}_{\pi})^2}
         * {1 - \pi^2\gamma^{r}_{\pi\pi} + \frac{(1+\pi\gamma^{r}_{\pi} -
         * \tau\pi\gamma^{r}_{\pi\tau})^2}{\tau^2(\gamma^{o}_{\tau\tau} + \gamma^{r}_{\tau\tau})}} \f]
         * @return VelocityT<Meter_Second>
         */
        [[nodiscard]] auto SpeedOfSound() const -> VelocityT<Meter_Second> override;

        /**
         * @brief Isobaric cubic expansion coefficient for steam
         * \f[
         *  \alpha_v = \frac{1}{v}\left(\frac{\partial v}{\partial T}\right)_p = \alpha_v(\pi, \tau) =
         * \frac{1}{T}\left(\frac{1 + \pi \gamma^r_{\pi} - \tau \pi \gamma^r_{\pi \tau}}{1 + \pi \gamma^r_{\pi}}\right)
         * \f]
         *
         * @return ThermalExpansionT<IKelvin>
         */
        [[nodiscard]] auto CubicExpansion() const -> ThermalExpansionT<IKelvin> override;

        /**
         * @brief Isothermal compressibility for steam
         * \f[
         * \kappa_T = \frac{-1}{v}\left(\frac{\partial v}{\partial p}\right)_T = \kappa_T(\pi, \tau) =
         * \frac{-1}{p}\frac{1-\pi^2 \gamma^r_{\gamma \gamma}}{1 + \pi \gamma^r_{\pi}} \f]
         *
         * @return CompressibilityT<IMegaPascal>
         */
        [[nodiscard]] auto Compressibility() const -> CompressibilityT<IMegaPascal> override;

        [[nodiscard]] auto WithinValidRange() const -> bool override;

    private:
        [[nodiscard]] auto ideal_gamma() const -> Property;
        [[nodiscard]] auto ideal_gamma_tau() const -> Property;
        [[nodiscard]] auto ideal_gamma_tau_tau() const -> Property;
        [[nodiscard]] auto ideal_gamma_pi() const -> Property;

        [[nodiscard]] auto residual_gamma() const -> Property;
        [[nodiscard]] auto residual_gamma_tau() const -> Property;
        [[nodiscard]] auto residual_gamma_tau_tau() const -> Property;
        [[nodiscard]] auto residual_gamma_pi() const -> Property;
        [[nodiscard]] auto residual_gamma_pi_pi() const -> Property;
        [[nodiscard]] auto residual_gamma_pi_tau() const -> Property;

    private:
        PressureT<MegaPascal> pressure_;
        TemperatureT<Kelvin> temperature_;
        NormalizedProperty pi_, tau_;
        NormalizedProperty tau_diff_;

        SaturationCurve saturation_;
        SuperCriticalCurve superCritical_;

        static const PressureT<MegaPascal> kPStar;
        static const TemperatureT<Kelvin> kTStar;

        static const Exponent kIdealJ, kResidualI, kResidualJ;

        static const Coefficient kIdealN, kResidualN;
    };

} // namespace fluid_props::water
