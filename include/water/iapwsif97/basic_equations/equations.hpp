#pragma once
#include "types/types.hpp"
#include "units/units.hpp"

namespace fluid_props::water
{
    /**
     * @brief Interface for a region's Basic Equation implementation
     *
     */
    class BasicEquation
    {
    public:
        /**
         * @brief Sets thermodynamic state pressure and temperature
         *
         * @param pressure pressure to set to
         * @param temperature  temperature to set to
         */
        virtual void SetState(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature) = 0;

        /**
         * @brief Enthalpy calculation
         *
         * @return SpecificEnergyT<KiloJoule_KiloGram>
         */
        [[nodiscard]] virtual auto Enthalpy() const -> SpecificEnergyT<KiloJoule_KiloGram> = 0;

        /**
         * @brief Entropy calculation
         *
         * @return SpecificEntropy<KiloJoule_KiloGram_Kelvin>
         */
        [[nodiscard]] virtual auto Entropy() const -> SpecificEntropyT<KiloJoule_KiloGram_Kelvin> = 0;

        /**
         * @brief Internal Energy calculation
         *
         * @return SpecificEnergyT<KiloJoule_KiloGram>
         */
        [[nodiscard]] virtual auto InternalEnergy() const -> SpecificEnergyT<KiloJoule_KiloGram> = 0;

        /**
         * @brief Specific Volume calculation
         *
         * @return SpecificVolumeT<CubicMeter_KiloGram>
         */
        [[nodiscard]] virtual auto SpecificVolume() const -> SpecificVolumeT<CubicMeter_KiloGram> = 0;

        /**
         * @brief Isobaric heat capacity calculation
         *
         * @return HeatCapacityT<KiloJoule_KiloGram_Kelvin>
         */
        [[nodiscard]] virtual auto IsobaricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> = 0;

        /**
         * @brief Isochoric heat capacity calculation
         *
         * @return HeatCapacityT<KiloJoule_KiloGram_Kelvin>
         */
        [[nodiscard]] virtual auto IsochoricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> = 0;

        /**
         * @brief Speed of sound calculation
         *
         * @return VelocityT<Meter_Second>
         */
        [[nodiscard]] virtual auto SpeedOfSound() const -> VelocityT<Meter_Second> = 0;

        /**
         * @brief Isobaric Cubic Expansion Coefficient
         *
         * @return ThermalExpansionT<IKelvin>
         */
        [[nodiscard]] virtual auto CubicExpansion() const -> ThermalExpansionT<IKelvin> = 0;

        /**
         * @brief Isothermal compressibility
         *
         * @return CompressibilityT<IMegaPascal>
         */
        [[nodiscard]] virtual auto Compressibility() const -> CompressibilityT<IMegaPascal> = 0;

        /**
         * @brief Checks if provided properties are in the valid range for this property
         *
         * @return Property
         */
        [[nodiscard]] virtual auto WithinValidRange() const -> bool = 0;
    };

} // namespace fluid_props::water
