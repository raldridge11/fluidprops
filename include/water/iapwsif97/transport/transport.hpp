#pragma once
#include "types/types.hpp"
#include "units/units.hpp"

namespace fluid_props::water
{
    /**
     * @brief The formulation for the dynamic viscosity is given as a function of density and temperature
     * and is expressed int the dimensionless form
     * \f[
     * \frac{\eta}{\eta^*} = \Psi(\delta, \theta) = \Psi_0(\theta)\Psi_1(\delta, \theta)
     * \f]
     * where \f$\delta=\rho/rho^*\f$ and \f$\theta=T/T^*\f$. The stared quantities are the density and temperature
     * at the critical point respectively and \f$\eta*\f$=1e-6 Pa-s.
     *
     * The first equation \f$\Psi_0(\theta)\f$ is the viscosity in the ideal-gas limit
     * \f[
     * \Psi_0(\theta) = \sqrt{\theta}\left[sum_{i=1}^4 n_i^o \theta^{1 - i}\right]^{-1}
     * \f]
     * and the second function is expressed as
     * \f[
     * \Psi_1(\theta, \delta)=exp\left[\delta \sum_{i=1}^{21} n_i (\delta -1)^{I_i}(\theta^{-1} - 1)^{J_i} \right]
     * \f]
     *
     */
    class IAPWSIF97Viscosity
    {

    public:
        IAPWSIF97Viscosity() = default;
        IAPWSIF97Viscosity(DensityT<KiloGram_CubicMeter> density, TemperatureT<Kelvin> temperature)
        {
            this->SetState(density, temperature);
        };
        ~IAPWSIF97Viscosity() = default;

    public:
        /**
         * @brief Sets the state variable density and temperature
         *
         * @param density
         * @param temperature
         */
        void SetState(DensityT<KiloGram_CubicMeter> density, TemperatureT<Kelvin> temperature);

        /**
         * @brief Viscosity at a given density and temperature
         *
         * @return ViscosityT<PascalSecond>
         */
        auto Viscosity() -> ViscosityT<PascalSecond>;

        /**
         * @brief Checks that properties are within the applicable range for IAPWSIF97
         *
         * @param pressure pressure state
         * @return true if in valid range
         * @return false if not in valid range
         */
        auto WithinValidRange(PressureT<MegaPascal> pressure) -> bool;

    private:
        [[nodiscard]] auto psi_0() const -> Property;
        [[nodiscard]] auto psi_1() const -> Property;

    private:
        DensityT<KiloGram_CubicMeter> density_;
        TemperatureT<Kelvin> temperature_;
        NormalizedProperty delta_, theta_;

        static const ViscosityT<PascalSecond> kEtaStar;
        static const Exponent kI0;
        static const Exponent kI;
        static const Exponent kJ;

        static const Coefficient kN0;
        static const Coefficient kN;
    };

    /**
     * @brief The formulation for thermal conductivity is given as a function of density and temperature
     * and is expressed in dimensionless form as,
     * \f[
     * \frac{\lambda(\rho, T)}{\lambda^*} = \Lambda(\delta, \theta) = \Lambda_0(\theta) + \Lambda_1(\delta) +
     * \Lambda_2(\delta, \theta) \f]
     * where \f$\delta=\rho/rho^*\f$ and \f$\theta=T/T^*\f$. The stared quantities are 317.7 kg/m**3 and 647/26 K
     * respectively, and \f$\lambda*\f$=1.0 W/mK.
     *
     * The functions \f$\Lambda_0(\theta)\f$ represents the thermal conductivity in the ideal-gas limit,
     * \f[
     * \Lambda_0(\theta) = \sqrt(\theta)\sum_{i=1}^4 n_i^0\theta^{i-1}
     * \f]
     *
     * The second function reads as,
     * \f[
     * \Lambda_1(\delta) = n_1 + n_2\delta +n_3 e^{n_4(\delta + n_5)^2}
     * \f]
     *
     * The third functions reads as,
     * \f[
     * \Lambda_2(\delta, \theta) = (n_1\theta^{-10} + n_2)\delta^{1.8}e^{n_3(1-\delta^{1.8})} +
     *       n_4A\delta^Be^{\left(\frac{B}{1+B}\right)\left(1-\delta^{1+B\right)} +
     *       n_5e^{n_6\theta^{1.5} + n_7\delta^{-5}}
     * \f]
     * where the functions \f$A\f$ and \f$B\f$ are
     * \f[
     * A(\theta) = 2 + n_8\left(\Delta \theta \right)^{-0.6} + n_{10}
     * \f]
     * \f[
     *  \begin{cases}
     *      \left( \Delta \theta \right)^{-1}, & \text{for} \theta \geq 1.0 \\
     *       n_9\left( \Delta \theta \right)^{-0.6}, & \text{for} \theta \lt 1.0
     *  \end{cases}
     * \f]
     * with
     * \f[
     * \Delta \theta = \lvert \theta -1 \rvert + n_{10}
     * \f]
     */
    class IAPWSIF97ThermalConductivity
    {
    public:
        IAPWSIF97ThermalConductivity() = default;
        IAPWSIF97ThermalConductivity(DensityT<KiloGram_CubicMeter> density, TemperatureT<Kelvin> temperature)
        {
            this->SetState(density, temperature);
        };
        ~IAPWSIF97ThermalConductivity() = default;

    public:
        /**
         * @brief Sets the state variable density and temperature
         *
         * @param density
         * @param temperature
         */
        void SetState(DensityT<KiloGram_CubicMeter> density, TemperatureT<Kelvin> temperature);

        /**
         * @brief Thermal conductivity at a given density and temperature
         *
         * @return ThermalConductivityT<Watts_Meter_Kelvin>
         */
        auto ThermalConductivity() -> ThermalConductivityT<Watts_Meter_Kelvin>;

        /**
         * @brief Checks that properties are within the applicable range for IAPWSIF97
         *
         * @param pressure pressure state
         * @return true if in valid range
         * @return false if not in valid range
         */
        auto WithinValidRange(PressureT<MegaPascal> pressure) -> bool;

    private:
        [[nodiscard]] auto lambda_0() const -> Property;
        [[nodiscard]] auto lambda_1() const -> Property;
        [[nodiscard]] auto lambda_2() const -> Property;

        [[nodiscard]] auto delta_theta() const -> Property;
        [[nodiscard]] auto factor_a() const -> Property;
        [[nodiscard]] auto factor_b() const -> Property;

    private:
        DensityT<KiloGram_CubicMeter> density_;
        TemperatureT<Kelvin> temperature_;
        NormalizedProperty delta_, theta_;

        static const TemperatureT<Kelvin> kTStar;
        static const DensityT<KiloGram_CubicMeter> kDStar;
        static const ThermalConductivityT<Watts_Meter_Kelvin> kLStar;

        static const Coefficient kNIdeal, kN1, kN2;

        static const Exponent kI;
    };

} // namespace fluid_props::water
