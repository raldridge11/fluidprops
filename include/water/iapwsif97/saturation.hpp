#pragma once
#include "interfaces/fluid.hpp"
#include "water/iapwsif97/basic_equations/liquid.hpp"
#include "water/iapwsif97/basic_equations/steam.hpp"
#include "water/iapwsif97/curves/saturation.hpp"
#include "water/iapwsif97/iapws_units.hpp"
#include <map>

namespace fluid_props::water
{
    template <typename T> using LazyPhaseMap = std::map<Phase, LazyEvaluator<T>>;

    using LazyEnthalpy = LazyEvaluator<SpecificEnergyT<KiloJoule_KiloGram>>;
    using LazyPhaseEnthalpy = LazyPhaseMap<SpecificEnergyT<KiloJoule_KiloGram>>;

    using LazyEntropy = LazyEvaluator<SpecificEntropyT<KiloJoule_KiloGram_Kelvin>>;
    using LazyPhaseEntropy = LazyPhaseMap<SpecificEntropyT<KiloJoule_KiloGram_Kelvin>>;

    using LazyInternalEnergy = LazyEvaluator<SpecificEnergyT<KiloJoule_KiloGram>>;
    using LazyPhaseInternalEnergy = LazyPhaseMap<SpecificEnergyT<KiloJoule_KiloGram>>;

    using LazySpecificVolume = LazyEvaluator<SpecificVolumeT<CubicMeter_KiloGram>>;
    using LazyPhaseSpecificVolume = LazyPhaseMap<SpecificVolumeT<CubicMeter_KiloGram>>;

    using LazyHeatCapacity = LazyEvaluator<HeatCapacityT<KiloJoule_KiloGram_Kelvin>>;
    using LazyPhaseHeatCapacity = LazyPhaseMap<HeatCapacityT<KiloJoule_KiloGram_Kelvin>>;

    using LazyVelocity = LazyEvaluator<VelocityT<Meter_Second>>;
    using LazyPhaseVelocity = LazyPhaseMap<VelocityT<Meter_Second>>;

    using LazyThermalExpansion = LazyEvaluator<ThermalExpansionT<IKelvin>>;
    using LazyPhaseThermalExpansion = LazyPhaseMap<ThermalExpansionT<IKelvin>>;

    using LazyCompressibility = LazyEvaluator<CompressibilityT<IMegaPascal>>;
    using LazyPhaseCompressibility = LazyPhaseMap<CompressibilityT<IMegaPascal>>;

    /**
     * @brief Implementation of the IAPWS Industrial Formulation 1997 saturation properties
     * of ordinary water.
     *
     * Base units are:
     *
     * | Property | Unit |
     * | ---------| ---- |
     * | Pressure | MPa |
     * | Temperature | K |
     * | Mass | kg |
     * | Length | m |
     * | Energy | kJ |
     */
    class IAPWSIF97Saturation : public Saturated<IAPWSUnits>
    {
    public:
        IAPWSIF97Saturation() = default;

    public: // mutable properties
        /**
         * @brief Pressure of fluid
         *
         * @return PressureT<MegaPascal>
         */
        auto Pressure() -> PressureT<MegaPascal> override { return this->pressure_; };

        /**
         * @brief Sets the fluid pressure
         *
         * @param pressure
         */
        void Pressure(PressureT<MegaPascal> pressure) override;

        /**
         * @brief Temperature of fluid
         *
         * @return TemperatureT<Kelvin>
         */
        auto Temperature() -> TemperatureT<Kelvin> override { return this->temperature_; };

        /**
         * @brief Sets fluid temperature
         *
         * @param temperature
         */
        void Temperature(TemperatureT<Kelvin> temperature) override;

    public: // Derived properties
        /**
         * @brief Saturated Enthalpy
         *
         * @param Phase phase to get saturated value of
         * @return SpecificEnergyT<KiloJoule_KiloGram>
         */
        auto Enthalpy(Phase phase) -> SpecificEnergyT<KiloJoule_KiloGram> override;

        /**
         * @brief Saturated Entropy
         *
         * @param Phase phase to get saturated value of
         * @return SpecificEntropyT<KiloJoule_KiloGram_Kelvin>
         */
        auto Entropy(Phase phase) -> SpecificEntropyT<KiloJoule_KiloGram_Kelvin> override;

        /**
         * @brief Saturated Specific Volume
         *
         * @param Phase phase to get saturated value of
         * @return Property
         */
        auto SpecificVolume(Phase phase) -> SpecificVolumeT<CubicMeter_KiloGram> override;

        /**
         * @brief Saturated Density
         *
         * @param Phase phase to get saturated value of
         * @return Property
         */
        auto Density(Phase phase) -> DensityT<KiloGram_CubicMeter> override;

        /**
         * @brief Saturated Internal Energy
         *
         * @param Phase phase to get saturated value of
         * @return SpecificEnergyT<KiloJoule_KiloGram>
         */
        auto InternalEnergy(Phase phase) -> SpecificEnergyT<KiloJoule_KiloGram> override;

        /**
         * @brief Saturated Isobaric Heat Capacity
         *
         * @param Phase phase to get saturated value of
         * @return HeatCapacityT<KiloJoule_KiloGram_Kelvin>
         */
        auto IsobaricHeatCapacity(Phase phase) -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> override;

        /**
         * @brief Saturated Isochoric Heat Capacity
         *
         * @param Phase phase to get saturated value of
         * @return HeatCapacityT<KiloJoule_KiloGram_Kelvin>
         */
        auto IsochoricHeatCapacity(Phase phase) -> HeatCapacityT<KiloJoule_KiloGram_Kelvin> override;

        /**
         * @brief Saturated Speed of Sound
         *
         * @param Phase phase to get saturated value of
         * @return VelocityT<Meter_Second>
         */
        auto SpeedOfSound(Phase phase) -> VelocityT<Meter_Second> override;

        /**
         * @brief Saturation Cubic Expansion coefficient
         *
         * @param phase phase to get saturated value of
         * @return ThermalExpansionT<IKelvin>
         */
        auto CubicExpansion(Phase phase) -> ThermalExpansionT<IKelvin> override;

        /**
         * @brief Fluid isothermal compressibility
         *
         * @param phase phase to get saturated value of
         * @return CompressibilityT<IMegaPascal>
         */
        auto Compressibility(Phase phase) -> CompressibilityT<IMegaPascal> override;

        /**
         * @brief Surface tension between liquid and vapor phase given by
         * \f[
         * \sigma/\sigma^* = B\tau^\mu\left(1 + b\tau\right)
         * \f]
         * where,
         * \f[
         *  \tau = 1 - T/T_c
         * \f]
         * and the constants are defined as,
         * \f$T_c\f$ = 647.096 K, \f$B\f$ = 235.8 mN/m, \f$b\f$=-0.625, \f$\mu\f$=1.256,ad \f$\sigma\f$ = 1.0e-3 N/m
         *
         * @return SurfaceTensionT<Newton_Meter>
         */
        auto SurfaceTension() -> SurfaceTensionT<Newton_Meter>;

    private:
        void set_phases(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature);
        void reset();

        template <typename T> void reset_map(LazyPhaseMap<T> &property)
        {
            for (auto &[key, value] : property)
            {
                value.Reset();
            }
        }
        auto sigma() -> SurfaceTensionT<Newton_Meter>;

    private:
        PressureT<MegaPascal> pressure_;
        TemperatureT<Kelvin> temperature_;
        BasicSteam steam_;
        BasicLiquid liquid_;
        SaturationCurve saturation_line_;

    private:
        LazyPhaseEnthalpy enthalpy_ = {
            {Phase::Liquid, LazyEnthalpy{[this]() { return this->liquid_.Enthalpy(); }}},
            { Phase::Vapor,  LazyEnthalpy{[this]() { return this->steam_.Enthalpy(); }}},
        };
        LazyPhaseEntropy entropy_ = {
            {Phase::Liquid, LazyEntropy{[this]() { return this->liquid_.Entropy(); }}},
            { Phase::Vapor,  LazyEntropy{[this]() { return this->steam_.Entropy(); }}},
        };
        LazyPhaseSpecificVolume specific_volume_ = {
            {Phase::Liquid, LazySpecificVolume{[this]() { return this->liquid_.SpecificVolume(); }}},
            { Phase::Vapor,  LazySpecificVolume{[this]() { return this->steam_.SpecificVolume(); }}},
        };
        LazyPhaseInternalEnergy internal_energy_ = {
            {Phase::Liquid, LazyInternalEnergy{[this]() { return this->liquid_.InternalEnergy(); }}},
            { Phase::Vapor,  LazyInternalEnergy{[this]() { return this->steam_.InternalEnergy(); }}},
        };
        LazyPhaseHeatCapacity isobaric_heat_capacity_ = {
            {Phase::Liquid, LazyHeatCapacity{[this]() { return this->liquid_.IsobaricHeatCapacity(); }}},
            { Phase::Vapor,  LazyHeatCapacity{[this]() { return this->steam_.IsobaricHeatCapacity(); }}},
        };
        LazyPhaseHeatCapacity isochoric_heat_capacity_ = {
            {Phase::Liquid, LazyHeatCapacity{[this]() { return this->liquid_.IsochoricHeatCapacity(); }}},
            { Phase::Vapor,  LazyHeatCapacity{[this]() { return this->steam_.IsochoricHeatCapacity(); }}},
        };
        LazyPhaseVelocity speed_of_sound_ = {
            {Phase::Liquid, LazyVelocity{[this]() { return this->liquid_.SpeedOfSound(); }}},
            { Phase::Vapor,  LazyVelocity{[this]() { return this->steam_.SpeedOfSound(); }}},
        };
        LazyPhaseThermalExpansion cubic_expansion_ = {
            {Phase::Liquid, LazyThermalExpansion{[this]() { return this->liquid_.CubicExpansion(); }}},
            { Phase::Vapor,  LazyThermalExpansion{[this]() { return this->steam_.CubicExpansion(); }}},
        };
        LazyPhaseCompressibility compressibility_ = {
            {Phase::Liquid, LazyCompressibility{[this]() { return this->liquid_.Compressibility(); }}},
            { Phase::Vapor,  LazyCompressibility{[this]() { return this->steam_.Compressibility(); }}},
        };

        LazyEvaluator<SurfaceTensionT<Newton_Meter>> surface_tension_{[this]() { return this->sigma(); }};
    };

} // namespace fluid_props::water
