#pragma once
#include "types/types.hpp"
#include "units/units.hpp"

namespace fluid_props::water
{

    using TemperatureEquation = std::function<TemperatureT<Kelvin>()>;
    /**
     * @brief Interface that defines a Backwards Equation
     *
     */
    class BackwardEquation
    {
    public:
        /**
         * @brief Sets thermodynamic states pressure and enthalpy
         *
         * @param pressure pressure to set to
         * @param enthalpy enthalpy to set to
         */
        virtual void SetState(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) = 0;

        /**
         * @brief Set thermodynamic states pressure and enthalpy
         *
         * @param pressure pressure to set to
         * @param entropy entropy to set to
         */
        virtual void SetState(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) = 0;

        /**
         * @brief Calculates temperature from provided thermodynamic properties
         *
         * @return TemperatureT<Kelvin>
         */
        [[nodiscard]] virtual auto Temperature() const -> TemperatureT<Kelvin> = 0;

        /**
         * @brief Checks that the provided properties within the valid applicability range of the backwards equation
         *
         * @return true
         * @return false
         */
        [[nodiscard]] virtual auto WithinValidRange() const -> bool = 0;
    };

} // namespace fluid_props::water
