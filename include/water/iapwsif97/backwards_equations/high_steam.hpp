#pragma once
#include "water/iapwsif97/backwards_equations/equations.hpp"

namespace fluid_props::water
{
    /**
     * @brief Backwards Equation formulation for high temperature steam
     * Applicability limits are pressures between Triple Point and 50 MPa,
     * and temperatures greater than the 1073.15 isotherm and less than the 2273.15 K
     * isotherm
     *
     */
    class BackwardsHighSteam final : public BackwardEquation
    {
    public:
        BackwardsHighSteam() = default;
        ~BackwardsHighSteam() = default;

    public:
        /**
         * @brief Sets thermodynamic states pressure and enthalpy
         *
         * @param pressure pressure to set to
         * @param enthalpy enthalpy to set to
         */
        void SetState(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) override;

        /**
         * @brief Set thermodynamic states pressure and enthalpy
         *
         * @param pressure pressure to set to
         * @param entropy entropy to set to
         */
        void SetState(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) override;

        /**
         * @brief Calculates temperature from provided thermodynamic properties
         *
         * @return TemperatureT<Kelvin>
         */
        [[nodiscard]] auto Temperature() const -> TemperatureT<Kelvin> override;

        /**
         * @brief Checks that the provided properties within the valid applicability range of the backwards equation
         *
         * @return true
         * @return false
         */
        [[nodiscard]] auto WithinValidRange() const -> bool override;

        /**
         * @brief Calculates the high temperature steam temperature from pressure and enthalpy. Backwards
         * equations for this region are not defined by the IAPWS, so temperature is found via the secant
         * method from the basic equation
         *
         * @param pressure pressure state for calculation
         * @param enthalpy enthalpy state for calculation
         * @return TemperatureT<Kelvin>
         */
        static auto BackwardTemperature(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
            -> TemperatureT<Kelvin>;

        /**
         * @brief Calculates the high temperature steam temperature from pressure and entropy. Backwards
         * equations for this region are not defined by the IAPWS, so temperature is found via the secant
         * method from the basic equation
         *
         * @param pressure pressure state for calculation
         * @param entropy entropy state for calculation
         * @return TemperatureT<Kelvin>
         */
        static auto BackwardTemperature(
            PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy
        ) -> TemperatureT<Kelvin>;

        /**
         * @brief Validates that the given pressure and enthalpy is contained in the
         * high temperature steam region
         *
         * @param pressure pressure to check
         * @param enthalpy enthalpy to check
         * @return true if in the high temperature steam region
         * @return false if not in the high temperature steam region
         */
        static auto Validate(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool;

        /**
         * @brief Validates that the given pressure and entropy is contained in the
         * high temperature steam region
         *
         * @param pressure pressure to check
         * @param entropy entropy to check
         * @return true if in the high temperature steam region
         * @return false if not in the high temperature steam region
         */
        static auto Validate(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            -> bool;

    private:
        TemperatureEquation equation_;
        Validator validator_;
    };
} // namespace fluid_props::water
