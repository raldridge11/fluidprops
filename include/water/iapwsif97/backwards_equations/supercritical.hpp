#pragma once
#include "types/types.hpp"
#include "water/iapwsif97/backwards_equations/equations.hpp"

namespace fluid_props::water
{
    /**
     * @brief Backwards Equation formulation for supercritical water.
     * Applicability limits are pressure between saturation and 100 MPa,
     * and temperatures greater than 623.15 K isotherm and less than the
     * steam <-> supercritical boundary isotherm.
     */
    class BackwardsSuperCritical final : public BackwardEquation
    {
    public:
        BackwardsSuperCritical() = default;
        ~BackwardsSuperCritical() = default;

    public:
        /**
         * @brief Sets thermodynamic states pressure and enthalpy
         *
         * @param pressure pressure to set to
         * @param enthalpy enthalpy to set to
         */
        void SetState(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) override;

        /**
         * @brief Set thermodynamic states pressure and enthalpy
         *
         * @param pressure pressure to set to
         * @param entropy entropy to set to
         */
        void SetState(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) override;

        /**
         * @brief Calculates temperature from provided thermodynamic properties
         *
         * @return TemperatureT<Kelvin>
         */
        [[nodiscard]] auto Temperature() const -> TemperatureT<Kelvin> override;

        /**
         * @brief Checks that the provided properties within the valid applicability range of the backwards equation
         *
         * @return true
         * @return false
         */
        [[nodiscard]] auto WithinValidRange() const -> bool override;

        /**
         * @brief Calculates the supercritical water temperature from pressure and enthalpy.
         * The region is split between two subregions defined by an equation that give enthalpy as a
         * function of pressure. Region A's temperature is calculated from
         * \f[
         * \frac{T_{sca}(p, h)}{T^*} = \sum_{i=1}^{31}(n_i(\pi+0.240)^{I_i}(\eta-0.615)^{J_i})
         * \f]
         * where \f$T^*\f$ = 760 K, f$\pi=P/P^*\f$, \f$\eta=h/h^*\f$, \f$P^*=\f$ 100 MPa and \f$h^*=\f$ 2300 kJ/kg.
         *
         * Region B's temperature is calculated from
         * \f[
         * \frac{T_{scb}(p, h)}{T^*} = \sum_{i=1}^{33}(n_i(\pi+0.298)^{I_i}(\eta-0.720)^{J_i})
         * \f]
         * where \f$T^*\f$ = 860 K, f$\pi=P/P^*\f$, \f$\eta=h/h^*\f$, \f$P^*=\f$ 100 MPa and \f$h^*=\f$ 2800 kJ/kg.
         *
         * All of the equations use different coefficients and exponent values
         *
         * @param pressure
         * @param enthalpy
         * @return TemperatureT<Kelvin>
         */
        static auto BackwardTemperature(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
            -> TemperatureT<Kelvin>;

        /**
         * @brief Calculates the supercritical water temperature from pressure and entropy.
         * The region is split between two subregions defined by the isentropic line at 4.412 kJ/kgK.
         * Region A's temperature is calculated from
         * \f[
         *  \frac{T_{sca}(p, s)}{T^*} = \sum_{i=1}^{33}(n_i(\pi+0.240)^{I_i}(\sigma-0.703)^{J_i})
         * \f]
         * where \f$T^*\f$ = 760 K, f$\pi=P/P^*\f$, \f$\eta=h/h^*\f$, \f$P^*=\f$ 100 MPa and \f$s^*=\f$ 4.4 kJ/kgK.
         *
         * Region B's temperature is calculated from
         *  \f[
         *  \frac{T_{sca}(p, s)}{T^*} = \sum_{i=1}^{28}(n_i(\pi+0.760)^{I_i}(\sigma-0.818)^{J_i})
         * \f]
         * where \f$T^*\f$ = 760 K, f$\pi=P/P^*\f$, \f$\eta=h/h^*\f$, \f$P^*=\f$ 100 MPa and \f$s^*=\f$ 5.3 kJ/kgK.
         *
         * @param pressure
         * @param entropy
         * @return TemperatureT<Kelvin>
         */
        static auto BackwardTemperature(
            PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy
        ) -> TemperatureT<Kelvin>;

        /**
         * @brief Validates that the given pressure and enthalpy is contained in the supercritical
         * region
         *
         * @param pressure pressure to check
         * @param enthalpy enthalpy to check
         * @return true if in the supercritical region
         * @return false if not in the supercritical region
         */
        static auto Validate(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool;

        /**
         * @brief Validates that the given pressure and entropy is contained in the supercritical
         * region
         *
         * @param pressure pressure to check
         * @param entropy entropy to check
         * @return true if in the supercritical region
         * @return false if not in the supercritical region
         */
        static auto Validate(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            -> bool;

    private:
        TemperatureEquation equation_;
        Validator validator_;
    };
} // namespace fluid_props::water
