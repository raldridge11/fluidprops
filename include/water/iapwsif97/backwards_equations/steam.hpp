#pragma once
#include "types/types.hpp"
#include "water/iapwsif97/backwards_equations/equations.hpp"

namespace fluid_props::water
{
    /**
     * @brief Backwards Equation formulation for steam
     * Applicability limits are pressures between Triple Point and 100 MPa,
     * and temperatures greater than saturation, greater than the supercritical
     * boundary and less than the 1073.15 K isotherm
     *
     */
    class BackwardsSteam final : public BackwardEquation
    {
    public:
        BackwardsSteam() = default;
        ~BackwardsSteam() = default;

    public:
        /**
         * @brief Sets thermodynamic states pressure and enthalpy
         *
         * @param pressure pressure to set to
         * @param enthalpy enthalpy to set to
         */
        void SetState(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) override;

        /**
         * @brief Set thermodynamic states pressure and enthalpy
         *
         * @param pressure pressure to set to
         * @param entropy entropy to set to
         */
        void SetState(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) override;

        /**
         * @brief Calculates temperature from provided thermodynamic properties
         *
         * @return TemperatureT<Kelvin>
         */
        [[nodiscard]] auto Temperature() const -> TemperatureT<Kelvin> override;

        /**
         * @brief Checks that the provided properties within the valid applicability range of the backwards equation
         *
         * @return true
         * @return false
         */
        [[nodiscard]] auto WithinValidRange() const -> bool override;

        /**
         * @brief Calculates steam temperature from pressure and enthalpy.
         * The regions is split between three subregions
         * Region A covers the limit of the triple point pressure to 4 MPa
         * and between saturation and the the 1073.15 K isotherm. Region A's
         * temperature is calculated from
         * \f[
         * \frac{T_{steamA}(p, h)}{T^*} = \theta(\pi, \eta) \sum_{i=1}^{34}n_i\pi^{I_i}(\eta - 2.1)^{J_i}
         * \f]
         * where \f$\pi=P/P^*\f$, \f$\eta=h/h^*\f$, \f$P^*=\f$ 1 MPa and \f$h^*=\f$ 2000 kJ/kg.
         *
         * Region B covers the pressures between 4 MPa and the maximum pressure 100 MPa. With enthalpies
         * above saturation and enthalpies above the isentropic line of 5.85 kJ/kgK.
         * * \f[
         * \frac{T_{steamB}(p, h)}{T^*} = \theta(\pi, \eta) \sum_{i=1}^{38}n_i(\pi-2.0)^{I_i}(\eta - 2.6)^{J_i}
         * \f]
         *
         * Region C covers the rest of the steam region up to the supercritical curve
         * * \f[
         * \frac{T_{steamC}(p, h)}{T^*} = \theta(\pi, \eta) \sum_{i=1}^{23}n_i(\pi+25)^{I_i}(\eta - 1.8)^{J_i}
         * \f]
         *
         * All of the equations use different coefficients and exponent values
         *
         * @param pressure pressure state for calculation
         * @param enthalpy enthalpy state for calculation
         * @return TemperatureT<Kelvin>
         */
        static auto BackwardTemperature(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthlapy)
            -> TemperatureT<Kelvin>;

        /**
         * @brief Calculates steam temperature from pressure and entropy.
         * The regions is split between three subregions
         * Region A covers the limit of the triple point pressure to 4 MPa
         * and between saturation and the the 1073.15 K isotherm. Region A's
         * temperature is calculated from
         * \f[
         * \frac{T_{steamA}(p, s)}{T^*} = \theta(\pi, \sigma) \sum_{i=1}^{46}n_i\pi^{I_i}(\sigma - 2)^{J_i}
         * \f]
         * where \f$\pi=P/P^*\f$, \f$\sigma=s/s^*\f$, \f$P^*=\f$ 1 MPa and \f$s^*=\f$ 2 kJ/kgK.
         *
         * Region B covers the pressures between 4 MPa and the maximum pressure 100 MPa. With enthalpies
         * above saturation and enthalpies above the isentropic line of 5.85 kJ/kgK.
         * * \f[
         * \frac{T_{steamB}(p, s)}{T^*} = \theta(\pi, \sigma) \sum_{i=1}^{44}n_i\pi^{I_i}(10 - \sigma)^{J_i}
         * \f]
         * where \f$s^*=\f$ 0.7853 kJ/kgK.
         *
         * Region C covers the rest of the steam region up to the supercritical curve
         * * \f[
         * \frac{T_{steamC}(p, s)}{T^*} = \theta(\pi, \sigma) \sum_{i=1}^{30}n_i\pi^{I_i}(2.0 - \sigma)^{J_i}
         * \f]
         * where \f$s^*=\f$ 2.9251 kJ/kgK.
         *
         * All of the equations use different coefficients and exponent values
         *
         * @param pressure pressure state for calculation
         * @param enthalpy enthalpy state for calculation
         * @return TemperatureT<Kelvin>
         */
        static auto BackwardTemperature(
            PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy
        ) -> TemperatureT<Kelvin>;

        /**
         * @brief Validates that the given pressure and enthalpy is contained in the
         * steam region
         *
         * @param pressure pressure to check
         * @param enthalpy enthalpy to check
         * @return true if in the steam region
         * @return false if not in the steam region
         */
        static auto Validate(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool;

        /**
         * @brief Validates that the given pressure and entropy is contained in the
         * steam region
         *
         * @param pressure pressure to check
         * @param entropy entropy to check
         * @return true if in the steam region
         * @return false if not in the steam region
         */
        static auto Validate(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            -> bool;

    private:
        TemperatureEquation equation_;
        Validator validator_;
    };

} // namespace fluid_props::water
