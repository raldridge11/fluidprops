#pragma once
#include "water/iapwsif97/backwards_equations/equations.hpp"

namespace fluid_props::water
{
    /**
     * @brief Backwards Equation formulation for subcooled liquid
     * Applicability limits are temperatures between the 273.15 K and 623.15 K isotherms
     * And pressures between saturation and 100 MPa
     */
    class BackwardsLiquid final : public BackwardEquation
    {
    public:
        BackwardsLiquid() = default;
        ~BackwardsLiquid() = default;

    public:
        /**
         * @brief Sets thermodynamic states of pressure and enthalpy
         *
         * @param pressure
         * @param enthalpy
         */
        void SetState(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) override;

        /**
         * @brief Sets thermodynamic states of pressure and entropy
         *
         * @param pressure
         * @param entropy
         */
        void SetState(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) override;

        /**
         * @brief Calculates temperature from provided thermodynamic properties
         *
         * @return TemperatureT<Kelvin>
         */
        [[nodiscard]] auto Temperature() const -> TemperatureT<Kelvin> override;

        /**
         * @brief Checks that the provided properties within the valid applicability range of the backwards equation
         *
         * @return true
         * @return false
         */
        [[nodiscard]] auto WithinValidRange() const -> bool override;

        /**
         * @brief Liquid temperature from pressure and enthalpy
         * \f[
         * T(p,h) = T^{*}\sum_{i=1}^{20}n_i\pi^{I_i}\left(\eta + 1\right)^{J_i}
         * \f]
         * Where \f$T^{*}\f$ = 1.0 K, \f$\eta=h/h^*\f$, \f$h^{*}\f$=2500 kJ/kg, \f$P/P^{*}\f$, and \f$P^{*}\f$ = 1 MPa
         *
         * @param pressure pressure state
         * @param enthalpy enthalpy state
         * @return TemperatureT<Kelvin> temperature
         */
        static auto BackwardTemperature(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
            -> TemperatureT<Kelvin>;

        /**
         * @brief Liquid temperature from pressure and entropy
         * \f[
         * T(p,s) = T^{*}\sum_{i=1}^{20}n_i\pi^{I_i}\left(\sigma + 2\right)^{J_i}
         * \f]
         * Where \f$T^{*}\f$ = 1.0 K, \f$\sigma=s/s^*\f$, \f$s^{*}\f$=1 kJ/kgK, \f$P/P^{*}\f$, and \f$P^{*}\f$ = 1 MPa
         *
         * @param pressure pressure state
         * @param entropy entropy state
         * @return TemperatureT<Kelvin> temperature
         */
        static auto BackwardTemperature(
            PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy
        ) -> TemperatureT<Kelvin>;

        /**
         * @brief Validates that pressure and enthalpy will be in the pressure range of
         * 611.213 Pa <= P <= 100.0 MPa, between the the 273.15K and 623.15 K isotherms, and
         * below saturation
         *
         * @param pressure pressure to check
         * @param enthalpy enthalpy to check
         * @return true if in liquid region
         * @return false not in liquid region
         */
        static auto Validate(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool;

        /**
         * @brief Validates that pressure and entropy will be in the pressure range of
         * 611.213 Pa <= P <= 100.0 MPa, between the the 273.15K and 623.15 K isotherms, and
         * below saturation
         *
         * @param pressure pressure to check
         * @param entropy entropy to check
         * @return true if in liquid region
         * @return false not in liquid region
         */
        static auto Validate(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            -> bool;

    private:
        TemperatureEquation equation_;
        Validator validator_;
    };

} // namespace fluid_props::water
