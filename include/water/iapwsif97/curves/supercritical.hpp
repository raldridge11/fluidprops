#pragma once
#include "types/types.hpp"
#include "units/units.hpp"

namespace fluid_props::water
{
    /**
     * @brief Describes the line between steam and super critical fluid
     * Roughly describes an isentropic line between entropy values of
     *  5.047 \f$\frac{kJ}{kgK}\f$ and 5.261 \f$\frac{kJ}{kgK}\f$.
     *
     */
    class SuperCriticalCurve
    {

    public:
        SuperCriticalCurve() = default;
        explicit SuperCriticalCurve(PressureT<MegaPascal> pressure) { this->SetState(pressure); }
        explicit SuperCriticalCurve(TemperatureT<Kelvin> temperature) { this->SetState(temperature); }
        ~SuperCriticalCurve() = default;

    public:
        /**
         * @brief Sets pressure
         *
         * @param pressure
         */
        void SetState(PressureT<MegaPascal> pressure);

        /**
         * @brief Sets temperature
         *
         * @param temperature
         */
        void SetState(TemperatureT<Kelvin> temperature);

    public:
        /**
         * @brief Pressure on supercritical line given by the equation
         * \f$[
         * \pi = n_1 + n_2\theta + n+3\theta^2
         * ]\f$
         * where \f$\pi = \frac{p}{p^*\F$ and \f$\theta=\frac{T}{T^*}\f$
         * @return const PressureT<MegaPascal>
         */
        [[nodiscard]] auto Pressure() const -> PressureT<MegaPascal>;

        /**
         * @brief Temperature on supercritical line given by the equation
         * \f$[
         * \theta = n_4 + \sqrt{\frac{\pi - n_5}{n_3}}
         * ]\f$
         * where \f$\pi = \frac{p}{p^*\f$ and \f$\theta=\frac{T}{T^*}\f$
         * @return const TemperatureT<Kelvin>
         */
        [[nodiscard]] auto Temperature() const -> TemperatureT<Kelvin>;

    private:
        PressureT<MegaPascal> pressure_;
        TemperatureT<Kelvin> temperature_;
        NormalizedProperty theta_, pi_;

        static const PressureT<MegaPascal> kPStar;
        static const TemperatureT<Kelvin> kTStar;
        static const Coefficient kN;
    };

    /**
     * @brief Describes the line between the A and B subregion of the SuperCritical region
     * in the backwards formulation
     *
     */
    class SuperCriticalBoundary
    {
    public:
        SuperCriticalBoundary() = default;
        explicit SuperCriticalBoundary(PressureT<MegaPascal> pressure) { this->SetState(pressure); };
        ~SuperCriticalBoundary() = default;

    public:
        /**
         * @brief Sets pressure
         *
         * @param pressure
         */
        void SetState(PressureT<MegaPascal> pressure);

    public:
        /**
         * @brief Enthalpy on supercritical boundary given by the equation
         * \f$[
         * \eta(\pi) = \sum_{i=0}^{3}n_i\pi^{i}
         * ]\f$
         * where \f$\pi = \frac{p}{p^*}\f$ and \f$\eta = \frac{h}{h^*}\f$
         * @return SpecificEnergyT<KiloJoule_KiloGram>
         */
        [[nodiscard]] auto Enthalpy() const -> SpecificEnergyT<KiloJoule_KiloGram>;

    private:
        PressureT<MegaPascal> pressure_;
        NormalizedProperty pi_;

        static const PressureT<MegaPascal> kPStar;
        static const SpecificEnergyT<KiloJoule_KiloGram> kHStar;
        static const Coefficient kN;
    };

} // namespace fluid_props::water
