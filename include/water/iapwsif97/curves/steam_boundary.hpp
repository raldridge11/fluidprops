#pragma once
#include "types/types.hpp"
#include "units/units.hpp"
namespace fluid_props::water
{
    /**
     * @brief Backwards Equation formulation for the steam region boundary between
     * subregions B and C.
     */
    class SteamBoundaryCurve
    {
    public:
        SteamBoundaryCurve() = default;

        explicit SteamBoundaryCurve(PressureT<MegaPascal> pressure) { this->SetState(pressure); };

        explicit SteamBoundaryCurve(SpecificEnergyT<KiloJoule_KiloGram> enthalpy) { this->SetState(enthalpy); };

        ~SteamBoundaryCurve() = default;

    public:
        /**
         * @brief Sets pressure state
         *
         * @param pressure
         */
        void SetState(PressureT<MegaPascal> pressure);

        /**
         * @brief Sets enthalpy state
         *
         * @param enthalpy
         */
        void SetState(SpecificEnergyT<KiloJoule_KiloGram> enthalpy);

        /**
         * @brief Pressure from enthalpy given by
         * * \f[
         * \frac{p_{2bc}(h)}{p^*} = \pi(\eta) = n_1 +n_2\eta+n_3|eta^2
         * ]\f
         * where \f$\pi=p/p*\f$, \f$p^*=\f$ 1 MPa NS \f$\eta=h/h^*\f$, and \f$h^*=\f$ 1 kJ/kg
         *
         * @return PressureT<MegaPascal>
         */
        [[nodiscard]] auto Pressure() const -> PressureT<MegaPascal>;

        /**
         * @brief Enthalpy from pressure given by
         * \f[
         * \frac{h_{2bc}(p)}{h^*} = \eta(\pi) = n_4 + \left(\frac{\pi -n_5}{n_3}\right)^{0.5}
         * ]\f
         * where \f$\pi=p/p*\f$, \f$p^*=\f$ 1 MPa NS \f$\eta=h/h^*\f$, and \f$h^*=\f$ 1 kJ/kg
         *
         * @return SpecificEnergyT<KiloJoule_KiloGram>
         */
        [[nodiscard]] auto Enthalpy() const -> SpecificEnergyT<KiloJoule_KiloGram>;

    private:
        PressureT<MegaPascal> pressure_;
        SpecificEnergyT<KiloJoule_KiloGram> enthalpy_;
        NormalizedProperty pi_, eta_;

        static const PressureT<MegaPascal> kPStar;
        static const SpecificEnergyT<KiloJoule_KiloGram> kHStar;

        static const Coefficient kN;

        static const PressureT<MegaPascal> kMinPressure;
        static const SpecificEnergyT<KiloJoule_KiloGram> kMinEnthalpy, kMaxEnthalpy;
    };

} // namespace fluid_props::water
