#pragma once
#include "types/types.hpp"
#include "units/units.hpp"

namespace fluid_props::water
{
    /**
     * @brief Basic Equations formulation for saturation line
     * The equation for describing the saturation line is an implicit quadratic equation which can
     * be solved for either saturation pressure or temperature given as,
     * \f[
     * 0 = \beta^2\theta^2 + n_1\beta^2\theta+n_2\beta^2 + n_3\beta\theta^2 + n_4\beta\theta + n_5\beta + n_6\theta^2 +
     * n_7\theta + n_8 \f] where \f[ \beta = \left(\frac{p_s}{p^*}\right)^1/4 \f] and \f[ \theta = \frac{T_s}{T^*} +
     * \frac{n_9}{\left(T_s/T^*\right) - n_{10}} \f] with \f$p^*=1.0 \text{MPa}\f$ and \f$T^*=1.0 \text{K}\f$ The
     * coefficients \f$n_i\f$ are tabulated. The equation is valid from 273.15 K to 647.096 K.
     */
    class SaturationCurve
    {
    public:
        SaturationCurve() = default;
        explicit SaturationCurve(PressureT<MegaPascal> pressure) { this->SetState(pressure); }
        explicit SaturationCurve(TemperatureT<Kelvin> temperature) { this->SetState(temperature); }
        ~SaturationCurve() = default;

        /**
         * @brief Sets pressure
         *
         * @param pressure
         */
        void SetState(PressureT<MegaPascal> pressure);

        /**
         * @brief Sets temperature
         *
         * @param temperature
         */
        void SetState(TemperatureT<Kelvin> temperature);

        /**
         * @brief Saturation pressure from solving the implicit quadratic equation
         * \f[
         * p = p^*\left[\frac{2C}{-B +\left(B^2 - 4AC\right)^{1/2}}\right]
         * \f]
         * where
         * \f[
         * A = \theta^2 + n_1\theta + n_2
         * \f]
         * \f[
         * B = n_3\theta^2 + n_4\theta + n_5
         * \f]
         * \f[
         * C = n_6\theta^2 + n_7\theta + n_8
         * \f]
         *
         * @return PressureT<MegaPascal>
         */
        [[nodiscard]] auto Pressure() const -> PressureT<MegaPascal>;

        /**
         * @brief Saturation temperature from solving the implicit quadratic equation
         * \f[
         * T = T^*\frac{n_{10} + D - \left[\left(n_{10} + D\right)^2 - 4\left(n_9 + n_{10}D\right)\right]^{1/2}}{2}
         * \f]
         * where
         * \f[
         * D = \frac{2G}{-F - \left(F^2 - 4EG\right)^{1/2}}
         * \f]
         * \f[
         * E = \beta^2 + n_3\beta + n_6
         * F = n_1\beta^2 + n_4\beta + n_7
         * G = n_2\beta^2 + n_5\beta + n_8
         * \f]
         *
         * @return TemperatureT<Kelvin>
         */
        [[nodiscard]] auto Temperature() const -> TemperatureT<Kelvin>;

    private:
        void calculate_theta();
        void calculate_beta();
        [[nodiscard]] auto coefficient(double value, const Coefficient &coefficients) const -> double;

    private:
        PressureT<MegaPascal> pressure_;
        TemperatureT<Kelvin> temperature_;
        NormalizedProperty theta_, beta_;

        static const PressureT<MegaPascal> kPStar;
        static const TemperatureT<Kelvin> kTStar;
        static const Coefficient kN;
    };

} // namespace fluid_props::water
