#pragma once
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

namespace fluid_props
{
    /**
     * @brief Logging wrapper around `spdlog` for FluidProps++
     *
     */
    class Logger
    {
    public:
        /**
         * @brief Gets the logger.
         * If the logger has not been initialized it will be initialized.
         * This is thread-safe.
         *
         * @return std::shared_ptr<spdlog::logger>& The logger
         */
        inline static auto Get() -> std::shared_ptr<spdlog::logger> &
        {
            static std::once_flag initFlag;
            std::call_once(initFlag, []() { Logger::initialize(); });
            return Logger::logger_;
        }

    private:
        static void initialize();

    private:
        static std::shared_ptr<spdlog::logger> logger_;
    };
} // namespace fluid_props
