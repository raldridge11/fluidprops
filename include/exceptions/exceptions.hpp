#pragma once
#include <exception>
#include <string>

namespace fluid_props
{
    /**
     * @brief PropertyOutOfBounds exception
     * Raised when a given property does not meet the
     * range of applicability
     *
     */
    class PropertyOutOfBounds : public std::exception
    {

    public:
        explicit PropertyOutOfBounds(char *msg) : message{msg} {};
        explicit PropertyOutOfBounds(const char *msg) : message{msg} {};
        explicit PropertyOutOfBounds(std::string &msg) : message{const_cast<char *>(msg.c_str())} {};
        ~PropertyOutOfBounds() override = default;

        /**
         * @brief The error message
         *
         * @return char*
         */
        auto what() -> const char * { return this->message; }

    private:
        const char *message;
        using std::exception::what;
    };

    /**
     * @brief ConversionError exception
     * Raised if a given unit is not valid when converting units.
     *
     */
    class ConversionError : public std::exception
    {
    public:
        explicit ConversionError(char *msg) : message{msg} {};
        explicit ConversionError(std::string &msg) : message{const_cast<char *>(msg.c_str())} {};
        ~ConversionError() override = default;

        /**
         * @brief The error message
         *
         * @return char*
         */
        auto what() -> const char * { return this->message; }

    private:
        char *message;
        using std::exception::what;
    };

} // namespace fluid_props
