#include "logger/logger.hpp"

namespace fluid_props
{
    std::shared_ptr<spdlog::logger> Logger::logger_ = nullptr;

    void Logger::initialize()
    {
        Logger::logger_ = spdlog::stdout_color_mt("FluidProps++");
        spdlog::set_level(spdlog::level::info);
        spdlog::set_pattern("[%Y-%m-%d %H:%M:%S] [%^%l%$] %v");
    }

} // namespace fluid_props
