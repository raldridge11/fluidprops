#include "water/iapwsif97/iapwsif97m.hpp"
#include "math_methods/math_methods.hpp"
#include "water/iapwsif97/iapws_constants.hpp"
#include "water/iapwsif97/saturation.hpp"

namespace fluid_props::water
{
    IAPWSIF97M::IAPWSIF97M()
    {
        this->enthalpy_.SetMethod([this]() { return this->metastable_.Enthalpy(); });
        this->entropy_.SetMethod([this]() { return this->metastable_.Entropy(); });
        this->internal_energy_.SetMethod([this]() { return this->metastable_.InternalEnergy(); });
        this->specific_volume_.SetMethod([this]() { return this->metastable_.SpecificVolume(); });
        this->isobaric_heat_capacity_.SetMethod([this]() { return this->metastable_.IsobaricHeatCapacity(); });
        this->isochoric_heat_capacity_.SetMethod([this]() { return this->metastable_.IsochoricHeatCapacity(); });
        this->speed_of_sound_.SetMethod([this]() { return this->metastable_.SpeedOfSound(); });
        this->cubic_expansion_.SetMethod([this]() { return this->metastable_.CubicExpansion(); });
        this->compressibility_.SetMethod([this]() { return this->metastable_.Compressibility(); });
    }

    IAPWSIF97M::IAPWSIF97M(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature) : IAPWSIF97M()
    {
        this->SetState(pressure, temperature);
    }

    IAPWSIF97M::IAPWSIF97M(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) : IAPWSIF97M()
    {
        this->SetState(pressure, enthalpy);
    }

    IAPWSIF97M::IAPWSIF97M(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
        : IAPWSIF97M()
    {
        this->SetState(pressure, entropy);
    }

    void IAPWSIF97M::SetState(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature)
    {
        this->reset();
        this->pressure_ = pressure;
        this->temperature_ = temperature;

        this->metastable_.SetState(pressure, temperature);
    }

    void IAPWSIF97M::SetState(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
    {
        this->reset();
        this->pressure_ = pressure;

        this->temperature_ = TemperatureT<Kelvin>{math_methods::BisectionMethod(
            [this, pressure, enthalpy](double temperature) {
                this->metastable_.SetState(pressure, TemperatureT<Kelvin>(temperature));
                return this->metastable_.Enthalpy().Value() - enthalpy.Value();
            },
            kTriplePointTemperature.Value(),
            kCriticalTemperature.Value()
        )};

        this->enthalpy_ = enthalpy;
    }

    void IAPWSIF97M::SetState(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
    {
        this->reset();
        this->pressure_ = pressure;

        this->temperature_ = TemperatureT<Kelvin>{math_methods::BisectionMethod(
            [this, pressure, entropy](double temperature) {
                this->metastable_.SetState(pressure, TemperatureT<Kelvin>(temperature));
                return this->metastable_.Entropy().Value() - entropy.Value();
            },
            kTriplePointTemperature.Value(),
            kCriticalTemperature.Value()
        )};

        this->entropy_ = entropy;
    }

    void IAPWSIF97M::Pressure(PressureT<MegaPascal> pressure)
    {
        this->SetState(pressure, TemperatureT<Kelvin>(this->Temperature()));
    }

    void IAPWSIF97M::Temperature(TemperatureT<Kelvin> temperature)
    {
        this->SetState(PressureT<MegaPascal>(this->Pressure()), temperature);
    }

    void IAPWSIF97M::Enthalpy(SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
    {
        this->SetState(PressureT<MegaPascal>(this->Pressure()), enthalpy);
    }

    void IAPWSIF97M::Entropy(SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
    {
        this->SetState(PressureT<MegaPascal>(this->Pressure()), entropy);
    }

    auto IAPWSIF97M::Density() -> DensityT<KiloGram_CubicMeter>
    {
        auto specificVolume = IAPWSIF97M::SpecificVolume().Value();
        return DensityT<KiloGram_CubicMeter>{specificVolume != 0.0 ? 1.0 / specificVolume : 0.0};
    }

    auto IAPWSIF97M::InValidRange() -> bool
    {
        constexpr auto kMaxMetaPressure = PressureT<MegaPascal>{10.0};
        if (this->Pressure() < PressureT<MegaPascal>{kTriplePointPressure} || this->Pressure() > kMaxMetaPressure)
        {
            return false;
        }

        constexpr auto quality = QualityT<Dimensionless>{0.95};
        IAPWSIF97Saturation saturation;
        saturation.Pressure(PressureT<MegaPascal>{this->Pressure()});

        auto moistEnthalpy =
            MixProperties(quality, saturation.Enthalpy(Phase::Liquid), saturation.Enthalpy(Phase::Vapor));

        if (this->Enthalpy() < moistEnthalpy || this->Enthalpy() > saturation.Enthalpy(Phase::Vapor))
        {
            return false;
        }

        return true;
    }

    void IAPWSIF97M::reset()
    {
        this->pressure_.Reset();
        this->temperature_.Reset();

        this->enthalpy_.Reset();
        this->entropy_.Reset();
        this->internal_energy_.Reset();
        this->specific_volume_.Reset();
        this->isobaric_heat_capacity_.Reset();
        this->isochoric_heat_capacity_.Reset();
        this->speed_of_sound_.Reset();
        this->cubic_expansion_.Reset();
        this->compressibility_.Reset();
    }
} // namespace fluid_props::water
