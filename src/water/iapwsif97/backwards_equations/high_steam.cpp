#include "water/iapwsif97/backwards_equations/high_steam.hpp"
#include "math_methods/math_methods.hpp"
#include "water/iapwsif97/basic_equations/high_steam.hpp"
#include "water/iapwsif97/iapws_constants.hpp"

namespace fluid_props::water
{
    void BackwardsHighSteam::SetState(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
    {
        this->equation_ = [pressure, enthalpy]() {
            return BackwardsHighSteam::BackwardTemperature(pressure, enthalpy);
        };
        this->validator_ = [pressure, enthalpy]() { return BackwardsHighSteam::Validate(pressure, enthalpy); };
    }

    void BackwardsHighSteam::SetState(
        PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy
    )
    {
        this->equation_ = [pressure, entropy]() { return BackwardsHighSteam::BackwardTemperature(pressure, entropy); };
        this->validator_ = [pressure, entropy]() { return BackwardsHighSteam::Validate(pressure, entropy); };
    }

    auto BackwardsHighSteam::Temperature() const -> TemperatureT<Kelvin>
    {
        return this->equation_();
    }

    auto BackwardsHighSteam::WithinValidRange() const -> bool
    {
        return this->validator_();
    }

    auto BackwardsHighSteam::BackwardTemperature(
        PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy
    ) -> TemperatureT<Kelvin>
    {
        auto result = fluid_props::math_methods::SecantMethod(
            [pressure, enthalpy](double temperature) {
                auto equation = BasicHighSteam(pressure, TemperatureT<Kelvin>(temperature));
                return equation.Enthalpy().Value() - enthalpy.Value();
            },
            kMaxTemperature.Value(),
            kHighMaxTemperature.Value(),
            1.0e-5
        );
        return TemperatureT<Kelvin>{result};
    }

    auto BackwardsHighSteam::BackwardTemperature(
        PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy
    ) -> TemperatureT<Kelvin>
    {
        auto result = fluid_props::math_methods::SecantMethod(
            [pressure, entropy](double temperature) {
                auto equation = BasicHighSteam(pressure, TemperatureT<Kelvin>(temperature));
                return equation.Entropy().Value() - entropy.Value();
            },
            kMaxTemperature.Value(),
            kHighMaxTemperature.Value(),
            1.0e-5
        );
        return TemperatureT<Kelvin>{result};
    }

    auto BackwardsHighSteam::Validate(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
        -> bool
    {
        if (pressure < kTriplePointPressure || pressure > KHighMaxPressure)
        {
            return false;
        }

        auto lowLimit = BasicHighSteam(pressure, kMaxTemperature);
        auto highLimit = BasicHighSteam(pressure, kHighMaxTemperature);
        return enthalpy >= lowLimit.Enthalpy() && enthalpy <= highLimit.Enthalpy();
    }

    auto BackwardsHighSteam::Validate(
        PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy
    ) -> bool
    {
        if (pressure < kTriplePointPressure || pressure > KHighMaxPressure)
        {
            return false;
        }

        auto lowLimit = BasicHighSteam(pressure, kMaxTemperature);
        auto highLimit = BasicHighSteam(pressure, kHighMaxTemperature);
        return entropy >= lowLimit.Entropy() && entropy <= highLimit.Entropy();
    }
} // namespace fluid_props::water
