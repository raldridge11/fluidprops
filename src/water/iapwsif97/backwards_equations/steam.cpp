#include "water/iapwsif97/backwards_equations/steam.hpp"
#include "water/iapwsif97/basic_equations/steam.hpp"
#include "water/iapwsif97/curves/saturation.hpp"
#include "water/iapwsif97/curves/steam_boundary.hpp"
#include "water/iapwsif97/curves/supercritical.hpp"
#include "water/iapwsif97/iapws_constants.hpp"

namespace fluid_props::water
{
    namespace
    {
        constexpr static const TemperatureT<Kelvin> kTStar{1.0};
        constexpr static const PressureT<MegaPascal> kPStar{1.0};
        constexpr static const SpecificEnergyT<KiloJoule_KiloGram> kHStar{2000.0};

        constexpr static const PressureT<MegaPascal> kRegionAMaxPressure{4.0};
        constexpr static const PressureT<MegaPascal> kBoundaryMinPressure{6.546699678};
        constexpr static const SpecificEntropyT<KiloJoule_KiloGram_Kelvin> kBoundaryEntropy{5.85};

        static auto SaturationTemperature(PressureT<MegaPascal> pressure) -> TemperatureT<Kelvin>
        {
            SaturationCurve saturation{pressure};
            return pressure <= kCriticalPressure ? saturation.Temperature() : kCriticalTemperature;
            ;
        }

        auto CheckRegionA(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool;
        auto CheckRegionB(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool;
        auto CheckRegionC(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool;
        auto CheckRegionA(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) -> bool;
        auto CheckRegionB(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) -> bool;
        auto CheckRegionC(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) -> bool;

        auto TemperatureRegionA(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
            -> TemperatureT<Kelvin>;
        auto TemperatureRegionB(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
            -> TemperatureT<Kelvin>;
        auto TemperatureRegionC(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
            -> TemperatureT<Kelvin>;
        auto TemperatureRegionA(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            -> TemperatureT<Kelvin>;
        auto TemperatureRegionB(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            -> TemperatureT<Kelvin>;
        auto TemperatureRegionC(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            -> TemperatureT<Kelvin>;
    } // namespace

    auto BackwardsSteam::BackwardTemperature(
        PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy
    ) -> TemperatureT<Kelvin>
    {
        if (CheckRegionA(pressure, enthalpy))
        {
            return TemperatureRegionA(pressure, enthalpy);
        }

        if (CheckRegionB(pressure, enthalpy))
        {
            return TemperatureRegionB(pressure, enthalpy);
        }
        return TemperatureRegionC(pressure, enthalpy);
    }

    auto BackwardsSteam::BackwardTemperature(
        PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy
    ) -> TemperatureT<Kelvin>
    {
        if (CheckRegionA(pressure, entropy))
        {
            return TemperatureRegionA(pressure, entropy);
        }

        if (CheckRegionB(pressure, entropy))
        {
            return TemperatureRegionB(pressure, entropy);
        }
        return TemperatureRegionC(pressure, entropy);
    }

    auto BackwardsSteam::Validate(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool
    {
        return CheckRegionA(pressure, enthalpy) || CheckRegionB(pressure, enthalpy) || CheckRegionC(pressure, enthalpy);
    }

    auto BackwardsSteam::Validate(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
        -> bool
    {
        return CheckRegionA(pressure, entropy) || CheckRegionB(pressure, entropy) || CheckRegionC(pressure, entropy);
    }

    void BackwardsSteam::SetState(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
    {
        this->equation_ = [pressure, enthalpy]() { return BackwardsSteam::BackwardTemperature(pressure, enthalpy); };
        this->validator_ = [pressure, enthalpy]() { return BackwardsSteam::Validate(pressure, enthalpy); };
    }

    void BackwardsSteam::SetState(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
    {
        this->equation_ = [pressure, entropy]() { return BackwardsSteam::BackwardTemperature(pressure, entropy); };
        this->validator_ = [pressure, entropy]() { return BackwardsSteam::Validate(pressure, entropy); };
    }

    auto BackwardsSteam::Temperature() const -> TemperatureT<Kelvin>
    {
        return this->equation_();
    }

    auto BackwardsSteam::WithinValidRange() const -> bool
    {
        return this->validator_();
    }

    namespace
    {
        auto TemperatureRegionA(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
            -> TemperatureT<Kelvin>
        {
            const Exponent kI = {
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0,
                2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 3.0, 3.0, 4.0, 4.0, 4.0, 5.0, 5.0, 5.0, 6.0, 6.0, 7.0,
            };
            const Exponent kJ = {
                0.0, 1.0,  2.0,  3.0,  7.0,  20.0, 0.0,  1.0,  2.0,  3.0,  7.0,  9.0,  11.0, 18.0, 44.0, 0.0,  2.0,
                7.0, 36.0, 38.0, 40.0, 42.0, 44.0, 24.0, 44.0, 12.0, 32.0, 44.0, 32.0, 36.0, 42.0, 34.0, 44.0, 28.0,
            };
            const Coefficient kN = {
                0.10898952318288e4,  0.84951654495535e3,   -0.10781748091826e3, 0.33153654801263e2,
                -0.74232016790248e1, 0.11765048724356e2,   0.18445749355790e1,  -0.41792700549624e1,
                0.62478196935812e1,  -0.17344563108114e2,  -0.20058176862096e3, 0.27196065473796e3,
                -0.45511318285818e3, 0.30919688604755e4,   0.25226640357872e6,  -0.61707422868339e-2,
                -0.31078046629583,   0.11670873077107e2,   0.12812798404046e9,  -0.98554909623276e9,
                0.28224546973002e10, -0.35948971410703e10, 0.17227349913197e10, -0.13551334240775e5,
                0.12848734664650e8,  0.13865724283226e1,   0.23598832556514e6,  -0.13105236545054e8,
                0.73999835474766e4,  -0.55196697030060e6,  0.37154085996233e7,  0.19127729239660e5,
                -0.41535164835634e6, -0.62459855192507e2,
            };
            NormalizedProperty pi = pressure / kPStar;
            NormalizedProperty eta = enthalpy / kHStar;
            auto result = kN * std::pow(pi, kI) * std::pow(eta - 2.1, kJ);
            return TemperatureT<Kelvin>{kTStar * result.sum()};
        }

        auto TemperatureRegionB(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
            -> TemperatureT<Kelvin>
        {
            const Exponent kI = {
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0,
                2.0, 3.0, 3.0, 3.0, 3.0, 4.0, 4.0, 4.0, 4.0, 4.0, 4.0, 5.0, 5.0, 5.0, 6.0, 7.0, 7.0, 9.0, 9.0,
            };
            const Exponent kJ = {
                0.0,  1.0,  2.0,  12.0, 18.0, 24.0, 28.0, 40.0, 0.0, 2.0,  6.0,  12.0, 18.0,
                24.0, 28.0, 40.0, 2.0,  8.0,  18.0, 40.0, 1.0,  2.0, 12.0, 24.0, 2.0,  12.0,
                18.0, 24.0, 28.0, 40.0, 18.0, 24.0, 40.0, 28.0, 2.0, 28.0, 1.0,  40.0,
            };
            const Coefficient kN = {
                0.14895041079516e4,    0.74307798314034e3,   -0.97708318797837e2,   0.24742464705674e1,
                -0.63281320016026,     0.11385952129658e1,   -0.47811863648625,     0.85208123431544e-2,
                0.93747147377932,      0.33593118604916e1,   0.33809355601454e1,    0.16844539671904,
                0.73875745236695,      -0.47128737436186,    0.15020273139707,      -0.21764114219750e-2,
                -0.21810755324761e-1,  -0.10829784403677,    -0.46333324635812e-1,  0.71280351959551e-4,
                0.11032831789999e-3,   0.18955248387902e-3,  0.30891541160537e-2,   0.13555504554949e-2,
                0.28640237477456e-6,   -0.10779857357512e-4, -0.76462712454814e-4,  0.14052392818316e-4,
                -0.31083814331434e-4,  -0.10302738212103e-5, 0.28217281635040e-6,   0.12704902271945e-5,
                0.73803353468292e-7,   -0.11030139238909e-7, -0.81456365207833e-13, -0.25180545682962e-10,
                -0.17565233969407e-17, 0.86934156344163e-14,
            };
            NormalizedProperty pi = pressure / kPStar;
            NormalizedProperty eta = enthalpy / kHStar;
            auto result = kN * std::pow(pi - 2.0, kI) * std::pow(eta - 2.6, kJ);
            return TemperatureT<Kelvin>{kTStar * result.sum()};
        }

        auto TemperatureRegionC(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
            -> TemperatureT<Kelvin>
        {
            const Exponent kI = {
                -7.0, -7.0, -6.0, -6.0, -5.0, -5.0, -2.0, -2.0, -1.0, -1.0, 0.0, 0.0,
                1.0,  1.0,  2.0,  6.0,  6.0,  6.0,  6.0,  6.0,  6.0,  6.0,  6.0,
            };
            const Exponent kJ = {
                0.0, 4.0, 0.0, 2.0, 0.0, 2.0, 0.0,  1.0,  0.0,  2.0,  0.0,  1.0,
                4.0, 8.0, 4.0, 0.0, 1.0, 4.0, 10.0, 12.0, 16.0, 20.0, 22.0,
            };
            const Coefficient kN = {
                -0.32368398555242e13, 0.73263350902181e13,   0.35825089945447e12,  -0.58340131851590e12,
                -0.10783068217470e11, 0.20825544563171e11,   0.61074783564516e6,   0.85977722535580e6,
                -0.25745723604170e5,  0.31081088422714e5,    0.12082315865936e4,   0.48219755109255e3,
                0.37966001272486e1,   -0.10842984880077e2,   -0.45364172676660e-1, 0.14559115658698e-12,
                0.11261597407230e-11, -0.17804982240686e-10, 0.12324579690832e-6,  -0.11606921130984e-5,
                0.27846367088554e-4,  -0.59270038474176e-3,  0.12918582991878e-2,
            };
            NormalizedProperty pi = pressure / kPStar;
            NormalizedProperty eta = enthalpy / kHStar;
            auto result = kN * std::pow(pi + 25.0, kI) * std::pow(eta - 1.8, kJ);
            return TemperatureT<Kelvin>{kTStar * result.sum()};
        }

        auto TemperatureRegionA(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            -> TemperatureT<Kelvin>
        {
            constexpr const SpecificEntropyT<KiloJoule_KiloGram_Kelvin> kSStar{2.0};
            const Exponent kI = {
                -1.5,  -1.5, -1.5, -1.5, -1.5, -1.5,  -1.25, -1.25, -1.25, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -0.75,
                -0.75, -0.5, -0.5, -0.5, -0.5, -0.25, -0.25, -0.25, -0.25, 0.25, 0.25, 0.25, 0.25, 0.5,  0.5,  0.5,
                0.5,   0.5,  0.5,  0.5,  0.75, 0.75,  0.75,  0.75,  1.0,   1.0,  1.25, 1.25, 1.5,  1.5,
            };
            const Exponent kJ = {
                -24.0, -23.0, -19.0, -13.0, -11.0, -10.0, -19.0, -15.0, -6.0, -26.0, -21.0, -17.0,
                -16.0, -9.0,  -8.0,  -15.0, -14.0, -26.0, -13.0, -9.0,  -7.0, -27.0, -25.0, -11.0,
                -6.0,  1.0,   4.0,   8.0,   11.0,  0.0,   1.0,   5.0,   6.0,  10.0,  14.0,  16.0,
                0.0,   4.0,   9.0,   17.0,  7.0,   18.0,  3.0,   15.0,  5.0,  18.0,
            };
            const Coefficient kN = {
                -0.39235983861984e6,  0.51526573827270e6,   0.40482443161048e5,   -0.32193790923902e3,
                0.96961424218694e2,   -0.22867846371773e2,  -0.44942914124357e6,  -0.50118336020166e4,
                0.35684463560015,     0.44235335848190e5,   -0.13673388811708e5,  0.42163260207864e6,
                0.22516925837475e5,   0.47442144865646e3,   -0.14931130797647e3,  -0.19781126320452e6,
                -0.23554399470760e5,  -0.19070616302076e5,  0.55375669883164e5,   0.38293691437363e4,
                -0.60391860580567e3,  0.19363102620331e4,   0.42660643698610e4,   -0.59780638872718e4,
                -0.70401463926862e3,  0.33836784107553e3,   0.20862786635187e2,   0.33834172656196e-1,
                -0.43124428414893e-4, 0.16653791356412e3,   -0.13986292055898e3,  -0.78849547999872,
                0.72132411753872e-1,  -0.59754839398283e-2, -0.12141358953904e-4, 0.23227096733871e-6,
                -0.10538463566194e2,  0.20718925496502e1,   -0.72193155260427e-1, 0.20749887081120e-6,
                -0.18340657911379e-1, 0.29036272348696e-6,  0.21037527893619,     0.25681239729999e-3,
                -0.12799002933781e-1, -0.82198102652018e-5,
            };

            NormalizedProperty pi = pressure / kPStar;
            NormalizedProperty sigma = entropy / kSStar;

            auto result = kN * std::pow(pi, kI) * std::pow(sigma - 2.0, kJ);

            return TemperatureT<Kelvin>{kTStar * result.sum()};
        }

        auto TemperatureRegionB(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            -> TemperatureT<Kelvin>
        {
            constexpr const SpecificEntropyT<KiloJoule_KiloGram_Kelvin> kSStar{0.7853};
            const Exponent kI = {
                -6.0, -6.0, -5.0, -5.0, -4.0, -4.0, -4.0, -3.0, -3.0, -3.0, -3.0, -2.0, -2.0, -2.0, -2.0,
                -1.0, -1.0, -1.0, -1.0, -1.0, 0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  1.0,  1.0,  1.0,
                1.0,  1.0,  1.0,  2.0,  2.0,  2.0,  3.0,  3.0,  3.0,  4.0,  4.0,  5.0,  5.0,  5.0,
            };
            const Exponent kJ = {
                0.0, 11.0, 0.0, 11.0, 0.0, 1.0, 11.0, 0.0, 1.0, 11.0, 12.0, 0.0, 1.0, 6.0, 10.0,
                0.0, 1.0,  5.0, 8.0,  9.0, 0.0, 1.0,  2.0, 4.0, 5.0,  6.0,  9.0, 0.0, 1.0, 2.0,
                3.0, 7.0,  8.0, 0.0,  1.0, 5.0, 0.0,  1.0, 3.0, 0.0,  1.0,  0.0, 1.0, 2.0,
            };
            const Coefficient kN = {
                0.31687665083497e6,  0.20864175881858e2,   -0.39859399803599e6,  -0.21816058518877e2,
                0.22369785194242e6,  -0.27841703445817e4,  0.99207436071480e1,   -0.75197512299157e5,
                0.29708605951158e4,  -0.34406878548526e1,  0.38815564249115,     0.17511295085750e5,
                -0.14237112854449e4, 0.10943803364167e1,   0.89971619308495,     -0.33759740098958e4,
                0.47162885818355e3,  -0.19188241993679e1,  0.41078580492196,     -0.33465378172097,
                0.13870034777505e4,  -0.40663326195838e3,  0.41727347159610e2,   0.21932549434532e1,
                -0.10320050009077e1, 0.35882943516703,     0.52511453726066e-2,  0.12838916450705e2,
                -0.28642437219381e1, 0.56912683664855,     -0.99962954584931e-1, -0.32632037778459e-2,
                0.23320922576723e-3, -0.15334809857450,    0.29072288239902e-1,  0.37534702741167e-3,
                0.17296691702411e-2, -0.38556050844504e-3, -0.35017712292608e-4, -0.14566393631492e-4,
                0.56420857267269e-5, 0.41286150074605e-7,  -0.20684671118824e-7, 0.16409393674725e-8,
            };

            NormalizedProperty pi = pressure / kPStar;
            NormalizedProperty sigma = entropy / kSStar;

            auto result = kN * std::pow(pi, kI) * std::pow(10.0 - sigma, kJ);

            return TemperatureT<Kelvin>{kTStar * result.sum()};
        }

        auto TemperatureRegionC(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            -> TemperatureT<Kelvin>
        {
            constexpr const SpecificEntropyT<KiloJoule_KiloGram_Kelvin> kSStar{2.9251}; // kJ/kgK
            const Exponent kI = {
                -2.0, -2.0, -1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 3.0,
                3.0,  3.0,  4.0,  4.0, 4.0, 5.0, 5.0, 5.0, 6.0, 6.0, 7.0, 7.0, 7.0, 7.0, 7.0,
            };
            const Exponent kJ = {
                0.0, 1.0, 0.0, 0.0, 1.0, 2.0, 3.0, 0.0, 1.0, 3.0, 4.0, 0.0, 1.0, 2.0, 0.0,
                1.0, 5.0, 0.0, 1.0, 4.0, 0.0, 1.0, 2.0, 0.0, 1.0, 0.0, 1.0, 3.0, 4.0, 5.0,
            };
            const Coefficient kN = {
                0.90968501005365e3,  0.24045667088420e4,   -0.59162326387130e3,   0.54145404128074e3,
                -0.27098308411192e3, 0.97976525097926e3,   -0.46966772959435e3,   0.14399274604723e2,
                -0.19104204230429e2, 0.53299167111971e1,   -0.21252975375934e2,   -0.31147334413760,
                0.60334840894623,    -0.42764839702509e-1, 0.58185597255259e-2,   -0.14597008284753e-1,
                0.56631175631027e-2, -0.76155864584577e-4, 0.22440342919332e-3,   -0.12561095013413e-4,
                0.63323132660934e-6, -0.20541989675375e-5, 0.36405370390082e-7,   -0.29759897789215e-8,
                0.10136618529763e-7, 0.59925719692351e-11, -0.20677870105164e-10, -0.20874278181886e-10,
                0.10162166825089e-9, -0.16429828281347e-9,
            };

            NormalizedProperty pi = pressure / kPStar;
            NormalizedProperty sigma = entropy / kSStar;

            auto result = kN * std::pow(pi, kI) * std::pow(2.0 - sigma, kJ);

            return TemperatureT<Kelvin>{kTStar * result.sum()};
        }

        auto CheckRegionA(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool
        {
            if (pressure < kTriplePointPressure || pressure > kRegionAMaxPressure)
            {
                return false;
            }

            auto saturationTemperature = SaturationTemperature(pressure);
            auto maxIsotherm = BasicSteam{pressure, kMaxTemperature};
            auto saturationLimit = BasicSteam{pressure, saturationTemperature};
            return enthalpy <= maxIsotherm.Enthalpy() && enthalpy >= saturationLimit.Enthalpy();
        }

        auto CheckRegionB(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool
        {
            if (pressure < kRegionAMaxPressure || pressure > kMaxPressure)
            {
                return false;
            }

            auto maxIsotherm = BasicSteam{pressure, kMaxTemperature};
            if (enthalpy > maxIsotherm.Enthalpy())
            {
                return false;
            }

            auto saturationTemperature = SaturationTemperature(pressure);
            auto saturationLimit = BasicSteam{pressure, saturationTemperature};
            if (pressure < kBoundaryMinPressure && enthalpy >= saturationLimit.Enthalpy())
            {
                return true;
            }

            auto boundaryLimit = SteamBoundaryCurve{pressure};
            if (pressure >= kBoundaryMinPressure && enthalpy >= boundaryLimit.Enthalpy())
            {
                return true;
            }
            return false;
        }

        auto CheckRegionC(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool
        {
            if (pressure < kRegionAMaxPressure || pressure > kMaxPressure)
            {
                return false;
            }

            auto boundaryLimit = SteamBoundaryCurve{pressure};
            if (enthalpy > boundaryLimit.Enthalpy())
            {
                return false;
            }

            auto saturationTemperature = SaturationTemperature(pressure);
            auto saturationLimit = BasicSteam{pressure, saturationTemperature};
            if (pressure < kMinCriticalPressure && enthalpy >= saturationLimit.Enthalpy())
            {
                return true;
            }

            auto superCriticalTemperature = SuperCriticalCurve{pressure}.Temperature();
            auto superCriticalLimit = BasicSteam{pressure, TemperatureT<Kelvin>(superCriticalTemperature)};
            if (pressure >= kMinCriticalPressure && enthalpy >= superCriticalLimit.Enthalpy())
            {
                return true;
            }

            return false;
        }

        auto CheckRegionA(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) -> bool
        {
            if (pressure > kBoundaryMinPressure || pressure < kTriplePointPressure)
            {
                return false;
            }

            auto saturationTemperature = SaturationTemperature(pressure);
            auto maxIsotherm = BasicSteam{pressure, kMaxTemperature};
            auto saturationLimit = BasicSteam{pressure, saturationTemperature};

            return entropy <= maxIsotherm.Entropy() && entropy >= saturationLimit.Entropy();
        }

        auto CheckRegionB(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) -> bool
        {
            if (pressure < kRegionAMaxPressure || pressure > kMaxPressure)
            {
                return false;
            }

            auto maxIsotherm = BasicSteam{pressure, kMaxTemperature};
            if (entropy > maxIsotherm.Entropy())
            {
                return false;
            }

            if (pressure >= kBoundaryMinPressure && entropy >= kBoundaryEntropy)
            {
                return true;
            }

            auto saturationTemperature = SaturationTemperature(pressure);
            auto saturationLimit = BasicSteam{pressure, saturationTemperature};
            if (pressure < kBoundaryMinPressure && entropy >= saturationLimit.Entropy())
            {
                return true;
            }

            return false;
        }

        auto CheckRegionC(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) -> bool
        {
            if (pressure < kRegionAMaxPressure || pressure > kMaxPressure)
            {
                return false;
            }

            if (entropy > kBoundaryEntropy)
            {
                return false;
            }

            auto saturationTemperature = SaturationTemperature(pressure);
            auto saturationLimit = BasicSteam{pressure, saturationTemperature};
            if (pressure < kMinCriticalPressure && entropy >= saturationLimit.Entropy())
            {
                return true;
            }

            auto superCriticalTemperature = SuperCriticalCurve{pressure}.Temperature();
            auto superCriticalLimit = BasicSteam{pressure, superCriticalTemperature};
            if (pressure >= kMinCriticalPressure && entropy >= superCriticalLimit.Entropy())
            {
                return true;
            }

            return false;
        }

    } // namespace

} // namespace fluid_props::water
