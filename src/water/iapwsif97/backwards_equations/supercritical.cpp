#include "water/iapwsif97/backwards_equations/supercritical.hpp"
#include "water/iapwsif97/basic_equations/liquid.hpp"
#include "water/iapwsif97/basic_equations/steam.hpp"
#include "water/iapwsif97/basic_equations/supercritical.hpp"
#include "water/iapwsif97/curves/saturation.hpp"
#include "water/iapwsif97/curves/supercritical.hpp"
#include "water/iapwsif97/iapws_constants.hpp"
#include <valarray>

namespace fluid_props::water
{
    namespace
    {
        constexpr static const TemperatureT<Kelvin> kTStarA{760.0};
        constexpr static const TemperatureT<Kelvin> kTStarB{860.0};

        constexpr static const PressureT<MegaPascal> kPStar{100.0};

        constexpr static const SpecificEnergyT<KiloJoule_KiloGram> kHStarA{2300.0};
        constexpr static const SpecificEnergyT<KiloJoule_KiloGram> kHStarB{2800.0};

        constexpr static const SpecificEntropyT<KiloJoule_KiloGram_Kelvin> kSStarA{4.4};
        constexpr static const SpecificEntropyT<KiloJoule_KiloGram_Kelvin> kSStarB{5.3};

        static auto SaturationTemperature(PressureT<MegaPascal> pressure) -> TemperatureT<Kelvin>
        {
            SaturationCurve saturation{pressure};
            return pressure <= kCriticalPressure ? saturation.Temperature() : kCriticalTemperature;
        }

        auto CheckRegionA(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool;
        auto CheckRegionB(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool;
        auto CheckRegionA(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) -> bool;
        auto CheckRegionB(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) -> bool;

        auto TemperatureRegionA(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
            -> TemperatureT<Kelvin>;
        auto TemperatureRegionB(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
            -> TemperatureT<Kelvin>;
        auto TemperatureRegionA(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            -> TemperatureT<Kelvin>;
        auto TemperatureRegionB(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            -> TemperatureT<Kelvin>;
    } // namespace

    auto BackwardsSuperCritical::Temperature() const -> TemperatureT<Kelvin>
    {
        return this->equation_();
    }

    auto BackwardsSuperCritical::WithinValidRange() const -> bool
    {
        return this->validator_();
    }

    void BackwardsSuperCritical::SetState(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
    {
        this->equation_ = [pressure, enthalpy] {
            return BackwardsSuperCritical::BackwardTemperature(pressure, enthalpy);
        };
        this->validator_ = [pressure, enthalpy] { return BackwardsSuperCritical::Validate(pressure, enthalpy); };
    }

    void BackwardsSuperCritical::SetState(
        PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy
    )
    {
        this->equation_ = [pressure, entropy] {
            return BackwardsSuperCritical::BackwardTemperature(pressure, entropy);
        };
        this->validator_ = [pressure, entropy] { return BackwardsSuperCritical::Validate(pressure, entropy); };
    }

    auto BackwardsSuperCritical::Validate(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
        -> bool
    {
        return CheckRegionA(pressure, enthalpy) || CheckRegionB(pressure, enthalpy);
    }

    auto BackwardsSuperCritical::Validate(
        PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy
    ) -> bool
    {
        return CheckRegionA(pressure, entropy) || CheckRegionB(pressure, entropy);
    }

    auto BackwardsSuperCritical::BackwardTemperature(
        PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy
    ) -> TemperatureT<Kelvin>
    {
        if (CheckRegionA(pressure, enthalpy))
        {
            return TemperatureRegionA(pressure, enthalpy);
        }

        return TemperatureRegionB(pressure, enthalpy);
    }

    auto BackwardsSuperCritical::BackwardTemperature(
        PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy
    ) -> TemperatureT<Kelvin>
    {
        if (CheckRegionA(pressure, entropy))
        {
            return TemperatureRegionA(pressure, entropy);
        }

        return TemperatureRegionB(pressure, entropy);
    }

    namespace
    {
        auto CheckRegionA(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool
        {
            if (pressure > kMaxPressure)
            {
                return false;
            }

            auto liquidIsotherm = BasicLiquid{pressure, kSubcooledMaxTemperature};
            if (enthalpy < liquidIsotherm.Enthalpy())
            {
                return false;
            }

            auto regionABLimit = SuperCriticalBoundary{pressure};
            if (pressure > kCriticalPressure && enthalpy > regionABLimit.Enthalpy())
            {
                return false;
            }

            auto saturationTemperature = SaturationTemperature(pressure);
            auto saturationLimit = BasicSuperCritical{pressure, saturationTemperature};
            if (pressure <= kCriticalPressure && enthalpy > saturationLimit.Enthalpy())
            {
                return false;
            }

            return true;
        }

        auto CheckRegionB(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool
        {
            if (pressure > kMaxPressure)
            {
                return false;
            }

            if (pressure > kCriticalPressure && enthalpy <= SuperCriticalBoundary{pressure}.Enthalpy())
            {
                return false;
            }

            auto steamBoundaryTemperature = SuperCriticalCurve{pressure}.Temperature();
            auto steamLimit = BasicSteam{pressure, steamBoundaryTemperature};
            if (pressure > kCriticalPressure && enthalpy > steamLimit.Enthalpy())
            {
                return false;
            }

            auto saturationTemperature = SaturationTemperature(pressure);
            auto saturationLimit = BasicSuperCritical{pressure, saturationTemperature};
            if (pressure <= kCriticalPressure && enthalpy < saturationLimit.Enthalpy())
            {
                return false;
            }

            return true;
        }

        auto CheckRegionA(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) -> bool
        {
            if (pressure > kMaxPressure)
            {
                return false;
            }

            if (pressure > kCriticalPressure && entropy > kCriticalEntropy)
            {
                return false;
            }

            auto liquidIsotherm = BasicLiquid{pressure, kSubcooledMaxTemperature};
            if (entropy < liquidIsotherm.Entropy())
            {
                return false;
            }

            auto saturationTemperature = SaturationTemperature(pressure);
            auto saturationLimit = BasicSuperCritical{pressure, saturationTemperature};
            if (pressure <= kCriticalPressure && entropy > saturationLimit.Entropy())
            {
                return false;
            }

            return true;
        }

        auto CheckRegionB(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy) -> bool
        {
            if (pressure > kMaxPressure)
            {
                return false;
            }

            if (pressure > kCriticalPressure && entropy <= kCriticalEntropy)
            {
                return false;
            }

            auto steamBoundaryTemperature = SuperCriticalCurve{pressure}.Temperature();
            auto steamLimit = BasicSteam{pressure, steamBoundaryTemperature};
            if (pressure > kCriticalPressure && entropy > steamLimit.Entropy())
            {
                return false;
            }

            auto saturationTemperature = SaturationTemperature(pressure);
            auto saturationLimit = BasicSuperCritical{pressure, saturationTemperature};
            if (pressure <= kCriticalPressure && entropy < saturationLimit.Entropy())
            {
                return false;
            }

            return true;
        }

        auto TemperatureRegionA(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
            -> TemperatureT<Kelvin>
        {
            auto pi = pressure / kPStar;
            auto eta = enthalpy / kHStarA;
            Exponent kI = {
                -12.0, -12.0, -12.0, -12.0, -12.0, -12.0, -12.0, -12.0, -10.0, -10.0, -10.0,
                -8.0,  -8.0,  -8.0,  -8.0,  -5.0,  -3.0,  -2.0,  -2.0,  -2.0,  -1.0,  -1.0,
                0.0,   0.0,   1.0,   3.0,   3.0,   4.0,   4.0,   10.0,  12.0,
            };

            Exponent kJ = {
                0.0, 1.0, 2.0, 6.0, 14.0, 16.0, 20.0, 22.0, 1.0, 5.0, 12.0, 0.0, 2.0, 4.0, 10.0, 2.0,
                0.0, 1.0, 3.0, 4.0, 0.0,  2.0,  0.0,  1.0,  1.0, 0.0, 1.0,  0.0, 3.0, 4.0, 5.0,
            };

            Coefficient kN = {
                -0.133645667811215e-6, 0.455912656802978e-5,  -0.146294640700979e-4, 0.639341312970080e-2,
                0.372783927268847e3,   -0.718654377460447e4,  0.573494752103400e6,   -0.267569329111439e7,
                -0.334066283302614e-4, -0.245479214069597e-1, 0.478087847764996e2,   0.764664131818904e-5,
                0.128350627676972e-2,  0.171219081377331e-1,  -0.851007304583213e1,  -0.136513461629781e-1,
                -0.384460997596657e-5, 0.337423807911655e-2,  -0.551624873066791,    0.729202277107470,
                -0.992522757376041e-2, -0.119308831407288,    0.793929190615421,     0.454270731799386,
                0.209998591259910,     -0.642109823904738e-2, -0.235155868604540e-1, 0.252233108341612e-2,
                -0.764885133368119e-2, 0.136176427574291e-1,  -0.133027883575669e-1,
            };

            auto result = kN * std::pow(pi + 0.240, kI) * std::pow(eta - 0.615, kJ);
            return TemperatureT<Kelvin>{kTStarA * result.sum()};
        }

        auto TemperatureRegionB(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
            -> TemperatureT<Kelvin>
        {
            auto pi = pressure / kPStar;
            auto eta = enthalpy / kHStarB;

            Exponent kI = {
                -12.0, -12.0, -10.0, -10.0, -10.0, -10.0, -10.0, -8.0, -8.0, -8.0, -8.0,
                -8.0,  -6.0,  -6.0,  -6.0,  -4.0,  -4.0,  -3.0,  -2.0, -2.0, -1.0, -1.0,
                -1.0,  -1.0,  -1.0,  -1.0,  0.0,   0.0,   1.0,   3.0,  5.0,  6.0,  8.0,
            };

            Exponent kJ = {
                0.0, 1.0, 0.0, 1.0, 5.0, 10.0, 12.0, 0.0,  1.0,  2.0, 4.0, 10.0, 0.0, 1.0, 2.0, 0.0, 1.0,
                5.0, 0.0, 4.0, 2.0, 4.0, 6.0,  10.0, 14.0, 16.0, 0.0, 2.0, 1.0,  1.0, 1.0, 1.0, 1.0,
            };

            Coefficient kN = {
                0.323254573644920e-4,  -0.127575556587181e-3, -0.475851877356068e-3, 0.156183014181602e-2,
                0.105724860113781,     -0.858514221132534e2,  0.724140095480911e3,   0.296475810273257e-2,
                -0.592721983365988e-2, -0.126305422818666e-1, -0.115716196364853,    0.849000969739595e2,
                -0.108602260086615e-1, 0.154304475328851e-1,  0.750455441524466e-1,  0.252520973612982e-1,
                -0.602507901232996e-1, -0.307622221350501e1,  -0.574011959864879e-1, 0.503471360939849e1,
                -0.925081888584834,    0.391733882917546e1,   -0.773146007130190e2,  0.949308762098587e4,
                -0.141043719679409e7,  0.849166230819026e7,   0.861095729446704,     0.323346442811720,
                0.873281936020439,     -0.436653048526683,    0.286596714529479,     -0.131778331276228,
                0.676682064330275e-2,
            };

            auto result = kN * std::pow(pi + 0.298, kI) * std::pow(eta - 0.720, kJ);

            return TemperatureT<Kelvin>{kTStarB * result.sum()};
        }

        auto TemperatureRegionA(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            -> TemperatureT<Kelvin>
        {
            auto pi = pressure / kPStar;
            auto sigma = entropy / kSStarA;

            Exponent kI = {
                -12.0, -12.0, -10.0, -10.0, -10.0, -10.0, -8.0, -8.0, -8.0, -8.0, -6.0,
                -6.0,  -6.0,  -5.0,  -5.0,  -5.0,  -4.0,  -4.0, -4.0, -2.0, -2.0, -1.0,
                -1.0,  0.0,   0.0,   0.0,   1.0,   2.0,   2.0,  3.0,  8.0,  8.0,  10.0,
            };

            Exponent kJ = {
                28.0, 32.0, 4.0, 10.0, 12.0, 14.0, 5.0, 7.0, 8.0, 28.0, 2.0, 6.0, 32.0, 0.0, 14.0, 32.0, 6.0,
                10.0, 36.0, 1.0, 4.0,  1.0,  6.0,  0.0, 1.0, 4.0, 0.0,  0.0, 3.0, 2.0,  0.0, 1.0,  2.0,
            };

            Coefficient kN = {
                0.150042008263875e10, -0.159397258480424e12, 0.502181140217975e-3, -0.672057767855466e2,
                0.145058545404456e4,  -0.823889534888890e4,  -0.154852214233853,   0.112305046746695e2,
                -0.297000213482822e2, 0.438565132635495e11,  0.137837838635464e-2, -0.297478527157462e1,
                0.971777947349413e13, -0.571527767052398e-4, 0.288307949778420e5,  -0.744428289262703e14,
                0.128017324848921e2,  -0.368275545889071e3,  0.664768904779177e16, 0.449359251958880e-1,
                -0.422897836099655e1, -0.240614376434179,    -0.474341365254924e1, 0.724093999126110,
                0.923874349695897,    0.399043655281015e1,   0.384066651868009e-1, -0.359344365571848e-2,
                -0.735196448821653,   0.188367048396131,     0.141064266818704e-3, -0.257418501496337e-2,
                0.123220024851555e-2,
            };

            auto result = kN * std::pow(pi + 0.240, kI) * std::pow(sigma - 0.703, kJ);

            return TemperatureT<Kelvin>{kTStarA * result.sum()};
        }

        auto TemperatureRegionB(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
            -> TemperatureT<Kelvin>
        {
            auto pi = pressure / kPStar;
            auto sigma = entropy / kSStarB;

            Exponent kI = {
                -12.0, -12.0, -12.0, -12.0, -8.0, -8.0, -8.0, -6.0, -6.0, -6.0, -5.0, -5.0, -5.0, -5.0,
                -5.0,  -4.0,  -3.0,  -3.0,  -2.0, 0.0,  2.0,  3.0,  4.0,  5.0,  6.0,  8.0,  12.0, 14.0,
            };

            Exponent kJ = {
                1.0, 3.0,  4.0, 7.0, 0.0, 1.0, 3.0, 0.0, 2.0, 4.0,  0.0, 1.0, 2.0, 4.0,
                6.0, 12.0, 1.0, 6.0, 2.0, 0.0, 1.0, 1.0, 0.0, 24.0, 0.0, 3.0, 1.0, 2.0,
            };

            Coefficient kN = {
                0.527111701601660,     -0.401317830052742e2, 0.153020073134484e3,  -0.224799398218827e4,
                -0.193993484669048,    -0.140467557893768e1, 0.426799878114024e2,  0.752810643416743,
                0.226657238616417e2,   -0.622873556909932e3, -0.660823667935396,   0.841267087271658,
                -0.253717501764397e2,  0.485708963532948e3,  0.880531517490555e3,  0.265015592794626e7,
                -0.359287150025783,    -0.656991567673753e3, 0.241768149185367e1,  0.856873461222588,
                0.655143675313458,     -0.213535213206406,   0.562974957606348e-2, -0.316955725450471e15,
                -0.699997000152457e-3, 0.119845803210767e-1, 0.193848122022095e-4, -0.215095749182309e-4,
            };

            auto result = kN * std::pow(pi + 0.760, kI) * std::pow(sigma - 0.818, kJ);

            return TemperatureT<Kelvin>{kTStarB * result.sum()};
        }
    } // namespace

} // namespace fluid_props::water
