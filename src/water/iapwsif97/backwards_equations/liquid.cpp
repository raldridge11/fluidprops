#include "water/iapwsif97/backwards_equations/liquid.hpp"
#include "water/iapwsif97/basic_equations/liquid.hpp"
#include "water/iapwsif97/curves/saturation.hpp"
#include "water/iapwsif97/iapws_constants.hpp"

namespace fluid_props::water
{
    namespace
    {
        constexpr static const PressureT<MegaPascal> kPStar{1.0};
        constexpr static const SpecificEnergyT<KiloJoule_KiloGram> kHStar{2500.0};
        constexpr static const TemperatureT<Kelvin> kTStar{1.0};
        constexpr static const SpecificEntropyT<KiloJoule_KiloGram_Kelvin> kSStar{1.0};

        /**
         * @brief Validates that the provided pressure is valid for a subcooled liquid
         * 611.213 Pa <= P <= 100.0 MPa
         *
         * @param pressure pressure to check
         * @return true is subcooled
         * @return false not subcooled
         */
        static auto ValidatePressure(PressureT<MegaPascal> pressure) -> bool
        {
            return pressure >= kTriplePointPressure && pressure <= kMaxPressure;
        }

        static auto SaturationTemperature(PressureT<MegaPascal> pressure) -> TemperatureT<Kelvin>
        {
            SaturationCurve saturation{pressure};
            return pressure <= kCriticalPressure ? saturation.Temperature() : kCriticalTemperature;
        }
    } // namespace

    auto BackwardsLiquid::Temperature() const -> TemperatureT<Kelvin>
    {
        return this->equation_();
    }

    auto BackwardsLiquid::WithinValidRange() const -> bool
    {
        return this->validator_();
    };

    void BackwardsLiquid::SetState(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
    {
        this->equation_ = [pressure, enthalpy]() { return BackwardsLiquid::BackwardTemperature(pressure, enthalpy); };
        this->validator_ = [pressure, enthalpy]() { return BackwardsLiquid::Validate(pressure, enthalpy); };
    }

    void BackwardsLiquid::SetState(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
    {
        this->equation_ = [pressure, entropy]() { return BackwardsLiquid::BackwardTemperature(pressure, entropy); };
        this->validator_ = [pressure, entropy]() { return BackwardsLiquid::Validate(pressure, entropy); };
    }

    auto BackwardsLiquid::BackwardTemperature(
        PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy
    ) -> TemperatureT<Kelvin>
    {
        const Exponent kI = {
            0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 3.0, 3.0, 4.0, 5.0, 6.0,
        };

        const Exponent kJ = {
            0.0, 1.0,  2.0,  6.0,  22.0, 32.0, 0.0,  1.0,  2.0,  3.0,
            4.0, 10.0, 32.0, 10.0, 32.0, 10.0, 32.0, 32.0, 32.0, 32.0,
        };

        const Coefficient kN = {
            -0.23872489924521e3,  0.40421188637945e3,    0.11349746881718e3,   -0.58457616048039e1,
            -0.15285482413140e-3, -0.10866707695377e-5,  -0.13391744872602e2,  0.43211039183559e2,
            -0.54010067170506e2,  0.30535892203916e2,    -0.65964749423638e1,  0.93965400878363e-2,
            0.11573647505340e-6,  -0.25858641282073e-4,  -0.40644363084799e-8, 0.66456186191635e-7,
            0.80670734103027e-10, -0.93477771213947e-12, 0.58265442020601e-14, -0.15020185953503e-16,
        };

        auto pi = pressure / kPStar;
        auto eta = enthalpy / kHStar;

        auto result = kN * std::pow(pi, kI) * std::pow(eta + 1.0, kJ);

        return TemperatureT<Kelvin>{kTStar * result.sum()};
    }

    auto BackwardsLiquid::BackwardTemperature(
        PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy
    ) -> TemperatureT<Kelvin>
    {
        const Exponent kI = {
            0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 2.0, 2.0, 3.0, 3.0, 4.0,
        };

        const Exponent kJ = {
            0.0, 1.0, 2.0, 3.0, 11.0, 31.0, 0.0, 1.0, 2.0, 3.0, 12.0, 31.0, 0.0, 1.0, 2.0, 9.0, 31.0, 10.0, 32.0, 32.0,
        };

        const Coefficient kN = {
            0.17478268058307e3,    0.34806930892873e2,    0.65292584978455e1,   0.33039981775489,
            -0.19281382923196e-6,  -0.24909197244573e-22, -0.26107636489332,    0.22592965981586,
            -0.64256463395226e-1,  0.78876289270526e-2,   0.35672110607366e-9,  0.17332496994895e-23,
            0.56608900654837e-3,   -0.32635483139717e-3,  0.44778286690632e-4,  -0.51322156908507e-9,
            -0.42522657042207e-25, 0.26400441360689e-12,  0.78124600459723e-28, -0.30732199903668e-30,
        };
        auto pi = pressure / kPStar;
        auto sigma = entropy / kSStar;
        auto result = kN * std::pow(pi, kI) * std::pow(sigma + 2.0, kJ);
        return TemperatureT<Kelvin>{kTStar * result.sum()};
    }

    auto BackwardsLiquid::Validate(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) -> bool
    {
        if (!ValidatePressure(pressure))
        {
            return false;
        }

        auto saturationTemperature = SaturationTemperature(pressure);

        BasicLiquid saturationLimit{pressure, saturationTemperature};
        BasicLiquid freezingIsotherm{pressure, kTriplePointTemperature};
        BasicLiquid maxIsotherm{pressure, kSubcooledMaxTemperature};

        return enthalpy <= saturationLimit.Enthalpy() && enthalpy >= freezingIsotherm.Enthalpy() &&
            enthalpy <= maxIsotherm.Enthalpy();
    }

    auto BackwardsLiquid::Validate(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
        -> bool
    {
        if (!ValidatePressure(pressure))
        {
            return false;
        }

        auto saturationTemperature = SaturationTemperature(pressure);

        BasicLiquid saturationLimit{pressure, saturationTemperature};
        BasicLiquid freezingIsotherm{pressure, kTriplePointTemperature};
        BasicLiquid maxIsotherm{pressure, kSubcooledMaxTemperature};

        return entropy <= saturationLimit.Entropy() && entropy >= freezingIsotherm.Entropy() &&
            entropy <= maxIsotherm.Entropy();
    }

} // namespace fluid_props::water
