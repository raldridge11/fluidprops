#include "water/iapwsif97/iapws_constants.hpp"
#include "water/iapwsif97/transport/transport.hpp"

namespace fluid_props::water
{
    void IAPWSIF97Viscosity::SetState(DensityT<KiloGram_CubicMeter> density, TemperatureT<Kelvin> temperature)
    {
        this->density_ = density;
        this->temperature_ = temperature;

        this->theta_ = temperature / kCriticalTemperature;
        this->delta_ = density / kCriticalDensity;
    }

    auto IAPWSIF97Viscosity::Viscosity() -> ViscosityT<PascalSecond>
    {
        return ViscosityT<PascalSecond>{IAPWSIF97Viscosity::kEtaStar * this->psi_0() * this->psi_1()};
    }

    auto IAPWSIF97Viscosity::WithinValidRange(PressureT<MegaPascal> pressure) -> bool
    {
        if (pressure > kMaxPressure || this->temperature_ < kTriplePointTemperature)
        {
            return false;
        }

        if (this->temperature_ > kHighMaxTemperature &&
            (pressure > KHighMaxPressure || this->temperature_ > kHighMaxTemperature))
        {
            return false;
        }

        return true;
    }

    auto IAPWSIF97Viscosity::psi_0() const -> Property
    {
        auto result = IAPWSIF97Viscosity::kN0 * std::pow(this->theta_, 1.0 - IAPWSIF97Viscosity::kI0);
        return std::sqrt(this->theta_) / result.sum();
    }

    auto IAPWSIF97Viscosity::psi_1() const -> Property
    {
        auto result = IAPWSIF97Viscosity::kN * std::pow(this->delta_ - 1.0, IAPWSIF97Viscosity::kI) *
            std::pow(1.0 / this->theta_ - 1.0, IAPWSIF97Viscosity::kJ);

        return std::exp(this->delta_ * result.sum());
    }

    const ViscosityT<PascalSecond> IAPWSIF97Viscosity::kEtaStar{1.0e-6};

    const Coefficient IAPWSIF97Viscosity::kN0 = {
        0.167752e-1,
        0.220462e-1,
        0.6366564e-2,
        -0.241605e-2,
    };

    const Coefficient IAPWSIF97Viscosity::kN = {
        0.520094,   0.850895e-1, -0.108374e1,  -0.289555,   0.222531,    0.999115,     0.188797e1,
        0.126613e1, 0.120573,    -0.281378,    -0.906851,   -0.772479,   -0.489837,    -0.257040,
        0.161913,   0.257399,    -0.325372e-1, 0.698452e-1, 0.872102e-2, -0.435673e-2, -0.593264e-3,
    };

    const Exponent IAPWSIF97Viscosity::kI0 = {
        1.0,
        2.0,
        3.0,
        4.0,
    };

    const Exponent IAPWSIF97Viscosity::kI = {
        0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 2.0, 2.0, 3.0, 3.0, 4.0, 4.0, 5.0, 6.0, 6.0,
    };

    const Exponent IAPWSIF97Viscosity::kJ = {
        0.0, 1.0, 2.0, 3.0, 0.0, 1.0, 2.0, 3.0, 5.0, 0.0, 1.0, 2.0, 3.0, 4.0, 0.0, 1.0, 0.0, 3.0, 4.0, 3.0, 5.0,
    };

} // namespace fluid_props::water
