#include "water/iapwsif97/iapws_constants.hpp"
#include "water/iapwsif97/transport/transport.hpp"

namespace fluid_props::water
{
    void IAPWSIF97ThermalConductivity::SetState(DensityT<KiloGram_CubicMeter> density, TemperatureT<Kelvin> temperature)
    {
        this->density_ = density;
        this->temperature_ = temperature;

        this->delta_ = this->density_ / IAPWSIF97ThermalConductivity::kDStar;
        this->theta_ = this->temperature_ / IAPWSIF97ThermalConductivity::kTStar;
    }

    auto IAPWSIF97ThermalConductivity::ThermalConductivity() -> ThermalConductivityT<Watts_Meter_Kelvin>
    {
        return ThermalConductivityT<Watts_Meter_Kelvin>{
            IAPWSIF97ThermalConductivity::kLStar * (this->lambda_0() + this->lambda_1() + this->lambda_2())
        };
    }

    auto IAPWSIF97ThermalConductivity::WithinValidRange(PressureT<MegaPascal> pressure) -> bool
    {
        if (pressure > kMaxPressure || this->temperature_ < kTriplePointTemperature)
        {
            return false;
        }

        if (this->temperature_ > kHighMaxTemperature &&
            (pressure > KHighMaxPressure || this->temperature_ > kHighMaxTemperature))
        {
            return false;
        }

        return true;
    }

    auto IAPWSIF97ThermalConductivity::lambda_0() const -> Property
    {
        auto result =
            IAPWSIF97ThermalConductivity::kNIdeal * std::pow(this->theta_, IAPWSIF97ThermalConductivity::kI - 1.0);
        return std::sqrt(this->theta_) * result.sum();
    }

    auto IAPWSIF97ThermalConductivity::lambda_1() const -> Property
    {
        auto exponent = std::exp(
            IAPWSIF97ThermalConductivity::kN1[3] * std::pow(this->delta_ + IAPWSIF97ThermalConductivity::kN1[4], 2.0)
        );
        return IAPWSIF97ThermalConductivity::kN1[0] + IAPWSIF97ThermalConductivity::kN1[1] * this->delta_ +
            IAPWSIF97ThermalConductivity::kN1[2] * exponent;
    }

    auto IAPWSIF97ThermalConductivity::lambda_2() const -> Property
    {
        auto first_term = (IAPWSIF97ThermalConductivity::kN2[0] * std::pow(this->theta_, -10.0) +
                           IAPWSIF97ThermalConductivity::kN2[1]) *
            std::pow(this->delta_, 1.8) *
            std::exp(IAPWSIF97ThermalConductivity::kN2[2] * (1.0 - std::pow(this->delta_, 2.8)));

        auto second_term = IAPWSIF97ThermalConductivity::kN2[3] * this->factor_a() *
            std::pow(this->delta_, this->factor_b()) *
            std::exp((this->factor_b() / (1.0 + this->factor_b())) *
                     (1.0 - std::pow(this->delta_, 1.0 + this->factor_b())));

        auto third_term = IAPWSIF97ThermalConductivity::kN2[4] *
            std::exp(IAPWSIF97ThermalConductivity::kN2[5] * std::pow(this->theta_, 1.5) +
                     IAPWSIF97ThermalConductivity::kN2[6] * std::pow(this->delta_, -5.0));

        return first_term + second_term + third_term;
    }

    auto IAPWSIF97ThermalConductivity::factor_a() const -> Property
    {
        return 2.0 + IAPWSIF97ThermalConductivity::kN2[7] * std::pow(this->delta_theta(), -0.6);
    }

    auto IAPWSIF97ThermalConductivity::factor_b() const -> Property
    {
        if (this->theta_ >= 1.0)
        {
            return 1.0 / this->delta_theta();
        }
        return IAPWSIF97ThermalConductivity::kN2[8] * std::pow(this->delta_theta(), -0.6);
    }

    auto IAPWSIF97ThermalConductivity::delta_theta() const -> Property
    {
        return std::abs(this->theta_ - 1.0) + IAPWSIF97ThermalConductivity::kN2[9];
    }

    const TemperatureT<Kelvin> IAPWSIF97ThermalConductivity::kTStar{647.26000};
    const DensityT<KiloGram_CubicMeter> IAPWSIF97ThermalConductivity::kDStar{317.70000};
    const ThermalConductivityT<Watts_Meter_Kelvin> IAPWSIF97ThermalConductivity::kLStar{1.0};

    const Exponent IAPWSIF97ThermalConductivity::kI = {
        1.0,
        2.0,
        3.0,
        4.0,
    };

    const Coefficient IAPWSIF97ThermalConductivity::kNIdeal = {
        0.102811e-1,
        0.299621e-1,
        0.156146e-1,
        -0.422464e-2,
    };

    const Coefficient IAPWSIF97ThermalConductivity::kN1 = {
        -0.397070,
        0.400302,
        0.106000e1,
        -0.171587,
        0.239219e1,
    };

    const Coefficient IAPWSIF97ThermalConductivity::kN2 = {
        0.701309e-1,
        0.118520e-1,
        0.642857,
        0.169937e-2,
        -0.102000e1,
        -0.411717e1,
        -0.617937e1,
        0.822994e-1,
        0.100932e2,
        0.308976e-2,
    };

} // namespace fluid_props::water
