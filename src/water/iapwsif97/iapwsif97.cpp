#include "water/iapwsif97/iapwsif97.hpp"
#include "exceptions/exceptions.hpp"
#include "logger/logger.hpp"
#include "math_methods/math_methods.hpp"
#include "water/iapwsif97/iapws_constants.hpp"
#include "water/iapwsif97/transport/transport.hpp"
#include <fmt/core.h>
#include <spdlog/spdlog.h>

namespace fluid_props::water
{
    IAPWSIF97::IAPWSIF97(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature) : IAPWSIF97()
    {
        this->SetState(pressure, temperature);
    }

    IAPWSIF97::IAPWSIF97(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy) : IAPWSIF97()
    {
        this->SetState(pressure, enthalpy);
    }

    IAPWSIF97::IAPWSIF97(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
        : IAPWSIF97()
    {
        this->SetState(pressure, entropy);
    }

    IAPWSIF97::IAPWSIF97(PressureT<MegaPascal> pressure, QualityT<Dimensionless> quality) : IAPWSIF97()
    {
        this->SetState(pressure, quality);
    }

    IAPWSIF97::IAPWSIF97(TemperatureT<Kelvin> temperature, QualityT<Dimensionless> quality) : IAPWSIF97()
    {
        this->SetState(temperature, quality);
    }

    IAPWSIF97::IAPWSIF97(
        SpecificEnergyT<KiloJoule_KiloGram> enthalpy, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy
    )
        : IAPWSIF97()
    {
        this->SetState(enthalpy, entropy);
    }

    void IAPWSIF97::Pressure(PressureT<MegaPascal> pressure)
    {
        this->reset();
        this->saturation_->Pressure(pressure);
    }

    void IAPWSIF97::Enthalpy(SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
    {
        this->reset();
        this->enthalpy_ = enthalpy;
    }

    void IAPWSIF97::Temperature(TemperatureT<Kelvin> temperature)
    {
        this->reset();
        this->saturation_->Temperature(temperature);
    }

    void IAPWSIF97::Entropy(SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
    {
        this->reset();
        this->entropy_ = entropy;
    }

    void IAPWSIF97::Quality(QualityT<Dimensionless> quality)
    {
        this->reset();
        this->quality_ = quality;
    }

    void IAPWSIF97::SetState(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature)
    {
        this->reset();
        this->pressure_ = pressure;
        this->temperature_ = temperature;

        this->liquid_.SetState(pressure, temperature);
        this->steam_.SetState(pressure, temperature);
        this->highTemperature_.SetState(pressure, temperature);
        this->supercritical_.SetState(pressure, temperature);

        this->set_calculator();
        this->setup_transport_calculations();
    }

    void IAPWSIF97::SetState(PressureT<MegaPascal> pressure, SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
    {
        this->reset();
        this->pressure_ = pressure;
        this->liquid_.SetState(pressure, enthalpy);
        this->steam_.SetState(pressure, enthalpy);
        this->highTemperature_.SetState(pressure, enthalpy);
        this->supercritical_.SetState(pressure, enthalpy);

        auto region = this->get_valid_region();
        IAPWSIF97::SetState(pressure, TemperatureT<Kelvin>(region->Temperature()));

        this->enthalpy_ = enthalpy;

        this->setup_transport_calculations();
    }

    void IAPWSIF97::SetState(PressureT<MegaPascal> pressure, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy)
    {
        this->reset();
        this->pressure_ = pressure;
        this->liquid_.SetState(pressure, entropy);
        this->steam_.SetState(pressure, entropy);
        this->highTemperature_.SetState(pressure, entropy);
        this->supercritical_.SetState(pressure, entropy);

        auto region = this->get_valid_region();
        IAPWSIF97::SetState(pressure, TemperatureT<Kelvin>(region->Temperature()));

        this->entropy_ = entropy;

        this->setup_transport_calculations();
    }

    void IAPWSIF97::SetState(PressureT<MegaPascal> pressure, QualityT<Dimensionless> quality)
    {
        this->reset();
        this->pressure_ = pressure;
        this->quality_ = quality;
        this->saturation_->Pressure(pressure);

        this->setup_mixture_calculations();
    }

    void IAPWSIF97::SetState(TemperatureT<Kelvin> temperature, QualityT<Dimensionless> quality)
    {
        this->reset();
        this->temperature_ = temperature;
        this->quality_ = quality;
        this->saturation_->Temperature(temperature);

        this->setup_mixture_calculations();
    }

    void IAPWSIF97::SetState(
        SpecificEnergyT<KiloJoule_KiloGram> enthalpy, SpecificEntropyT<KiloJoule_KiloGram_Kelvin> entropy
    )
    {
        this->reset();
        this->enthalpy_ = enthalpy;
        this->entropy_ = entropy;
    }

    auto IAPWSIF97::Density() -> DensityT<KiloGram_CubicMeter>
    {
        auto specificVolume = IAPWSIF97::SpecificVolume().Value();
        return DensityT<KiloGram_CubicMeter>{specificVolume != 0.0 ? 1.0 / specificVolume : 0.0};
    }

    void IAPWSIF97::reset()
    {
        this->pressure_.Reset();
        this->temperature_.Reset();
        this->enthalpy_.Reset();
        this->entropy_.Reset();
        this->quality_.Reset();
        this->specific_volume_.Reset();
        this->internal_energy_.Reset();
        this->isobaric_heat_capacity_.Reset();
        this->isochoric_heat_capacity_.Reset();
        this->speed_of_sound_.Reset();
        this->cubic_expansion_.Reset();
        this->compressibility_.Reset();
        this->viscosity_.Reset();

        this->enthalpy_.SetMethod([this]() { return this->calculator_->Enthalpy(); });
        this->specific_volume_.SetMethod([this]() { return this->calculator_->SpecificVolume(); });
        this->internal_energy_.SetMethod([this]() { return this->calculator_->InternalEnergy(); });
        this->entropy_.SetMethod([this] { return this->calculator_->Entropy(); });
        this->isobaric_heat_capacity_.SetMethod([this]() { return this->calculator_->IsobaricHeatCapacity(); });
        this->isochoric_heat_capacity_.SetMethod([this]() { return this->calculator_->IsochoricHeatCapacity(); });

        this->speed_of_sound_.SetMethod([this]() { return this->calculator_->SpeedOfSound(); });
        this->cubic_expansion_.SetMethod([this]() { return this->calculator_->CubicExpansion(); });
        this->compressibility_.SetMethod([this]() { return this->calculator_->Compressibility(); });

        this->quality_.SetMethod([]() { return QualityT<Dimensionless>{}; });
        this->void_.SetMethod([]() { return VoidT<Dimensionless>{}; });
    }

    auto IAPWSIF97::get_valid_region() -> Fluid *
    {
        if (this->liquid_.WithinValidRange())
        {
            return &this->liquid_;
        }
        else if (this->steam_.WithinValidRange())
        {
            return &this->steam_;
        }
        else if (this->highTemperature_.WithinValidRange())
        {
            return &this->highTemperature_;
        }
        else if (this->supercritical_.WithinValidRange())
        {
            return &this->supercritical_;
        }
        auto message = fmt::format(
            "Pressure of {} or Temperature of {} is outside the valid range!\n"
            "The valid range of IAPWSIF97 is:\n"
            "{} <= T <= {} 0 < p <= {} \n"
            "{} <= T <= {} 0 < p <= {} \n",
            this->Pressure(),
            this->Temperature(),
            kTriplePointTemperature,
            kMaxTemperature,
            kMaxPressure,
            kMaxTemperature,
            kHighMaxTemperature,
            KHighMaxPressure
        );
        Logger::Get()->error(message);
        throw PropertyOutOfBounds(message);
    }

    void IAPWSIF97::set_calculator()
    {
        if (!this->pressure_.IsSet() || !this->temperature_.IsSet())
        {
            return;
        }
        this->calculator_ = this->get_valid_region();
    }

    void IAPWSIF97::setup_mixture_calculations()
    {
        this->void_.SetMethod([this]() {
            auto liquidSpecificVolume = this->saturation_->SpecificVolume(Phase::Liquid);
            auto vaporSpecificVolume = this->saturation_->SpecificVolume(Phase::Vapor);
            return VoidT<Dimensionless>{
                this->Quality() * vaporSpecificVolume /
                MixProperties(this->Quality(), liquidSpecificVolume, vaporSpecificVolume)
            };
        });

        this->enthalpy_.SetMethod([this]() {
            return MixProperties(
                this->Quality(),
                this->saturation_->Enthalpy(Phase::Liquid),
                this->saturation_->Enthalpy(Phase::Vapor)
            );
        });

        this->specific_volume_.SetMethod([this]() {
            return MixProperties(
                this->VoidFraction(),
                this->saturation_->SpecificVolume(Phase::Liquid),
                this->saturation_->SpecificVolume(Phase::Vapor)
            );
        });

        this->internal_energy_.SetMethod([this]() {
            return MixProperties(
                this->Quality(),
                this->saturation_->InternalEnergy(Phase::Liquid),
                this->saturation_->InternalEnergy(Phase::Vapor)
            );
        });

        this->entropy_.SetMethod([this]() {
            return MixProperties(
                this->Quality(),
                this->saturation_->Entropy(Phase::Liquid),
                this->saturation_->Entropy(Phase::Vapor)
            );
        });

        this->isobaric_heat_capacity_.SetMethod([this]() {
            return MixProperties(
                this->Quality(),
                this->saturation_->IsobaricHeatCapacity(Phase::Liquid),
                this->saturation_->IsobaricHeatCapacity(Phase::Vapor)
            );
        });

        this->isochoric_heat_capacity_.SetMethod([this]() {
            return MixProperties(
                this->Quality(),
                this->saturation_->IsochoricHeatCapacity(Phase::Liquid),
                this->saturation_->IsochoricHeatCapacity(Phase::Vapor)
            );
        });

        this->speed_of_sound_.SetMethod([this]() {
            return MixProperties(
                this->VoidFraction(),
                this->saturation_->SpeedOfSound(Phase::Liquid),
                this->saturation_->SpeedOfSound(Phase::Vapor)
            );
        });

        this->cubic_expansion_.SetMethod([this]() {
            return MixProperties(
                this->VoidFraction(),
                this->saturation_->CubicExpansion(Phase::Liquid),
                this->saturation_->CubicExpansion(Phase::Vapor)
            );
        });

        this->compressibility_.SetMethod([this]() {
            return MixProperties(
                this->VoidFraction(),
                this->saturation_->Compressibility(Phase::Liquid),
                this->saturation_->Compressibility(Phase::Vapor)
            );
        });
    }

    void IAPWSIF97::setup_transport_calculations()
    {
        this->viscosity_.SetMethod([this]() {
            auto viscosity = IAPWSIF97Viscosity(this->Density(), this->Temperature());
            if (!viscosity.WithinValidRange(this->Pressure()))
            {
                auto message = fmt::format(
                    "Pressure or Temperature out of bounds for viscosity "
                    "calculation. Pressure: {} Temperature: {}",
                    this->Pressure(),
                    this->Temperature()
                );
                Logger::Get()->error(message);
                throw PropertyOutOfBounds(message);
            }

            return viscosity.Viscosity();
        });

        this->thermal_conductivity_.SetMethod([this]() {
            auto thermal_conductivity = IAPWSIF97ThermalConductivity(this->Density(), this->Temperature());

            if (!thermal_conductivity.WithinValidRange(this->Pressure()))
            {
                auto message = fmt::format(
                    "Pressure or Temperature out of bounds for thermal conductivity "
                    "calculation. Pressure: {} Temperature: {}",
                    this->Pressure(),
                    this->Temperature()
                );
                Logger::Get()->error(message);
                throw PropertyOutOfBounds(message);
            }

            return thermal_conductivity.ThermalConductivity();
        });
    }

} // namespace fluid_props::water
