#include "water/iapwsif97/saturation.hpp"
#include "water/iapwsif97/iapws_constants.hpp"

namespace fluid_props::water
{
    void IAPWSIF97Saturation::Pressure(PressureT<MegaPascal> pressure)
    {
        this->pressure_ = pressure;
        this->saturation_line_.SetState(pressure);
        auto temperature = TemperatureT<Kelvin>(this->saturation_line_.Temperature());
        this->set_phases(pressure, temperature);
        this->temperature_ = temperature;
        this->reset();
    }

    void IAPWSIF97Saturation::Temperature(TemperatureT<Kelvin> temperature)
    {
        this->temperature_ = temperature;
        this->saturation_line_.SetState(temperature);
        auto pressure = PressureT<MegaPascal>(this->saturation_line_.Pressure());
        this->set_phases(pressure, temperature);
        this->pressure_ = pressure;
        this->reset();
    }

    auto IAPWSIF97Saturation::Enthalpy(Phase phase) -> SpecificEnergyT<KiloJoule_KiloGram>
    {
        return this->enthalpy_[phase].Value();
    }

    auto IAPWSIF97Saturation::Entropy(Phase phase) -> SpecificEntropyT<KiloJoule_KiloGram_Kelvin>
    {
        return this->entropy_[phase].Value();
    }

    auto IAPWSIF97Saturation::SpecificVolume(Phase phase) -> SpecificVolumeT<CubicMeter_KiloGram>
    {
        return this->specific_volume_[phase].Value();
    }

    auto IAPWSIF97Saturation::Density(Phase phase) -> DensityT<KiloGram_CubicMeter>
    {
        auto specific_volume = this->SpecificVolume(phase).Value();
        return DensityT<KiloGram_CubicMeter>{specific_volume != 0.0 ? 1.0 / specific_volume : 0.0};
    }

    auto IAPWSIF97Saturation::InternalEnergy(Phase phase) -> SpecificEnergyT<KiloJoule_KiloGram>
    {
        return this->internal_energy_[phase].Value();
    }

    auto IAPWSIF97Saturation::IsobaricHeatCapacity(Phase phase) -> HeatCapacityT<KiloJoule_KiloGram_Kelvin>
    {
        return this->isobaric_heat_capacity_[phase].Value();
    }

    auto IAPWSIF97Saturation::IsochoricHeatCapacity(Phase phase) -> HeatCapacityT<KiloJoule_KiloGram_Kelvin>
    {
        return this->isochoric_heat_capacity_[phase].Value();
    }

    auto IAPWSIF97Saturation::SpeedOfSound(Phase phase) -> VelocityT<Meter_Second>
    {
        return this->speed_of_sound_[phase].Value();
    }

    auto IAPWSIF97Saturation::CubicExpansion(Phase phase) -> ThermalExpansionT<IKelvin>
    {
        return this->cubic_expansion_[phase].Value();
    }

    auto IAPWSIF97Saturation::Compressibility(Phase phase) -> CompressibilityT<IMegaPascal>
    {
        return this->compressibility_[phase].Value();
    }

    auto IAPWSIF97Saturation::SurfaceTension() -> SurfaceTensionT<Newton_Meter>
    {
        return this->surface_tension_.Value();
    }

    void IAPWSIF97Saturation::set_phases(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature)
    {
        this->liquid_.SetState(pressure, temperature);
        this->steam_.SetState(pressure, temperature);
    }

    void IAPWSIF97Saturation::reset()
    {
        this->reset_map(this->enthalpy_);
        this->reset_map(this->entropy_);
        this->reset_map(this->specific_volume_);
        this->reset_map(this->internal_energy_);
        this->reset_map(this->isochoric_heat_capacity_);
        this->reset_map(this->isobaric_heat_capacity_);
        this->reset_map(this->speed_of_sound_);
        this->reset_map(this->cubic_expansion_);
        this->reset_map(this->compressibility_);
        this->surface_tension_.Reset();
    }

    auto IAPWSIF97Saturation::sigma() -> SurfaceTensionT<Newton_Meter>
    {
        constexpr double kB = 235.8;
        constexpr double kb = -0.625;
        constexpr double kMu = 1.256;
        constexpr double kSigmaStar = 1.0e-3;

        auto tau = 1.0 - IAPWSIF97Saturation::Temperature() / kCriticalTemperature;
        return SurfaceTensionT<Newton_Meter>{kSigmaStar * kB * std::pow(tau, kMu) * (1.0 + kb * tau)};
    }

} // namespace fluid_props::water
