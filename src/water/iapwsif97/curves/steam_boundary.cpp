#include "water/iapwsif97/curves/steam_boundary.hpp"
#include "exceptions/exceptions.hpp"
#include "logger/logger.hpp"
#include "water/iapwsif97/iapws_constants.hpp"
#include <fmt/core.h>

namespace fluid_props::water
{
    void SteamBoundaryCurve::SetState(PressureT<MegaPascal> pressure)
    {
        this->pressure_ = pressure;
        this->pi_ = pressure / SteamBoundaryCurve::kPStar;
    }

    void SteamBoundaryCurve::SetState(SpecificEnergyT<KiloJoule_KiloGram> enthalpy)
    {
        this->enthalpy_ = enthalpy;
        this->eta_ = enthalpy / SteamBoundaryCurve::kHStar;
    }

    auto SteamBoundaryCurve::Pressure() const -> PressureT<MegaPascal>
    {
        if (this->enthalpy_ < SteamBoundaryCurve::kMinEnthalpy || this->enthalpy_ > SteamBoundaryCurve::kMaxEnthalpy)
        {
            auto message = fmt::format(
                "Enthalpy must be between {} and {}! Received: {}.",
                SteamBoundaryCurve::kMinEnthalpy,
                SteamBoundaryCurve::kMaxEnthalpy,
                this->enthalpy_
            );
            Logger::Get()->error(message);
            throw PropertyOutOfBounds(message);
        }

        auto result = SteamBoundaryCurve::kN[0] + SteamBoundaryCurve::kN[1] * this->eta_ +
            SteamBoundaryCurve::kN[2] * std::pow(this->eta_, 2.0);
        return PressureT<MegaPascal>{SteamBoundaryCurve::kPStar * result};
    }

    auto SteamBoundaryCurve::Enthalpy() const -> SpecificEnergyT<KiloJoule_KiloGram>
    {
        if (this->pressure_ < SteamBoundaryCurve::kMinPressure || this->pressure_ > kMaxPressure)
        {
            auto message = fmt::format(
                "Pressure must be between {} and {}! Received: {}",
                SteamBoundaryCurve::kMinPressure,
                kMaxPressure,
                this->pressure_
            );
            Logger::Get()->error(message);
            throw PropertyOutOfBounds(message);
        }

        auto result =
            SteamBoundaryCurve::kN[3] + std::sqrt((this->pi_ - SteamBoundaryCurve::kN[4]) / SteamBoundaryCurve::kN[2]);
        return SpecificEnergyT<KiloJoule_KiloGram>{SteamBoundaryCurve::kHStar * result};
    }

    const PressureT<MegaPascal> SteamBoundaryCurve::kPStar{1.0};
    const SpecificEnergyT<KiloJoule_KiloGram> SteamBoundaryCurve::kHStar{1.0};

    const Coefficient SteamBoundaryCurve::kN = {
        0.90584278514723e3,
        -0.67955786399241,
        0.12809002730136e-3,
        0.26526571908428e4,
        0.45257578905948e1,
    };

    const PressureT<MegaPascal> SteamBoundaryCurve::kMinPressure{6.546699678};
    const SpecificEnergyT<KiloJoule_KiloGram> SteamBoundaryCurve::kMinEnthalpy{2278.265754};
    const SpecificEnergyT<KiloJoule_KiloGram> SteamBoundaryCurve::kMaxEnthalpy{3516.004323};
} // namespace fluid_props::water
