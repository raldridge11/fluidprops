#include "water/iapwsif97/curves/saturation.hpp"
#include "exceptions/exceptions.hpp"
#include "logger/logger.hpp"
#include "water/iapwsif97/iapws_constants.hpp"
#include <fmt/core.h>

namespace fluid_props::water
{
    void SaturationCurve::SetState(TemperatureT<Kelvin> temperature)
    {
        this->temperature_ = temperature;
        this->calculate_theta();
    }

    void SaturationCurve::SetState(PressureT<MegaPascal> pressure)
    {
        this->pressure_ = pressure;
        this->calculate_beta();
    }

    auto SaturationCurve::Pressure() const -> PressureT<MegaPascal>
    {
        if (this->temperature_ < kTriplePointTemperature || this->temperature_ > kCriticalTemperature)
        {
            auto message = fmt::format(
                "Temperature must be between {} and {}! Received: {}",
                kTriplePointTemperature,
                kCriticalTemperature,
                this->temperature_
            );
            Logger::Get()->error(message);
            throw PropertyOutOfBounds(message);
        }
        auto coefficientA =
            std::pow(this->theta_, 2) + this->coefficient(this->theta_, SaturationCurve::kN[std::slice(0, 2, 1)]);
        auto coefficientB = this->coefficient(this->theta_, SaturationCurve::kN[std::slice(2, 3, 1)]);
        auto coefficientC = this->coefficient(this->theta_, SaturationCurve::kN[std::slice(5, 3, 1)]);

        auto quotient = 2.0 * coefficientC /
            (-1.0 * coefficientB + std::sqrt(coefficientB * coefficientB - 4.0 * coefficientA * coefficientC));

        return PressureT<MegaPascal>{SaturationCurve::kPStar * std::pow(quotient, 4)};
    }

    auto SaturationCurve::Temperature() const -> TemperatureT<Kelvin>
    {
        if (this->pressure_ < kTriplePointPressure || this->pressure_ > kCriticalPressure)
        {
            auto message = fmt::format(
                "Pressure must be between {} and {} ! Receive: {}",
                kTriplePointPressure,
                kCriticalPressure,
                this->pressure_
            );
            Logger::Get()->error(message);
            throw PropertyOutOfBounds(message);
        }
        auto coefficientE =
            std::pow(this->beta_, 2) + this->coefficient(this->beta_, SaturationCurve::kN[std::slice(2, 2, 3)]);
        auto coefficientF = this->coefficient(this->beta_, SaturationCurve::kN[std::slice(0, 3, 3)]);
        auto coefficientG = this->coefficient(this->beta_, SaturationCurve::kN[std::slice(1, 3, 3)]);

        auto coefficientD = 2.0 * coefficientG /
            (-1.0 * coefficientF - std::sqrt(std::pow(coefficientF, 2) - 4.0 * coefficientE * coefficientG));

        auto a = std::pow(SaturationCurve::kN[9] + coefficientD, 2);
        auto b = 4.0 * (SaturationCurve::kN[8] + SaturationCurve::kN[9] * coefficientD);

        return TemperatureT<Kelvin>{
            SaturationCurve::kTStar * (SaturationCurve::kN[9] + coefficientD - std::sqrt(a - b)) / 2.0
        };
    }

    void SaturationCurve::calculate_theta()
    {
        auto ratio = this->temperature_ / SaturationCurve::kTStar;
        this->theta_ = ratio + SaturationCurve::kN[8] / (ratio - SaturationCurve::kN[9]);
    }

    void SaturationCurve::calculate_beta()
    {
        this->beta_ = std::pow(this->pressure_.Value(), 1.0 / 4.0);
    }

    auto SaturationCurve::coefficient(double value, const Coefficient &coefficients) const -> double
    {
        size_t order = coefficients.size();
        auto result = 0.0;
        for (size_t i = 0; i < coefficients.size(); i++)
        {
            auto exponent = order - 1 - i;
            result += coefficients[i] * std::pow(value, exponent);
        }
        return result;
    }

    const PressureT<MegaPascal> SaturationCurve::kPStar{1.0};
    const TemperatureT<Kelvin> SaturationCurve::kTStar{1.0};

    const Coefficient SaturationCurve::kN = {
        0.11670521452767e4,
        -0.72421316703206e6,
        -0.17073846940092e2,
        0.12020824702470e5,
        -0.32325550322333e7,
        0.14915108613530e2,
        -0.48232657361591e4,
        0.40511340542057e6,
        -0.23855557567849,
        0.65017534844798e3
    };

} // namespace fluid_props::water
