#include "water/iapwsif97/curves/supercritical.hpp"
#include "exceptions/exceptions.hpp"
#include "logger/logger.hpp"
#include "water/iapwsif97/iapws_constants.hpp"
#include <fmt/core.h>

namespace fluid_props::water
{
    void SuperCriticalCurve::SetState(PressureT<MegaPascal> pressure)
    {
        this->pressure_ = pressure;
        this->pi_ = this->pressure_ / SuperCriticalCurve::kPStar;
    }

    void SuperCriticalCurve::SetState(TemperatureT<Kelvin> temperature)
    {
        this->temperature_ = temperature;
        this->theta_ = this->temperature_ / SuperCriticalCurve::kTStar;
    }

    auto SuperCriticalCurve::Pressure() const -> PressureT<MegaPascal>
    {
        if (this->temperature_ < kSubcooledMaxTemperature || this->temperature_ > kMaxTemperatureSuperCritical)
        {
            auto message = fmt::format(
                "Temperature must be between {} and {}! Received: {}",
                kSubcooledMaxTemperature,
                kMaxTemperatureSuperCritical,
                this->temperature_
            );
            Logger::Get()->error(message);
            throw PropertyOutOfBounds(message);
        }
        auto result = SuperCriticalCurve::kN[0] + SuperCriticalCurve::kN[1] * this->theta_ +
            SuperCriticalCurve::kN[2] * this->theta_ * this->theta_;
        return PressureT<MegaPascal>{SuperCriticalCurve::kPStar * result};
    }

    auto SuperCriticalCurve::Temperature() const -> TemperatureT<Kelvin>
    {
        if (this->pressure_ < kMinCriticalPressure || this->pressure_ > kMaxPressure)
        {
            auto message = fmt::format(
                "Pressure must be between {} and {}! Receive: {}",
                kMinCriticalPressure,
                kMaxPressure,
                this->pressure_
            );
            Logger::Get()->error(message);
            throw PropertyOutOfBounds(message);
        }

        auto quotient = (this->pi_ - SuperCriticalCurve::kN[4]) / SuperCriticalCurve::kN[2];
        auto result = SuperCriticalCurve::kN[3] + std::sqrt(quotient);
        return TemperatureT<Kelvin>{SuperCriticalCurve::kTStar * result};
    }

    const PressureT<MegaPascal> SuperCriticalCurve::kPStar{1.0};
    const TemperatureT<Kelvin> SuperCriticalCurve::kTStar{1.0};

    const Coefficient SuperCriticalCurve::kN = {
        0.34805185628969e3,
        -0.11671859879975e1,
        0.10192970039326e-2,
        0.57254459862746e3,
        0.13918839778870e2,
    };

    void SuperCriticalBoundary::SetState(PressureT<MegaPascal> pressure)
    {
        this->pressure_ = pressure;
        this->pi_ = pressure / SuperCriticalBoundary::kPStar;
    }

    auto SuperCriticalBoundary::Enthalpy() const -> SpecificEnergyT<KiloJoule_KiloGram>
    {
        if (this->pressure_ < kCriticalPressure || this->pressure_ > kMaxPressure)
        {
            auto message = fmt::format(
                "Pressure must be between {} MPa and {} MPa! Receive: {} MPa",
                kCriticalPressure.Value(),
                kMaxPressure.Value(),
                this->pressure_.Value()
            );
            Logger::Get()->error(message);
            throw PropertyOutOfBounds(message);
        }

        auto result = 0.0;
        for (size_t i = 0; i < 4; i++)
        {
            result += SuperCriticalBoundary::kHStar * SuperCriticalBoundary::kN[i] * std::pow(this->pi_, i);
        }

        return SpecificEnergyT<KiloJoule_KiloGram>{result};
    }

    const PressureT<MegaPascal> SuperCriticalBoundary::kPStar{1.0};
    const SpecificEnergyT<KiloJoule_KiloGram> SuperCriticalBoundary::kHStar{1.0};

    const Coefficient SuperCriticalBoundary::kN = {
        0.201464004206875e4,
        0.374696550136983e1,
        -0.219921901054187e-1,
        0.875131686009950e-4,
    };

} // namespace fluid_props::water
