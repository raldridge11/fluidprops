#include "water/iapwsif97/basic_equations/steam.hpp"
#include "water/iapwsif97/iapws_constants.hpp"

namespace fluid_props::water
{
    void BasicSteam::SetState(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature)
    {
        this->pressure_ = pressure;
        this->temperature_ = temperature;

        this->pi_ = this->pressure_ / BasicSteam::kPStar;
        this->tau_ = BasicSteam::kTStar / this->temperature_;
        this->tau_diff_ = this->tau_ - 0.5;

        this->saturation_.SetState(temperature);
        this->superCritical_.SetState(temperature);
    }

    auto BasicSteam::Enthalpy() const -> SpecificEnergyT<KiloJoule_KiloGram>
    {
        return SpecificEnergyT<KiloJoule_KiloGram>{
            this->temperature_ * kSpecificGas * this->tau_ * (this->ideal_gamma_tau() + this->residual_gamma_tau())
        };
    }

    auto BasicSteam::Entropy() const -> SpecificEntropyT<KiloJoule_KiloGram_Kelvin>
    {
        auto result = this->tau_ * (ideal_gamma_tau() + this->residual_gamma_tau()) -
            (this->ideal_gamma() + this->residual_gamma());
        return SpecificEntropyT<KiloJoule_KiloGram_Kelvin>{kSpecificGas * result};
    }

    auto BasicSteam::InternalEnergy() const -> SpecificEnergyT<KiloJoule_KiloGram>
    {
        auto result = this->Enthalpy() -
            kSpecificGas * this->temperature_ * this->pi_ * (this->ideal_gamma_pi() + this->residual_gamma_pi());
        return result;
    }

    auto BasicSteam::SpecificVolume() const -> SpecificVolumeT<CubicMeter_KiloGram>
    {
        /*
        The factor of 1000.0 is not documented anywhere, BUT it needs to be there so that the units of pressure
        are consistent with the units of kSpecificGas.
        */
        return SpecificVolumeT<CubicMeter_KiloGram>{
            kSpecificGas * this->temperature_ / this->pressure_ * this->pi_ *
            (this->ideal_gamma_pi() + this->residual_gamma_pi()) / 1000.0
        };
    }

    auto BasicSteam::IsobaricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin>
    {
        return HeatCapacityT<KiloJoule_KiloGram_Kelvin>{
            -1.0 * kSpecificGas * this->tau_ * this->tau_ *
            (this->ideal_gamma_tau_tau() + this->residual_gamma_tau_tau())
        };
    }

    auto BasicSteam::IsochoricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin>
    {
        auto numerator =
            1.0 + this->pi_ * this->residual_gamma_pi() - this->tau_ * this->pi_ * this->residual_gamma_pi_tau();
        auto denominator = 1.0 - this->pi_ * this->pi_ * this->residual_gamma_pi_pi();
        return this->IsobaricHeatCapacity() - kSpecificGas * numerator * numerator / denominator;
    }

    auto BasicSteam::SpeedOfSound() const -> VelocityT<Meter_Second>
    {
        auto a = 1.0 + 2.0 * this->pi_ * this->residual_gamma_pi() +
            this->pi_ * this->pi_ * std::pow(this->residual_gamma_pi(), 2.0);
        auto b = 1.0 - this->pi_ * this->pi_ * this->residual_gamma_pi_pi();
        auto c = 1.0 + this->pi_ * this->residual_gamma_pi() - this->tau_ * this->pi_ * this->residual_gamma_pi_tau();
        auto d = this->tau_ * this->tau_ * (this->ideal_gamma_tau_tau() + this->residual_gamma_tau_tau());
        auto e = c * c / d;
        auto f = a / (b + e);
        /*
        The factor of 1000.0 is not documented anywhere, BUT it needs to be there so that the units of pressure
        are consistent with the units of kSpecificGas.
        */
        return VelocityT<Meter_Second>{std::sqrt(1000.0 * kSpecificGas * this->temperature_ * f)};
    }

    auto BasicSteam::CubicExpansion() const -> ThermalExpansionT<IKelvin>
    {
        auto numerator =
            1.0 + this->pi_ * this->residual_gamma_pi() - this->tau_ * this->pi_ * this->residual_gamma_pi_tau();
        auto denominator = 1.0 + this->pi_ * this->residual_gamma_pi();
        return ThermalExpansionT<IKelvin>{numerator / denominator / this->temperature_};
    }

    auto BasicSteam::Compressibility() const -> CompressibilityT<IMegaPascal>
    {
        auto numerator = 1.0 - this->pi_ * this->pi_ * this->residual_gamma_pi_pi();
        auto denominator = 1.0 + this->pi_ * this->residual_gamma_pi();
        return CompressibilityT<IMegaPascal>{numerator / denominator / this->pressure_};
    }

    auto BasicSteam::WithinValidRange() const -> bool
    {
        // Are we below saturation for saturation range?
        auto belowSaturation = this->temperature_ >= kTriplePointTemperature &&
            this->temperature_ <= kSubcooledMaxTemperature && this->pressure_ <= this->saturation_.Pressure();
        if (belowSaturation)
        {
            return true;
        }

        // Are we below the super critical line?
        auto belowSuperCritical = this->temperature_ > kSubcooledMaxTemperature &&
            this->temperature_ <= kMaxTemperatureSuperCritical && this->pressure_ <= this->superCritical_.Pressure();
        if (belowSuperCritical)
        {
            return true;
        }

        if (this->temperature_ > kMaxTemperatureSuperCritical && this->temperature_ <= kMaxTemperature &&
            this->pressure_ <= kMaxPressure)
        {
            return true;
        }

        return false;
    }

    auto BasicSteam::ideal_gamma() const -> Property
    {
        auto result = BasicSteam::kIdealN * std::pow(this->tau_, BasicSteam::kIdealJ);
        return std::log(this->pi_) + result.sum();
    }

    auto BasicSteam::ideal_gamma_tau() const -> Property
    {
        auto result = BasicSteam::kIdealN * BasicSteam::kIdealJ * std::pow(this->tau_, BasicSteam::kIdealJ - 1.0);
        return result.sum();
    }

    auto BasicSteam::ideal_gamma_tau_tau() const -> Property
    {
        auto result = BasicSteam::kIdealN * BasicSteam::kIdealJ * (BasicSteam::kIdealJ - 1) *
            std::pow(this->tau_, BasicSteam::kIdealJ - 2);
        return result.sum();
    }

    auto BasicSteam::ideal_gamma_pi() const -> Property
    {
        return 1.0 / this->pi_;
    }

    auto BasicSteam::residual_gamma() const -> Property
    {
        auto result = BasicSteam::kResidualN * std::pow(this->pi_, BasicSteam::kResidualI) *
            std::pow(this->tau_diff_, BasicSteam::kResidualJ);
        return result.sum();
    }

    auto BasicSteam::residual_gamma_tau() const -> Property
    {
        auto result = BasicSteam::kResidualN * std::pow(this->pi_, BasicSteam::kResidualI) * BasicSteam::kResidualJ *
            std::pow(this->tau_diff_, BasicSteam::kResidualJ - 1.0);
        return result.sum();
    }

    auto BasicSteam::residual_gamma_tau_tau() const -> Property
    {
        auto result = BasicSteam::kResidualN * std::pow(this->pi_, BasicSteam::kResidualI) * BasicSteam::kResidualJ *
            (BasicSteam::kResidualJ - 1) * std::pow(this->tau_diff_, BasicSteam::kResidualJ - 2);
        return result.sum();
    }

    auto BasicSteam::residual_gamma_pi() const -> Property
    {
        auto result = BasicSteam::kResidualN * BasicSteam::kResidualI *
            std::pow(this->pi_, BasicSteam::kResidualI - 1.0) * std::pow(this->tau_diff_, BasicSteam::kResidualJ);
        return result.sum();
    }

    auto BasicSteam::residual_gamma_pi_pi() const -> Property
    {
        auto result = BasicSteam::kResidualN * BasicSteam::kResidualI * (BasicSteam::kResidualI - 1.0) *
            std::pow(this->pi_, BasicSteam::kResidualI - 2.0) * std::pow(this->tau_diff_, BasicSteam::kResidualJ);
        return result.sum();
    }

    auto BasicSteam::residual_gamma_pi_tau() const -> Property
    {
        auto result = BasicSteam::kResidualN * BasicSteam::kResidualI *
            std::pow(this->pi_, BasicSteam::kResidualI - 1.0) * BasicSteam::kResidualJ *
            std::pow(this->tau_diff_, BasicSteam::kResidualJ - 1.0);
        return result.sum();
    }

    const PressureT<MegaPascal> BasicSteam::kPStar{1.0};
    const TemperatureT<Kelvin> BasicSteam::kTStar{540.0};

    const Exponent BasicSteam::kIdealJ = {
        0.0,
        1.0,
        -5.0,
        -4.0,
        -3.0,
        -2.0,
        -1.0,
        2.0,
        3.0,
    };

    const Coefficient BasicSteam::kIdealN = {
        -0.96927686500217e1,
        0.10086655968018e2,
        -0.56087911283020e-2,
        0.71452738081455e-1,
        -0.40710498223928,
        0.14240819171444e1,
        -0.43839511319450e1,
        -0.28408632460772,
        0.21268463753307e-1,
    };

    const Exponent BasicSteam::kResidualI = {
        1.0,  1.0,  1.0,  1.0,  1.0,  2.0,  2.0,  2.0,  2.0,  2.0,  3.0,  3.0,  3.0,  3.0,  3.0,
        4.0,  4.0,  4.0,  5.0,  6.0,  6.0,  6.0,  7.0,  7.0,  7.0,  8.0,  8.0,  9.0,  10.0, 10.0,
        10.0, 16.0, 16.0, 18.0, 20.0, 20.0, 20.0, 21.0, 22.0, 23.0, 24.0, 24.0, 24.0,
    };

    const Exponent BasicSteam::kResidualJ = {
        0.0,  1.0,  2.0,  3.0,  6.0,  1.0,  2.0,  4.0,  7.0,  36.0, 0.0,  1.0,  3.0,  6.0, 35.0,
        1.0,  2.0,  3.0,  7.0,  3.0,  16.0, 35.0, 0.0,  11.0, 25.0, 8.0,  36.0, 13.0, 4.0, 10.0,
        14.0, 29.0, 50.0, 57.0, 20.0, 35.0, 48.0, 21.0, 53.0, 39.0, 26.0, 40.0, 58.0,
    };

    const Coefficient BasicSteam::kResidualN = {
        -0.17731742473213e-2, -0.17834862292358e-1,  -0.45996013696365e-1,  -0.57581259083432e-1, -0.50325278727930e-1,
        -0.33032641670203e-4, -0.18948987516315e-3,  -0.39392777243355e-2,  -0.43797295650573e-1, -0.26674547914087e-4,
        0.20481737692309e-7,  0.43870667284435e-6,   -0.32277677238570e-4,  -0.15033924542148e-2, -0.40668253562649e-1,
        -0.78847309559367e-9, 0.12790717852285e-7,   0.48225372718507e-6,   0.22922076337661e-5,  -0.16714766451061e-10,
        -0.21171472321355e-2, -0.23895741934104e2,   -0.59059564324270e-17, -0.12621808899101e-5, -0.38946842435739e-1,
        0.11256211360459e-10, -0.82311340897998e1,   0.19809712802088e-7,   0.10406965210174e-18, -0.10234747095929e-12,
        -0.10018179379511e-8, -0.80882908646985e-10, 0.10693031879409,      -0.33662250574171,    0.89185845355421e-24,
        0.30629316876232e-12, -0.42002467698208e-5,  -0.59056029685639e-25, 0.37826947613457e-5,  -0.12768608934681e-14,
        0.73087610595061e-28, 0.55414715350778e-16,  -0.94369707241210e-6,
    };

} // namespace fluid_props::water
