#include "water/iapwsif97/basic_equations/high_steam.hpp"
#include "water/iapwsif97/iapws_constants.hpp"

namespace fluid_props::water
{
    void BasicHighSteam::SetState(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature)
    {
        this->pressure_ = pressure;
        this->temperature_ = temperature;
        this->pi_ = this->pressure_ / BasicHighSteam::kPStar;
        this->tau_ = BasicHighSteam::kTStar / this->temperature_;
    }

    auto BasicHighSteam::Enthalpy() const -> SpecificEnergyT<KiloJoule_KiloGram>
    {
        return SpecificEnergyT<KiloJoule_KiloGram>{
            kSpecificGas * this->temperature_ * this->tau_ * (this->ideal_gamma_tau() + this->residual_gamma_tau())
        };
    }
    auto BasicHighSteam::Entropy() const -> SpecificEntropyT<KiloJoule_KiloGram_Kelvin>
    {
        return SpecificEntropyT<KiloJoule_KiloGram_Kelvin>{
            kSpecificGas *
            (this->tau_ * (this->ideal_gamma_tau() + this->residual_gamma_tau()) -
             (this->ideal_gamma() + this->residual_gamma()))
        };
    }

    auto BasicHighSteam::SpecificVolume() const -> SpecificVolumeT<CubicMeter_KiloGram>
    {
        /*
        The factor of 1000.0 is not documented anywhere, BUT it needs to be there so that the units of pressure
        are consistent with the units of kSpecificGas.
        */
        return SpecificVolumeT<CubicMeter_KiloGram>{
            kSpecificGas * this->temperature_ * this->pi_ / this->pressure_ *
            (this->ideal_gamma_pi() + this->residual_gamma_pi()) / 1000.0
        };
    }

    auto BasicHighSteam::InternalEnergy() const -> SpecificEnergyT<KiloJoule_KiloGram>
    {
        return this->Enthalpy() -
            kSpecificGas * this->temperature_ * this->pi_ * (this->ideal_gamma_pi() + this->residual_gamma_pi());
    }

    auto BasicHighSteam::IsobaricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin>
    {
        return HeatCapacityT<KiloJoule_KiloGram_Kelvin>{
            -1.0 * kSpecificGas * std::pow(this->tau_, 2.0) *
            (this->ideal_gamma_tau_tau() + this->residual_gamma_tau_tau())
        };
    }

    auto BasicHighSteam::IsochoricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin>
    {
        auto numerator =
            1.0 + this->pi_ * this->residual_gamma_pi() - this->tau_ * this->pi_ * this->residual_gamma_pi_tau();
        auto denominator = 1.0 - std::pow(this->pi_, 2.0) * this->residual_gamma_pi_pi();
        return this->IsobaricHeatCapacity() - kSpecificGas * std::pow(numerator, 2.0) / denominator;
    }

    auto BasicHighSteam::SpeedOfSound() const -> VelocityT<Meter_Second>
    {
        auto a =
            1.0 + 2.0 * this->pi_ * this->residual_gamma_pi() + std::pow(this->pi_ * this->residual_gamma_pi(), 2.0);
        auto b = 1.0 - std::pow(this->pi_, 2.0) * this->residual_gamma_pi_pi();
        auto c = std::pow(
            1.0 + this->pi_ * this->residual_gamma_pi() - this->tau_ * this->pi_ * this->residual_gamma_pi_tau(),
            2.0
        );
        auto d = std::pow(this->tau_, 2.0) * (this->ideal_gamma_tau_tau() + this->residual_gamma_tau_tau());
        auto e = c / d;
        auto result = a / (b + e);

        return VelocityT<Meter_Second>{std::sqrt(1000.0 * kSpecificGas * this->temperature_ * result)};
    }

    auto BasicHighSteam::CubicExpansion() const -> ThermalExpansionT<IKelvin>
    {
        auto numerator =
            1.0 + this->pi_ * this->residual_gamma_pi() - this->tau_ * this->pi_ * this->residual_gamma_pi_tau();
        auto denominator = 1.0 + this->pi_ * this->residual_gamma_pi();
        return ThermalExpansionT<IKelvin>{numerator / denominator / this->temperature_};
    }

    auto BasicHighSteam::Compressibility() const -> CompressibilityT<IMegaPascal>
    {
        auto numerator = 1.0 - this->pi_ * this->pi_ * this->residual_gamma_pi_pi();
        auto denominator = 1.0 + this->pi_ * this->residual_gamma_pi();
        return CompressibilityT<IMegaPascal>{numerator / denominator / this->pressure_};
    }

    auto BasicHighSteam::WithinValidRange() const -> bool
    {
        return this->temperature_ >= kMaxTemperature && this->temperature_ <= kHighMaxTemperature &&
            this->pressure_ <= KHighMaxPressure;
    }

    auto BasicHighSteam::ideal_gamma() const -> Property
    {
        auto result = BasicHighSteam::kIdealN * std::pow(this->tau_, BasicHighSteam::kIdealJ);
        return std::log(this->pi_) + result.sum();
    }

    auto BasicHighSteam::ideal_gamma_tau() const -> Property
    {
        auto result =
            BasicHighSteam::kIdealN * BasicHighSteam::kIdealJ * std::pow(this->tau_, BasicHighSteam::kIdealJ - 1.0);
        return result.sum();
    }

    auto BasicHighSteam::ideal_gamma_tau_tau() const -> Property
    {
        auto result = BasicHighSteam::kIdealN * BasicHighSteam::kIdealJ * (BasicHighSteam::kIdealJ - 1.0) *
            std::pow(this->tau_, BasicHighSteam::kIdealJ - 2.0);
        return result.sum();
    }

    auto BasicHighSteam::ideal_gamma_pi() const -> Property
    {
        return 1.0 / this->pi_;
    }

    auto BasicHighSteam::residual_gamma() const -> Property
    {
        auto result = BasicHighSteam::kResidualN * std::pow(this->pi_, BasicHighSteam::kResidualI) *
            std::pow(this->tau_, BasicHighSteam::kResidualJ);
        return result.sum();
    }

    auto BasicHighSteam::residual_gamma_tau() const -> Property
    {
        auto result = BasicHighSteam::kResidualN * std::pow(this->pi_, BasicHighSteam::kResidualI) *
            BasicHighSteam::kResidualJ * std::pow(this->tau_, BasicHighSteam::kResidualJ - 1.0);
        return result.sum();
    }

    auto BasicHighSteam::residual_gamma_tau_tau() const -> Property
    {
        auto result = BasicHighSteam::kResidualN * std::pow(this->pi_, BasicHighSteam::kResidualI) *
            BasicHighSteam::kResidualJ * (BasicHighSteam::kResidualJ - 1.0) *
            std::pow(this->tau_, BasicHighSteam::kResidualJ - 2.0);
        return result.sum();
    }

    auto BasicHighSteam::residual_gamma_pi() const -> Property
    {
        auto result = BasicHighSteam::kResidualN * BasicHighSteam::kResidualI *
            std::pow(this->pi_, BasicHighSteam::kResidualI - 1.0) * std::pow(this->tau_, BasicHighSteam::kResidualJ);
        return result.sum();
    }

    auto BasicHighSteam::residual_gamma_pi_pi() const -> Property
    {
        auto result = BasicHighSteam::kResidualN * BasicHighSteam::kResidualI * (BasicHighSteam::kResidualI - 1.0) *
            std::pow(this->pi_, BasicHighSteam::kResidualI - 2.0) * std::pow(this->tau_, BasicHighSteam::kResidualJ);
        return result.sum();
    }

    auto BasicHighSteam::residual_gamma_pi_tau() const -> Property
    {
        auto result = BasicHighSteam::kResidualN * BasicHighSteam::kResidualI *
            std::pow(this->pi_, BasicHighSteam::kResidualI - 1.0) * BasicHighSteam::kResidualJ *
            std::pow(this->tau_, BasicHighSteam::kResidualJ - 1.0);
        return result.sum();
    }

    const PressureT<MegaPascal> BasicHighSteam::kPStar{1.0};
    const TemperatureT<Kelvin> BasicHighSteam::kTStar{1000.0};

    const Exponent BasicHighSteam::kIdealJ = {
        0.0,
        1.0,
        -3.0,
        -2.0,
        -1.0,
        2.0,
    };

    const Exponent BasicHighSteam::kResidualI = {
        1.0,
        1.0,
        1.0,
        2.0,
        2.0,
        3.0,
    };

    const Exponent BasicHighSteam::kResidualJ = {
        1.0,
        2.0,
        3.0,
        3.0,
        9.0,
        7.0,
    };

    const Coefficient BasicHighSteam::kIdealN = {
        -0.13179983674201e2,
        0.68540841634434e1,
        -0.24805148933466e-1,
        0.36901534980333,
        -0.31161318213925e1,
        -0.32961626538917,
    };

    const Coefficient BasicHighSteam::kResidualN = {
        0.15736404855259e-2,
        0.90153761673944e-3,
        -0.50270077677648e-2,
        0.22440037409485e-5,
        -0.41163275453471e-5,
        0.37919454822955e-7,
    };
} // namespace fluid_props::water
