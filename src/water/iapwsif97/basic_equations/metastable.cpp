#include "water/iapwsif97/basic_equations/metastable.hpp"
#include "water/iapwsif97/iapws_constants.hpp"

namespace fluid_props::water
{
    void MetaStable::SetState(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature)
    {
        this->pressure_ = pressure;
        this->temperature_ = temperature;

        this->pi_ = this->pressure_ / MetaStable::kPStar;
        this->tau_ = MetaStable::kTStar / this->temperature_;
        this->tau_diff_ = this->tau_ - 0.5;
    }

    auto MetaStable::Enthalpy() const -> SpecificEnergyT<KiloJoule_KiloGram>
    {
        return SpecificEnergyT<KiloJoule_KiloGram>{
            this->temperature_ * kSpecificGas * this->tau_ * (this->ideal_gamma_tau() + this->residual_gamma_tau())
        };
    }

    auto MetaStable::Entropy() const -> SpecificEntropyT<KiloJoule_KiloGram_Kelvin>
    {
        auto result = this->tau_ * (ideal_gamma_tau() + this->residual_gamma_tau()) -
            (this->ideal_gamma() + this->residual_gamma());
        return SpecificEntropyT<KiloJoule_KiloGram_Kelvin>{kSpecificGas * result};
    }

    auto MetaStable::InternalEnergy() const -> SpecificEnergyT<KiloJoule_KiloGram>
    {
        return this->Enthalpy() -
            kSpecificGas * this->temperature_ * this->pi_ * (this->ideal_gamma_pi() + this->residual_gamma_pi());
    }

    auto MetaStable::SpecificVolume() const -> SpecificVolumeT<CubicMeter_KiloGram>
    {
        /*
        The factor of 1000.0 is not documented anywhere, BUT it needs to be there so that the units of pressure
        are consistent with the units of kSpecificGas.
        */
        return SpecificVolumeT<CubicMeter_KiloGram>{
            kSpecificGas * this->temperature_ / this->pressure_ * this->pi_ *
            (this->ideal_gamma_pi() + this->residual_gamma_pi()) / 1000.0
        };
    }

    auto MetaStable::IsobaricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin>
    {
        return HeatCapacityT<KiloJoule_KiloGram_Kelvin>{
            -1.0 * kSpecificGas * this->tau_ * this->tau_ *
            (this->ideal_gamma_tau_tau() + this->residual_gamma_tau_tau())
        };
    }

    auto MetaStable::IsochoricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin>
    {
        auto numerator =
            1.0 + this->pi_ * this->residual_gamma_pi() - this->tau_ * this->pi_ * this->residual_gamma_pi_tau();
        auto denominator = 1.0 - this->pi_ * this->pi_ * this->residual_gamma_pi_pi();
        return this->IsobaricHeatCapacity() - kSpecificGas * numerator * numerator / denominator;
    }

    auto MetaStable::SpeedOfSound() const -> VelocityT<Meter_Second>
    {
        auto a = 1.0 + 2.0 * this->pi_ * this->residual_gamma_pi() +
            this->pi_ * this->pi_ * std::pow(this->residual_gamma_pi(), 2.0);
        auto b = 1.0 - this->pi_ * this->pi_ * this->residual_gamma_pi_pi();
        auto c = 1.0 + this->pi_ * this->residual_gamma_pi() - this->tau_ * this->pi_ * this->residual_gamma_pi_tau();
        auto d = this->tau_ * this->tau_ * (this->ideal_gamma_tau_tau() + this->residual_gamma_tau_tau());
        auto e = c * c / d;
        auto f = a / (b + e);
        /*
        The factor of 1000.0 is not documented anywhere, BUT it needs to be there so that the units of pressure
        are consistent with the units of kSpecificGas.
        */
        return VelocityT<Meter_Second>{std::sqrt(1000.0 * kSpecificGas * this->temperature_ * f)};
    }

    auto MetaStable::CubicExpansion() const -> ThermalExpansionT<IKelvin>
    {
        auto numerator =
            1.0 + this->pi_ * this->residual_gamma_pi() - this->tau_ * this->pi_ * this->residual_gamma_pi_tau();
        auto denominator = 1.0 + this->pi_ * this->residual_gamma_pi();
        return ThermalExpansionT<IKelvin>{numerator / denominator / this->temperature_};
    }

    auto MetaStable::Compressibility() const -> CompressibilityT<IMegaPascal>
    {
        auto numerator = 1.0 - this->pi_ * this->pi_ * this->residual_gamma_pi_pi();
        auto denominator = 1.0 + this->pi_ * this->residual_gamma_pi();
        return CompressibilityT<IMegaPascal>{numerator / denominator / this->pressure_};
    }

    auto MetaStable::ideal_gamma() const -> Property
    {
        auto result = MetaStable::kIdealN * std::pow(this->tau_, MetaStable::kIdealJ);
        return std::log(this->pi_) + result.sum();
    }

    auto MetaStable::ideal_gamma_tau() const -> Property
    {
        auto result = MetaStable::kIdealN * MetaStable::kIdealJ * std::pow(this->tau_, MetaStable::kIdealJ - 1.0);
        return result.sum();
    }

    auto MetaStable::ideal_gamma_tau_tau() const -> Property
    {
        auto result = MetaStable::kIdealN * MetaStable::kIdealJ * (MetaStable::kIdealJ - 1) *
            std::pow(this->tau_, MetaStable::kIdealJ - 2);
        return result.sum();
    }

    auto MetaStable::ideal_gamma_pi() const -> Property
    {
        return 1.0 / this->pi_;
    }

    auto MetaStable::residual_gamma() const -> Property
    {
        auto result = MetaStable::kResidualN * std::pow(this->pi_, MetaStable::kResidualI) *
            std::pow(this->tau_diff_, MetaStable::kResidualJ);
        return result.sum();
    }

    auto MetaStable::residual_gamma_tau() const -> Property
    {
        auto result = MetaStable::kResidualN * std::pow(this->pi_, MetaStable::kResidualI) * MetaStable::kResidualJ *
            std::pow(this->tau_diff_, MetaStable::kResidualJ - 1.0);
        return result.sum();
    }

    auto MetaStable::residual_gamma_tau_tau() const -> Property
    {
        auto result = MetaStable::kResidualN * std::pow(this->pi_, MetaStable::kResidualI) * MetaStable::kResidualJ *
            (MetaStable::kResidualJ - 1) * std::pow(this->tau_diff_, MetaStable::kResidualJ - 2);
        return result.sum();
    }

    auto MetaStable::residual_gamma_pi() const -> Property
    {
        auto result = MetaStable::kResidualN * MetaStable::kResidualI *
            std::pow(this->pi_, MetaStable::kResidualI - 1.0) * std::pow(this->tau_diff_, MetaStable::kResidualJ);
        return result.sum();
    }

    auto MetaStable::residual_gamma_pi_pi() const -> Property
    {
        auto result = MetaStable::kResidualN * MetaStable::kResidualI * (MetaStable::kResidualI - 1.0) *
            std::pow(this->pi_, MetaStable::kResidualI - 2.0) * std::pow(this->tau_diff_, MetaStable::kResidualJ);
        return result.sum();
    }

    auto MetaStable::residual_gamma_pi_tau() const -> Property
    {
        auto result = MetaStable::kResidualN * MetaStable::kResidualI *
            std::pow(this->pi_, MetaStable::kResidualI - 1.0) * MetaStable::kResidualJ *
            std::pow(this->tau_diff_, MetaStable::kResidualJ - 1.0);
        return result.sum();
    }

    const Normalization MetaStable::kPStar = 1.0;
    const Normalization MetaStable::kTStar = 540.0;

    const Exponent MetaStable::kIdealJ = {
        0.0,
        1.0,
        -5.0,
        -4.0,
        -3.0,
        -2.0,
        -1.0,
        2.0,
        3.0,
    };

    const Coefficient MetaStable::kIdealN = {
        -0.96937268393049e1,
        0.10087275970006e2,
        -0.56087911283020e-2,
        0.71452738081455e-1,
        -0.40710498223928,
        0.14240819171444e1,
        -0.43839511319450e1,
        -0.28408632460772,
        0.21268463753307e-1,
    };

    const Exponent MetaStable::kResidualI = {
        1.0,
        1.0,
        1.0,
        1.0,
        2.0,
        2.0,
        2.0,
        3.0,
        3.0,
        4.0,
        4.0,
        5.0,
        5.0,
    };

    const Exponent MetaStable::kResidualJ = {
        0.0,
        2.0,
        5.0,
        11.0,
        1.0,
        7.0,
        16.0,
        4.0,
        16.0,
        7.0,
        10.0,
        9.0,
        10.0,
    };

    const Coefficient MetaStable::kResidualN = {
        -0.73362260186506e-2,
        -0.88223831943146e-1,
        -0.72334555213245e-1,
        -0.40813178534455e-2,
        0.20097803380207e-2,
        -0.53045921898642e-1,
        -0.76190409086970e-2,
        -0.63498037657313e-2,
        -0.86043093028588e-1,
        0.75321581522770e-2,
        -0.79238375446139e-2,
        -0.22888160778447e-3,
        -0.26456501482810e-2,
    };
} // namespace fluid_props::water
