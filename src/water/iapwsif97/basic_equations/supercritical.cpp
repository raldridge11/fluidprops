#include "water/iapwsif97/basic_equations/supercritical.hpp"
#include "exceptions/exceptions.hpp"
#include "math_methods/math_methods.hpp"
#include "water/iapwsif97/curves/supercritical.hpp"

namespace fluid_props::water
{
    void BasicSuperCritical::SetState(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature)
    {
        this->density_.Reset();
        this->temperature_ = temperature;
        this->pressure_ = pressure;

        this->tau_ = kCriticalTemperature / this->temperature_;

        this->density_.SetMethod([this]() {
            return fluid_props::math_methods::SecantMethod(
                [this](double density) {
                    this->delta_ = density / kCriticalDensity;
                    return density * this->delta_.Value() * kSpecificGas * this->temperature_ * this->phi_delta() /
                        1000.0 -
                        this->pressure_.Value();
                },
                kCriticalDensity.Value(),
                kCriticalDensity.Value() + 100.0,
                fluid_props::math_methods::kDefaultTolerance,
                10000000
            );
        });
    }

    auto BasicSuperCritical::Enthalpy() const -> SpecificEnergyT<KiloJoule_KiloGram>
    {
        return SpecificEnergyT<KiloJoule_KiloGram>{
            kSpecificGas * this->temperature_ *
            (this->tau_ * this->phi_tau() + this->delta_.Value() * this->phi_delta())
        };
    }

    auto BasicSuperCritical::Entropy() const -> SpecificEntropyT<KiloJoule_KiloGram_Kelvin>
    {
        return SpecificEntropyT<KiloJoule_KiloGram_Kelvin>{kSpecificGas * (this->tau_ * this->phi_tau() - this->phi())};
    }

    auto BasicSuperCritical::InternalEnergy() const -> SpecificEnergyT<KiloJoule_KiloGram>
    {
        return SpecificEnergyT<KiloJoule_KiloGram>{kSpecificGas * this->temperature_ * this->tau_ * this->phi_tau()};
    }

    auto BasicSuperCritical::SpecificVolume() const -> SpecificVolumeT<CubicMeter_KiloGram>
    {
        auto result = this->density_.Value() != 0.0 ? 1.0 / this->density_.Value() : 0.0;
        return SpecificVolumeT<CubicMeter_KiloGram>{result};
    }

    auto BasicSuperCritical::IsobaricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin>
    {
        auto numerator = std::pow(
            this->delta_.Value() * this->phi_delta() - this->delta_.Value() * this->tau_ * this->phi_delta_tau(),
            2.0
        );
        auto denominator = 2.0 * this->delta_.Value() * this->phi_delta() +
            std::pow(this->delta_.Value(), 2.0) * this->phi_delta_delta();
        return this->IsochoricHeatCapacity() + kSpecificGas * numerator / denominator;
    }

    auto BasicSuperCritical::IsochoricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin>
    {
        return HeatCapacityT<KiloJoule_KiloGram_Kelvin>{
            -1.0 * kSpecificGas * std::pow(this->tau_, 2) * this->phi_tau_tau()
        };
    }

    auto BasicSuperCritical::SpeedOfSound() const -> VelocityT<Meter_Second>
    {
        auto numerator = std::pow(
            this->delta_.Value() * this->phi_delta() - this->delta_.Value() * this->tau_ * this->phi_delta_tau(),
            2.0
        );
        auto denominator = this->tau_ * this->tau_ * this->phi_tau_tau();
        auto speed_squared = 2.0 * this->delta_.Value() * this->phi_delta() +
            this->delta_.Value() * this->delta_.Value() * this->phi_delta_delta() - numerator / denominator;

        // factor of 1000.0 for correct conversion
        return VelocityT<Meter_Second>{std::sqrt(1000.0 * kSpecificGas * this->temperature_ * speed_squared)};
    }

    auto BasicSuperCritical::CubicExpansion() const -> ThermalExpansionT<IKelvin>
    {
        auto numerator = this->phi_delta() - this->tau_ * this->phi_delta_tau();
        auto denominator = 2.0 * this->phi_delta() + this->delta_.Value() * this->phi_delta_delta();
        return ThermalExpansionT<IKelvin>{numerator / denominator / this->temperature_};
    }

    auto BasicSuperCritical::Compressibility() const -> CompressibilityT<IMegaPascal>
    {
        auto result = this->density_.Value() * kSpecificGas * this->temperature_ *
            (2.0 * this->delta_.Value() * this->phi_delta() +
             this->delta_.Value() * this->delta_.Value() * this->phi_delta_delta());
        return CompressibilityT<IMegaPascal>{1000.0 / result};
    }

    auto BasicSuperCritical::WithinValidRange() const -> bool
    {
        auto validTemperature =
            this->temperature_ >= kSubcooledMaxTemperature && this->temperature_ <= kMaxTemperatureSuperCritical;

        auto validPressure = false;
        try
        {
            validPressure =
                this->pressure_ <= kMaxPressure && this->pressure_ >= SuperCriticalCurve(this->temperature_).Pressure();
        }
        catch (const PropertyOutOfBounds &error)
        {
            return false;
        }

        return validTemperature && validPressure;
    }

    auto BasicSuperCritical::phi() const -> Property
    {
        auto result = BasicSuperCritical::kN * std::pow(this->delta_.Value(), BasicSuperCritical::kI) *
            std::pow(this->tau_, BasicSuperCritical::kJ);
        return BasicSuperCritical::kN1[0] * std::log(this->delta_.Value()) + result.sum();
    }

    auto BasicSuperCritical::phi_delta() const -> Property
    {
        auto result = BasicSuperCritical::kN * BasicSuperCritical::kI *
            std::pow(this->delta_.Value(), BasicSuperCritical::kI - 1.0) * std::pow(this->tau_, BasicSuperCritical::kJ);
        return BasicSuperCritical::kN1[0] / this->delta_.Value() + result.sum();
    }

    auto BasicSuperCritical::phi_delta_delta() const -> Property
    {
        auto result = BasicSuperCritical::kN * BasicSuperCritical::kI * (BasicSuperCritical::kI - 1.0) *
            std::pow(this->delta_.Value(), BasicSuperCritical::kI - 2.0) * std::pow(this->tau_, BasicSuperCritical::kJ);
        return -1.0 * BasicSuperCritical::kN1[0] * std::pow(this->delta_.Value(), -2.0) + result.sum();
    }

    auto BasicSuperCritical::phi_tau() const -> Property
    {
        auto result = BasicSuperCritical::kN * std::pow(this->delta_.Value(), BasicSuperCritical::kI) *
            BasicSuperCritical::kJ * std::pow(this->tau_, BasicSuperCritical::kJ - 1.0);
        return result.sum();
    }

    auto BasicSuperCritical::phi_tau_tau() const -> Property
    {
        auto result = BasicSuperCritical::kN * std::pow(this->delta_.Value(), BasicSuperCritical::kI) *
            BasicSuperCritical::kJ * (BasicSuperCritical::kJ - 1.0) *
            std::pow(this->tau_, BasicSuperCritical::kJ - 2.0);
        return result.sum();
    }

    auto BasicSuperCritical::phi_delta_tau() const -> Property
    {
        auto result = BasicSuperCritical::kN * BasicSuperCritical::kI *
            std::pow(this->delta_.Value(), BasicSuperCritical::kI - 1.0) * BasicSuperCritical::kJ *
            std::pow(this->tau_, BasicSuperCritical::kJ - 1.0);
        return result.sum();
    }

    const Coefficient BasicSuperCritical::kN1 = {0.10658070028513e1};

    const Coefficient BasicSuperCritical::kN = {
        -0.15732845290239e2,  0.20944396974307e2,   -0.76867707878716e1,  0.26185947787954e1,   -0.28080781148620e1,
        0.12053369696517e1,   -0.84566812812502e-2, -0.12654315477714e1,  -0.11524407806681e1,  0.88521043984318,
        -0.64207765181607,    0.38493460186671,     -0.85214708824206,    0.48972281541877e1,   -0.30502617256965e1,
        0.39420536879154e-1,  0.12558408424308,     -0.27999329698710,    0.13899799569460e1,   -0.20189915023570e1,
        -0.82147637173963e-2, -0.47596035734923,    0.43984074473500e-1,  -0.44476435428739,    0.90572070719733,
        0.70522450087967,     0.10770512626332,     -0.32913623258954,    -0.50871062041158,    -0.22175400873096e-1,
        0.94260751665092e-1,  0.16436278447961,     -0.13503372241348e-1, -0.14834345352472e-1, 0.57922953628084e-3,
        0.32308904703711e-2,  0.80964802996215e-4,  -0.16557679795037e-3, -0.44923899061815e-4,
    };

    const Exponent BasicSuperCritical::kI = {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0,  3.0,  3.0,  3.0,
        3.0, 3.0, 4.0, 4.0, 4.0, 4.0, 5.0, 5.0, 5.0, 6.0, 6.0, 6.0, 7.0, 8.0, 9.0, 9.0, 10.0, 10.0, 11.0,
    };

    const Exponent BasicSuperCritical::kJ = {
        0.0,  1.0,  2.0, 7.0, 10.0, 12.0, 23.0, 2.0, 6.0,  15.0, 17.0, 0.0,  2.0, 6.0,  7.0, 22.0, 26.0, 0.0, 2.0,  4.0,
        16.0, 26.0, 0.0, 2.0, 4.0,  26.0, 1.0,  3.0, 26.0, 0.0,  2.0,  26.0, 2.0, 26.0, 2.0, 26.0, 0.0,  1.0, 26.0,
    };

} // namespace fluid_props::water
