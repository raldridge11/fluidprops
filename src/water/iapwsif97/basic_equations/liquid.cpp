#include "water/iapwsif97/basic_equations/liquid.hpp"
#include "water/iapwsif97/iapws_constants.hpp"

namespace fluid_props::water
{
    void BasicLiquid::SetState(PressureT<MegaPascal> pressure, TemperatureT<Kelvin> temperature)
    {
        this->pressure_ = pressure;
        this->pi_ = this->pressure_ / BasicLiquid::kPStar;
        this->pi_diff_ = 7.1 - this->pi_;

        this->temperature_ = temperature;
        this->tau_ = BasicLiquid::kTStar / this->temperature_;
        this->tau_diff_ = this->tau_ - 1.222;
        this->saturation_.SetState(temperature);
    }

    auto BasicLiquid::Enthalpy() const -> SpecificEnergyT<KiloJoule_KiloGram>
    {
        return SpecificEnergyT<KiloJoule_KiloGram>{kSpecificGas * this->temperature_ * this->tau_ * this->gamma_tau()};
    }

    auto BasicLiquid::Entropy() const -> SpecificEntropyT<KiloJoule_KiloGram_Kelvin>
    {
        return SpecificEntropyT<KiloJoule_KiloGram_Kelvin>{
            kSpecificGas * (this->tau_ * this->gamma_tau() - this->gamma())
        };
    }

    auto BasicLiquid::InternalEnergy() const -> SpecificEnergyT<KiloJoule_KiloGram>
    {
        return SpecificEnergyT<KiloJoule_KiloGram>{
            kSpecificGas * this->temperature_ * (this->tau_ * this->gamma_tau() - this->pi_ * this->gamma_pi())
        };
    }

    auto BasicLiquid::SpecificVolume() const -> SpecificVolumeT<CubicMeter_KiloGram>
    {
        /*
        The factor of 1000.0 is not documented anywhere, BUT it needs to be there so that the units of pressure
        are consistent with the units of kSpecificGas.
        */
        return SpecificVolumeT<CubicMeter_KiloGram>{
            kSpecificGas * this->temperature_ * this->pi_ * this->gamma_pi() / (1000.0 * this->pressure_)
        };
    }

    auto BasicLiquid::IsobaricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin>
    {
        return HeatCapacityT<KiloJoule_KiloGram_Kelvin>{
            -1.0 * kSpecificGas * std::pow(this->tau_, 2) * this->gamma_tau_tau()
        };
    }

    auto BasicLiquid::IsochoricHeatCapacity() const -> HeatCapacityT<KiloJoule_KiloGram_Kelvin>
    {
        return this->IsobaricHeatCapacity() +
            kSpecificGas * std::pow(this->gamma_pi() - this->tau_ * this->gamma_pi_tau(), 2) / this->gamma_pi_pi();
    }

    auto BasicLiquid::SpeedOfSound() const -> VelocityT<Meter_Second>
    {
        auto a = 1000.0 * kSpecificGas * this->temperature_ * std::pow(this->gamma_pi(), 2);
        auto b = std::pow(this->gamma_pi() - this->tau_ * this->gamma_pi_tau(), 2);
        auto c = std::pow(this->tau_, 2) * this->gamma_tau_tau();
        auto d = b / c - this->gamma_pi_pi();
        return VelocityT<Meter_Second>{std::sqrt(a / d)};
    }

    auto BasicLiquid::CubicExpansion() const -> ThermalExpansionT<IKelvin>
    {
        auto fraction = this->tau_ * this->gamma_pi_tau() / this->gamma_pi();
        return ThermalExpansionT<IKelvin>{(1.0 - fraction) / this->temperature_};
    }

    auto BasicLiquid::Compressibility() const -> CompressibilityT<IMegaPascal>
    {
        return CompressibilityT<IMegaPascal>{
            (-1.0 / this->pressure_) * this->pi_ * this->gamma_pi_pi() / this->gamma_pi()
        };
    }

    auto BasicLiquid::WithinValidRange() const -> bool
    {
        if (!(this->temperature_ >= kTriplePointTemperature && this->temperature_ <= kSubcooledMaxTemperature))
        {
            return false;
        }

        if (!(this->pressure_ <= kMaxPressure && this->pressure_ >= this->saturation_.Pressure()))
        {
            return false;
        }

        return true;
    }

    auto BasicLiquid::gamma() const -> Property
    {
        auto result =
            BasicLiquid::kN * std::pow(this->pi_diff_, BasicLiquid::kI) * std::pow(this->tau_diff_, BasicLiquid::kJ);
        return result.sum();
    }

    auto BasicLiquid::gamma_tau() const -> Property
    {
        auto result = BasicLiquid::kN * BasicLiquid::kJ * std::pow(this->pi_diff_, BasicLiquid::kI) *
            std::pow(this->tau_diff_, BasicLiquid::kJ - 1);
        return result.sum();
    }

    auto BasicLiquid::gamma_pi() const -> Property
    {
        auto result = -1.0 * BasicLiquid::kN * BasicLiquid::kI * std::pow(this->pi_diff_, BasicLiquid::kI - 1) *
            std::pow(this->tau_diff_, BasicLiquid::kJ);
        return result.sum();
    }

    auto BasicLiquid::gamma_tau_tau() const -> Property
    {
        auto result = BasicLiquid::kN * std::pow(this->pi_diff_, BasicLiquid::kI) * BasicLiquid::kJ *
            (BasicLiquid::kJ - 1.0) * std::pow(this->tau_diff_, BasicLiquid::kJ - 2);
        return result.sum();
    }

    auto BasicLiquid::gamma_pi_pi() const -> Property
    {
        auto result = BasicLiquid::kN * BasicLiquid::kI * (BasicLiquid::kI - 1) *
            std::pow(this->pi_diff_, BasicLiquid::kI - 2) * std::pow(this->tau_diff_, BasicLiquid::kJ);
        return result.sum();
    }

    auto BasicLiquid::gamma_pi_tau() const -> Property
    {
        auto result = -1.0 * BasicLiquid::kN * BasicLiquid::kI * std::pow(this->pi_diff_, BasicLiquid::kI - 1) *
            BasicLiquid::kJ * std::pow(this->tau_diff_, BasicLiquid::kJ - 1);
        return result.sum();
    }

    const Normalization BasicLiquid::kPStar = 16.53;
    const Normalization BasicLiquid::kTStar = 1386.0;

    const Coefficient BasicLiquid::kN = {
        0.14632971213167,      -0.84548187169114,      -0.37563603672040e1,   0.33855169168385e1,
        -0.95791963387872,     0.15772038513228,       -0.16616417199501e-1,  0.81214629983568e-3,
        0.28319080123804e-3,   -0.6070630156587410e-3, -0.18990068218419e-1,  -0.32529748770505e-1,
        -0.21841717175414e-1,  -0.52838357969930e-4,   -0.47184321073267e-3,  -0.30001780793026e-3,
        0.47661393906987e-4,   -0.44141845330846e-5,   -0.72694996297594e-15, -0.31679644845054e-4,
        -0.28270797985312e-5,  -0.85205128120103e-9,   -0.22425281908000e-5,  -0.65171222895601e-6,
        -0.14341729937924e-12, -0.40516996860117e-6,   -0.12734301741641e-8,  -0.17424871230634e-9,
        -0.68762131295531e-18, 0.14478307828521e-19,   0.26335781662795e-22,  -0.11947622640071e-22,
        0.18228094581404e-23,  -0.93537087292458e-25,
    };

    const Exponent BasicLiquid::kI = {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0,  1.0,  1.0,  2.0,  2.0, 2.0,
        2.0, 2.0, 3.0, 3.0, 3.0, 4.0, 4.0, 4.0, 5.0, 8.0, 8.0, 21.0, 23.0, 29.0, 30.0, 31,  32,
    };

    const Exponent BasicLiquid::kJ = {
        -2.0, -1.0, 0.0,  1.0, 2.0, 3.0,  4.0,  5.0,  -9.0, -7.0,  -1.0, 0.0,   1.0,   3.0,   -3.0,  0.0,   1.0,
        3.0,  17.0, -4.0, 0.0, 6.0, -5.0, -2.0, 10.0, -8.0, -11.0, -6.0, -29.0, -31.0, -38.0, -39.0, -40.0, -41.0,
    };

} // namespace fluid_props::water
