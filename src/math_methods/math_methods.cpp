#include "math_methods/math_methods.hpp"
#include <cmath>

namespace fluid_props::math_methods
{
    auto SecantMethod(
        const std::function<double(double)> &func,
        double x_0,
        double x_1,
        const double tolerance,
        const int max_iteration
    ) -> double
    {
        auto error = x_1 - x_0;
        auto iteration = 0;

        while (std::abs(error) > tolerance && iteration++ < max_iteration)
        {
            auto tmp = x_1;
            auto y_0 = func(x_0);
            auto y_1 = func(x_1);
            x_1 = x_1 - y_1 * (x_1 - x_0) / (y_1 - y_0);
            x_0 = tmp;
            error = x_1 - x_0;
        }
        return x_1;
    }

    auto BisectionMethod(
        const std::function<double(double)> &func,
        double x_0,
        double x_1,
        const double tolerance,
        const int max_iteration
    ) -> double
    {
        auto midpoint = (x_1 + x_0) / 2.0;
        auto iteration = 0;
        while ((x_1 - x_0) / 2.0 > tolerance && iteration++ < max_iteration)
        {
            if (func(x_0) * func(midpoint) < 0)
            {
                x_1 = midpoint;
            }
            else
            {
                x_0 = midpoint;
            }

            midpoint = (x_1 + x_0) / 2.0;
        }
        return midpoint;
    }
} // namespace fluid_props::math_methods
