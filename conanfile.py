"""
Conan file for FluidProps++
"""
import os
import pathlib
import shutil

import conan
from conan.tools.cmake import CMake, CMakeDeps, CMakeToolchain
from conan.tools.files import copy


class FluidProps(conan.ConanFile):
    """
    Conan recipe for FluidProps++
    """
    name = "fluidprops++"
    description = "Fluid properties implemented in C++."
    license = "MIT"
    url = "https://gitlab.com/aldridgesoftwaredesigns/fluidprops"
    homepage = "https://gitlab.com/aldridgesoftwaredesigns/fluidprops"
    topics = ("fluids", "cfd", "fluid dynamics")
    package_type = "library"
    settings = "os", "arch", "compiler", "build_type"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"fPIC": True, "shared": True}
    cross_building = os.environ.get("PLATFORM") == "win64"

    def requirements(self):
        """
        Setup of project dependencies.
        """
        self.requires("spdlog/1.15.0")
        self.requires("gtest/1.13.0", test=True)
        self.requires("benchmark/1.8.2", test=True)

    def generate(self):
        """
        Generates cmake code for dependencies
        """
        tool_chain = CMakeToolchain(self, generator="Ninja")
        tool_chain.preprocessor_definitions["SPDLOG_FMT_EXTERNAL"] = 1
        tool_chain.generate()
        cmake_deps = CMakeDeps(self)
        cmake_deps.generate()

    def build(self):
        """
        Builds FluidProps
        """
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        self.package()

    def package(self):
        """
        Packages the build artifacts into the distributable.
        """
        self._copy_headers()
        self._copy_mingw_libs()

    def _copy_headers(self):
        """
        Copy header files of project.
        """
        shutil.copytree(pathlib.Path("..") / "include",
                        pathlib.Path("include"),
                        copy_function=self._verbose_copy,
                        dirs_exist_ok=True)

    def _copy_mingw_libs(self):
        """
        If we're cross building then copy the relevant mingw libs to bin directory
        """
        if not self.cross_building:
            return

        build_env = self.buildenv.vars(self)
        dlls = ("libstdc++-6", "libgcc_s_seh-1")
        dll_directory = pathlib.Path(build_env["DLL_DIR"])
        bin_folder = pathlib.Path(self.build_folder) / "bin"

        for dll in dlls:
            dll = f"{dll}.dll"
            self.output.success(f"Copying: {dll} to {bin_folder}")
            copy(self, dll, dll_directory, bin_folder)
        self.output.success(dll_directory)

        pthreads = pathlib.Path(
            build_env["CONAN_CMAKE_SYSROOT"]) / "lib" / "libwinpthread-1.dll"
        self.output.success(f"Copying: {pthreads.name}")
        copy(self, pthreads.name, pthreads.parent, bin_folder)

    def _verbose_copy(self, source: str, destination: str):
        self.output.success(f"Copying {source} to {destination}")
        shutil.copy2(source, destination)
