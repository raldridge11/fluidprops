#include <cmath>
#include <functional>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <math_methods/math_methods.hpp>

using ::testing::TestWithParam;
using ::testing::Values;

namespace fluid_props::testing
{
    using Expected = double;
    using FunctionTest = std::tuple<std::function<double(double)>, Expected>;

    class WithFunction : public TestWithParam<FunctionTest>
    {
    };

    using SecantMethod = WithFunction;

    TEST_P(SecantMethod, RootIsCorrectlyCalculated)
    {
        auto [func, expected] = this->GetParam();
        auto result = fluid_props::math_methods::SecantMethod(func, 1.0, 2.0);
        EXPECT_NEAR(result, expected, fluid_props::math_methods::kDefaultTolerance);
    }

    /**
     * @brief Test suite for provided functions and expected values from
     * "Numerical Analysis" by Timothy Sauer (2006)
     * Example 1.16 and Exercise 1.5.1
     */
    INSTANTIATE_TEST_SUITE_P(
        FunctionTest,
        SecantMethod,
        Values(
            std::make_tuple([](double x) { return std::pow(x, 3) + x - 1; }, 0.6823278),
            std::make_tuple([](double x) { return std::pow(x, 3) - 2 * x - 2; }, 1.76929235),
            std::make_tuple([](double x) { return std::exp(x) + x - 7; }, 1.6728217),
            std::make_tuple([](double x) { return std::exp(x) + std::sin(x) - 4; }, 1.12998050)
        )
    );

    using BisectionMethod = WithFunction;

    TEST_P(BisectionMethod, RootIsCorrectlyCalculated)
    {
        auto [func, expected] = this->GetParam();
        auto result = fluid_props::math_methods::BisectionMethod(func, -10.0, 10.0);
        EXPECT_NEAR(result, expected, fluid_props::math_methods::kDefaultTolerance);
    }

    /**
     * @brief Test suite for provided functions and expected values from
     * "Numerical Analysis" by Timothy Sauer (2006)
     * Exercise 1.1.1
     */
    INSTANTIATE_TEST_SUITE_P(
        FunctionTest,
        BisectionMethod,
        Values(
            std::make_tuple([](double x) { return std::pow(x, 3) - 9; }, 2.0800838),
            std::make_tuple([](double x) { return 3.0 * std::pow(x, 3) + std::pow(x, 2) - x - 5; }, 1.16972621),
            std::make_tuple([](double x) { return std::pow(std::cos(x), 2) - x + 6; }, 6.7760923)
        )
    );

} // namespace fluid_props::testing
