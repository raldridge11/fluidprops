#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <lazy_evaluator/lazy_evaluator.hpp>
using ::testing::Eq;
using ::testing::Test;

namespace fluid_props::testing
{
    class ALazyEvaluator : public Test
    {
    public:
        LazyEvaluator<int> evaluator = LazyEvaluator<int>();
    };

    TEST_F(ALazyEvaluator, IfValueIsTruthyIsSetIsTrue)
    {
        this->evaluator = 1;
        ASSERT_THAT(this->evaluator.Value(), Eq(1));
        ASSERT_TRUE(this->evaluator.IsSet());
    }

    TEST_F(ALazyEvaluator, IfValueIsFalseyIsSetIsFalse)
    {
        this->evaluator = 0;
        ASSERT_THAT(this->evaluator.Value(), Eq(0));
        ASSERT_FALSE(this->evaluator.IsSet());
    }

    TEST_F(ALazyEvaluator, ResetSetsValueToDefault)
    {
        this->evaluator = 1;
        this->evaluator.Reset();
        ASSERT_THAT(this->evaluator.Value(), Eq(0));
    }

    TEST_F(ALazyEvaluator, CanSetMethodAndEvaluate)
    {
        auto method = []() { return 42; };
        this->evaluator.SetMethod(method);
        ASSERT_THAT(this->evaluator.Value(), Eq(42));
    }

    TEST_F(ALazyEvaluator, CanPassMethodToCtorAndEvaluate)
    {
        auto method = []() -> int { return 42; };
        this->evaluator = LazyEvaluator<int>(method);
        ASSERT_THAT(this->evaluator.Value(), Eq(42));
    }
} // namespace fluid_props::testing
