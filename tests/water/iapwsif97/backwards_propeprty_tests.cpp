#include "bivariant_fixture.hpp"
#include <water/iapwsif97/iapwsif97.hpp>

namespace fluid_props::testing::water
{
    using Water = fluid_props::water::IAPWSIF97;

    DEFINE_BIVARIATE_PROPERTY_TESTS(
        IAPWSIF97,
        EnthalpicTemperature,
        FromPressureAndEnthalpy,
        Water,
        PressureT<MegaPascal>,
        SpecificEnergyT<KiloJoule_KiloGram>,
        TemperatureT<Kelvin>,
        &Water::Temperature,
        9,
        std::make_tuple(3.0, 500.0, 391.798509),
        std::make_tuple(80.0, 500.0, 378.108626),
        std::make_tuple(80.0, 1500.0, 611.041229),
        std::make_tuple(0.001, 3000.0, 534.433241),
        std::make_tuple(3.0, 3000.0, 575.373370),
        std::make_tuple(3.0, 4000.0, 1010.77577),
        std::make_tuple(5.0, 3500.0, 801.299102),
        std::make_tuple(5.0, 4000.0, 1015.31583),
        std::make_tuple(25.0, 3500.0, 875.279054),
        std::make_tuple(40.0, 2700.0, 743.056411),
        std::make_tuple(60.0, 2700.0, 791.137067),
        std::make_tuple(60.0, 3200.0, 882.756860),
        std::make_tuple(0.1, 4500.0, 1215.17671),
        std::make_tuple(1.0, 4500.0, 1216.39219),
        std::make_tuple(1.0, 5000.0, 1415.63620),
        std::make_tuple(20.0, 1700.0, 629.3083892),
        std::make_tuple(50.0, 2000.0, 690.5718338),
        std::make_tuple(100.0, 2100.0, 733.6163014),
        std::make_tuple(20.0, 2500.0, 641.8418053),
        std::make_tuple(50.0, 2400.0, 735.1848618),
        std::make_tuple(100.0, 2700.0, 842.0460876)
    )

    DEFINE_BIVARIATE_PROPERTY_TESTS(
        IAPWSIF97,
        EntropicTemperature,
        FromPressureAndEntropy,
        Water,
        PressureT<MegaPascal>,
        SpecificEntropyT<KiloJoule_KiloGram_Kelvin>,
        TemperatureT<Kelvin>,
        &Water::Temperature,
        9,
        std::make_tuple(3.0, 0.5, 307.842258),
        std::make_tuple(80.0, 0.5, 309.979785),
        std::make_tuple(80.0, 3.0, 565.899909),
        std::make_tuple(0.1, 7.5, 399.517097),
        std::make_tuple(0.1, 8.0, 514.127081),
        std::make_tuple(2.5, 8.0, 1039.84917),
        std::make_tuple(8.0, 6.0, 600.484040),
        std::make_tuple(8.0, 7.5, 1064.95556),
        std::make_tuple(90.0, 6.0, 1038.01126),
        std::make_tuple(20.0, 5.75, 697.992849),
        std::make_tuple(80.0, 5.25, 854.011484),
        std::make_tuple(80.0, 5.75, 949.017998),
        std::make_tuple(0.1, 9.156, 895.644940),
        std::make_tuple(1.0, 9.156, 1400.58224),
        std::make_tuple(1.0, 10.0, 1917.05663),
        std::make_tuple(20.0, 3.8, 628.2959869),
        std::make_tuple(50.0, 3.6, 629.7158726),
        std::make_tuple(100.0, 4.0, 705.6880237),
        std::make_tuple(20.0, 5.0, 640.1176443),
        std::make_tuple(50.0, 4.5, 716.3687517),
        std::make_tuple(100.0, 5.0, 847.4332825)

    );

} // namespace fluid_props::testing::water
