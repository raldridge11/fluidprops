#include "fluid_fixture.hpp"
#include "twophase_fixture.hpp"
#include "univariant_fixture.hpp"
#include <exceptions/exceptions.hpp>
#include <water/iapwsif97/saturation.hpp>

namespace fluid_props::testing::water
{
    using Water = fluid_props::water::IAPWSIF97Saturation;

#define IAPWSIF97_PHASE_SATURATION_TEST(PropertyName, DependentPropertyUnit, Method, SigFigs, ...)                     \
    DEFINE_PHASE_PROPERTY_TESTS(                                                                                       \
        IAPWSIF97,                                                                                                     \
        PropertyName,                                                                                                  \
        FromPressure,                                                                                                  \
        Water,                                                                                                         \
        PressureT<MegaPascal>,                                                                                         \
        DependentPropertyUnit,                                                                                         \
        &Water::Pressure,                                                                                              \
        Method,                                                                                                        \
        SigFigs,                                                                                                       \
        __VA_ARGS__                                                                                                    \
    );

    class IAPWSIF97SaturationBounds : public FluidFixture<Water>
    {
    };

    TEST_F(IAPWSIF97SaturationBounds, PropertyOutOfBoundsIsThrownIfTemperatureIsInvalidForSaturationPressure)
    {
        ASSERT_THROW(this->fluid.Temperature(TemperatureT<Kelvin>{650.0}), fluid_props::PropertyOutOfBounds);
    }

    TEST_F(IAPWSIF97SaturationBounds, PropertyOutOfBoundsIsThrownIfPressureIsInvalidForSaturationTemperature)
    {
        auto pressure = PressureT<Pascal>{600.0};
        ASSERT_THROW(this->fluid.Pressure(pressure.As(MegaPascal{})), fluid_props::PropertyOutOfBounds);
    }

    DEFINE_UNIVARIATE_PROPERTY_TESTS(
        IAPWSIF97,
        SaturationPressure,
        FromTemperature,
        Water,
        TemperatureT<Kelvin>,
        PressureT<MegaPascal>,
        &Water::Temperature,
        &Water::Pressure,
        9,
        std::make_tuple(300.0, 0.353658941e-2),
        std::make_tuple(500.0, 2.63889776),
        std::make_tuple(600.0, 12.3443146)
    );

    DEFINE_UNIVARIATE_PROPERTY_TESTS(
        IAPWSIF97,
        SaturationTemperature,
        FromPressure,
        Water,
        PressureT<MegaPascal>,
        TemperatureT<Kelvin>,
        &Water::Pressure,
        &Water::Temperature,
        9,
        std::make_tuple(0.1, 372.755919),
        std::make_tuple(1.0, 453.035632),
        std::make_tuple(10.0, 584.149488)
    );

    IAPWSIF97_PHASE_SATURATION_TEST(
        SaturationEnthalpy,
        SpecificEnergyT<KiloJoule_KiloGram>,
        &Water::Enthalpy,
        9,
        std::make_tuple(0.1, Phase::Liquid, 417.436486),
        std::make_tuple(0.1, Phase::Vapor, 2674.94964)
    );

    IAPWSIF97_PHASE_SATURATION_TEST(
        SaturationSpecificVolume,
        SpecificVolumeT<CubicMeter_KiloGram>,
        &Water::SpecificVolume,
        9,
        std::make_tuple(0.1, Phase::Liquid, 0.00104314784),
        std::make_tuple(0.1, Phase::Vapor, 1.69402252)
    );

    IAPWSIF97_PHASE_SATURATION_TEST(
        SaturationDensity,
        DensityT<KiloGram_CubicMeter>,
        &Water::Density,
        8,
        std::make_tuple(0.1, Phase::Liquid, 1.0 / 0.00104314784),
        std::make_tuple(0.1, Phase::Vapor, 1.0 / 1.69402252)
    );

    IAPWSIF97_PHASE_SATURATION_TEST(
        SaturationInternalEnergy,
        SpecificEnergyT<KiloJoule_KiloGram>,
        &Water::InternalEnergy,
        9,
        std::make_tuple(0.1, Phase::Liquid, 417.332171),
        std::make_tuple(0.1, Phase::Vapor, 2505.54739)
    );

    IAPWSIF97_PHASE_SATURATION_TEST(
        SaturationEntropy,
        SpecificEntropyT<KiloJoule_KiloGram_Kelvin>,
        &Water::Entropy,
        9,
        std::make_tuple(0.1, Phase::Liquid, 1.30256017),
        std::make_tuple(0.1, Phase::Vapor, 7.35880664)
    );

    IAPWSIF97_PHASE_SATURATION_TEST(
        SaturationIsobaricHeatCapacity,
        HeatCapacityT<KiloJoule_KiloGram_Kelvin>,
        &Water::IsobaricHeatCapacity,
        9,
        std::make_tuple(0.1, Phase::Liquid, 4.21614943),
        std::make_tuple(0.1, Phase::Vapor, 2.07593803)
    );

    IAPWSIF97_PHASE_SATURATION_TEST(
        SaturationIsochoricHeatCapacity,
        HeatCapacityT<KiloJoule_KiloGram_Kelvin>,
        &Water::IsochoricHeatCapacity,
        9,
        std::make_tuple(0.1, Phase::Liquid, 3.76969968),
        std::make_tuple(0.1, Phase::Vapor, 1.55269698)
    );

    IAPWSIF97_PHASE_SATURATION_TEST(
        SaturationSpeedOfSound,
        VelocityT<Meter_Second>,
        &Water::SpeedOfSound,
        9,
        std::make_tuple(0.1, Phase::Liquid, 1545.45195),
        std::make_tuple(0.1, Phase::Vapor, 472.054157)
    );

    IAPWSIF97_PHASE_SATURATION_TEST(
        SaturationCubicExpansion,
        ThermalExpansionT<IKelvin>,
        &Water::CubicExpansion,
        9,
        std::make_tuple(0.1, Phase::Liquid, 0.000748898795),
        std::make_tuple(0.1, Phase::Vapor, 0.00290208842)
    );

    IAPWSIF97_PHASE_SATURATION_TEST(
        SaturationCompressibility,
        CompressibilityT<IMegaPascal>,
        &Water::Compressibility,
        9,
        std::make_tuple(0.1, Phase::Liquid, 0.000488476969),
        std::make_tuple(0.1, Phase::Vapor, 10.1639660)
    );

    DEFINE_UNIVARIATE_PROPERTY_TESTS(
        IAPWSIF97,
        SurfaceTension,
        FromTemperature,
        Water,
        TemperatureT<Kelvin>,
        SurfaceTensionT<Newton_Meter>,
        &Water::Temperature,
        &Water::SurfaceTension,
        9,
        std::make_tuple(300.0, 0.716859625e-1),
        std::make_tuple(450.0, 0.428914992e-1),
        std::make_tuple(600.0, 0.837561087e-2)
    );

} // namespace fluid_props::testing::water
