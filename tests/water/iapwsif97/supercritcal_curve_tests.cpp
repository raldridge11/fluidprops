#include "matchers.hpp"
#include <exceptions/exceptions.hpp>
#include <water/iapwsif97/curves/supercritical.hpp>

using ::testing::Test;

namespace fluid_props::testing::water
{
    using SuperCriticalCurve = fluid_props::water::SuperCriticalCurve;

    const auto kPressure = PressureT<MegaPascal>{16.5291643};
    const auto kTemperature = TemperatureT<Kelvin>{623.15};

    class IAPWSIF97SuperCriticalCurve : public Test
    {
    public:
        SuperCriticalCurve curve = SuperCriticalCurve();
    };

    TEST_F(IAPWSIF97SuperCriticalCurve, MeetsTemperatureGivenPressure)
    {
        this->curve.SetState(kPressure);
        EXPECT_THAT(this->curve.Temperature(), PropertyTEq(kTemperature, 9));
    }

    TEST_F(IAPWSIF97SuperCriticalCurve, MeetsPressureGivenTemperature)
    {
        this->curve.SetState(kTemperature);
        EXPECT_THAT(this->curve.Pressure(), PropertyTEq(kPressure, 9));
    }

    TEST_F(IAPWSIF97SuperCriticalCurve, PropertyOutOfBoundsIsThrownIfTemperatureIsInvalid)
    {
        this->curve.SetState(TemperatureT<Kelvin>{300.0});
        ASSERT_THROW((void)this->curve.Pressure(), fluid_props::PropertyOutOfBounds);
    }

    TEST_F(IAPWSIF97SuperCriticalCurve, PropertyOutOfBoundsIsThrownIfPressureIsInvalid)
    {
        this->curve.SetState(PressureT<MegaPascal>{300.0});
        ASSERT_THROW((void)this->curve.Temperature(), fluid_props::PropertyOutOfBounds);
    }

} // namespace fluid_props::testing::water
