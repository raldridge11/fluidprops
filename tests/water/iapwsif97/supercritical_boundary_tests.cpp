#include "matchers.hpp"
#include <exceptions/exceptions.hpp>
#include <water/iapwsif97/curves/supercritical.hpp>

using ::testing::Test;

namespace fluid_props::testing::water
{
    using SuperCriticalBoundary = fluid_props::water::SuperCriticalBoundary;

    const auto kPressure = PressureT<MegaPascal>{25.0};
    const auto kEnthalpy = SpecificEnergyT<KiloJoule_KiloGram>{2095.936454};

    class IAPWSIF97SuperCriticalBoundary : public Test
    {
    public:
        SuperCriticalBoundary curve = SuperCriticalBoundary();
    };

    TEST_F(IAPWSIF97SuperCriticalBoundary, MeetsEnthalpyGivenPressure)
    {
        this->curve.SetState(kPressure);
        EXPECT_THAT(this->curve.Enthalpy(), PropertyTEq(kEnthalpy, 9));
    }

    TEST_F(IAPWSIF97SuperCriticalBoundary, PropertyOutOfBoundsIsThrownIfPressureIsInvalid)
    {
        this->curve.SetState(PressureT<MegaPascal>{3.0});
        ASSERT_THROW((void)this->curve.Enthalpy(), fluid_props::PropertyOutOfBounds);
    }

} // namespace fluid_props::testing::water
