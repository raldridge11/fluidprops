#include "bivariant_fixture.hpp"
#include <water/iapwsif97/iapwsif97.hpp>

namespace fluid_props::testing::water
{
    using Water = fluid_props::water::IAPWSIF97;

    struct PhaseProperties
    {
        double liquid, vapor;
    };

    const auto kPressure = 0.1;

    const auto kTemperature = 372.755919;

    const auto kEnthalpy = PhaseProperties{.liquid = 417.436486, .vapor = 2674.94964};

    const auto kSpecificVolume = PhaseProperties{.liquid = 0.00104314784, .vapor = 1.69402252};

    const auto kVoidFraction = PhaseProperties{.liquid = 1.0 - 0.999384597, .vapor = 0.999384597};

    const auto kInternalEnergy = PhaseProperties{.liquid = 417.332171, .vapor = 2505.54739};

    const auto kEntropy = PhaseProperties{.liquid = 1.30256017, .vapor = 7.35880664};

    const auto kIsobaricHeatCapacity = PhaseProperties{.liquid = 4.21614943, .vapor = 2.07593803};

    const auto kIsochoricHeatCapacity = PhaseProperties{.liquid = 3.76969968, .vapor = 1.55269698};

    const auto kSpeedOfSound = PhaseProperties{.liquid = 1545.45195, .vapor = 472.054157};

    const auto kCubicExpansion = PhaseProperties{.liquid = 0.000748898795, .vapor = 0.00290208842};

    const auto kCompressibility = PhaseProperties{.liquid = 0.000488476969, .vapor = 10.1639660};

#define IAPWSIF97_MIXTURE_TEST(PropertyName, PropertyUnit, Method, SigFigs, ...)                                       \
    DEFINE_BIVARIATE_PROPERTY_TESTS(                                                                                   \
        IAPWSIF97,                                                                                                     \
        PropertyName,                                                                                                  \
        FromPressureAndQuality,                                                                                        \
        Water,                                                                                                         \
        PressureT<MegaPascal>,                                                                                         \
        QualityT<Dimensionless>,                                                                                       \
        PropertyUnit,                                                                                                  \
        Method,                                                                                                        \
        SigFigs,                                                                                                       \
        __VA_ARGS__                                                                                                    \
    );
#define IAPWSIF97_MIXTURE_TEST_FROM_TEMPERATURE(PropertyName, PropertyUnit, Method, SigFigs, ...)                      \
    DEFINE_BIVARIATE_PROPERTY_TESTS(                                                                                   \
        IAPWSIF97,                                                                                                     \
        PropertyName,                                                                                                  \
        FromTemperatureAndQuality,                                                                                     \
        Water,                                                                                                         \
        TemperatureT<Kelvin>,                                                                                          \
        QualityT<Dimensionless>,                                                                                       \
        PropertyUnit,                                                                                                  \
        Method,                                                                                                        \
        SigFigs,                                                                                                       \
        __VA_ARGS__                                                                                                    \
    );

    IAPWSIF97_MIXTURE_TEST(
        MixtureEnthalpy,
        SpecificEnergyT<KiloJoule_KiloGram>,
        &Water::Enthalpy,
        9,
        std::make_tuple(kPressure, 0.0, kEnthalpy.liquid),
        std::make_tuple(kPressure, 0.25, 0.75 * kEnthalpy.liquid + 0.25 * kEnthalpy.vapor),
        std::make_tuple(kPressure, 1.0, kEnthalpy.vapor)

    );

    IAPWSIF97_MIXTURE_TEST_FROM_TEMPERATURE(
        MixtureEnthalpyT,
        SpecificEnergyT<KiloJoule_KiloGram>,
        &Water::Enthalpy,
        8,
        std::make_tuple(kTemperature, 0.0, kEnthalpy.liquid),
        std::make_tuple(kTemperature, 0.25, 0.75 * kEnthalpy.liquid + 0.25 * kEnthalpy.vapor),
        std::make_tuple(kTemperature, 1.0, kEnthalpy.vapor)
    );

    IAPWSIF97_MIXTURE_TEST(
        VoidFraction,
        VoidT<Dimensionless>,
        &Water::VoidFraction,
        9,
        std::make_tuple(kPressure, 0.0, 0.0),
        std::make_tuple(kPressure, 1.0, 1.0),
        std::make_tuple(kPressure, 0.5, kVoidFraction.vapor)
    );

    IAPWSIF97_MIXTURE_TEST(
        MixtureSpecificVolume,
        SpecificVolumeT<CubicMeter_KiloGram>,
        &Water::SpecificVolume,
        9,
        std::make_tuple(kPressure, 0.0, kSpecificVolume.liquid),
        std::make_tuple(
            kPressure, 0.5, kVoidFraction.liquid *kSpecificVolume.liquid + kVoidFraction.vapor * kSpecificVolume.vapor
        ),
        std::make_tuple(kPressure, 1.0, kSpecificVolume.vapor)

    );

    IAPWSIF97_MIXTURE_TEST(
        MixtureInternalEnergy,
        SpecificEnergyT<KiloJoule_KiloGram>,
        &Water::InternalEnergy,
        9,
        std::make_tuple(kPressure, 0.0, kInternalEnergy.liquid),
        std::make_tuple(kPressure, 0.5, 0.5 * kInternalEnergy.liquid + 0.5 * kInternalEnergy.vapor),
        std::make_tuple(kPressure, 1.0, kInternalEnergy.vapor)
    );

    IAPWSIF97_MIXTURE_TEST(
        MixtureEntropy,
        SpecificEntropyT<KiloJoule_KiloGram_Kelvin>,
        &Water::Entropy,
        9,
        std::make_tuple(kPressure, 0.0, kEntropy.liquid),
        std::make_tuple(kPressure, 0.5, 0.5 * kEntropy.liquid + 0.5 * kEntropy.vapor),
        std::make_tuple(kPressure, 1.0, kEntropy.vapor)
    );

    IAPWSIF97_MIXTURE_TEST(
        MixtureIsobaricHeatCapacity,
        HeatCapacityT<KiloJoule_KiloGram_Kelvin>,
        &Water::IsobaricHeatCapacity,
        9,
        std::make_tuple(kPressure, 0.0, kIsobaricHeatCapacity.liquid),
        std::make_tuple(kPressure, 0.5, 0.5 * kIsobaricHeatCapacity.liquid + 0.5 * kIsobaricHeatCapacity.vapor),
        std::make_tuple(kPressure, 1.0, kIsobaricHeatCapacity.vapor)
    );

    IAPWSIF97_MIXTURE_TEST(
        MixtureIsochoricHeatCapacity,
        HeatCapacityT<KiloJoule_KiloGram_Kelvin>,
        &Water::IsochoricHeatCapacity,
        9,
        std::make_tuple(kPressure, 0.0, kIsochoricHeatCapacity.liquid),
        std::make_tuple(kPressure, 0.5, 0.5 * kIsochoricHeatCapacity.liquid + 0.5 * kIsochoricHeatCapacity.vapor),
        std::make_tuple(kPressure, 1.0, kIsochoricHeatCapacity.vapor)
    );

    IAPWSIF97_MIXTURE_TEST(
        MixtureSpeedOfSound,
        VelocityT<Meter_Second>,
        &Water::SpeedOfSound,
        9,
        std::make_tuple(kPressure, 0.0, kSpeedOfSound.liquid),
        std::make_tuple(
            kPressure, 0.5, kVoidFraction.liquid *kSpeedOfSound.liquid + kVoidFraction.vapor * kSpeedOfSound.vapor
        ),
        std::make_tuple(kPressure, 1.0, kSpeedOfSound.vapor)
    );

    IAPWSIF97_MIXTURE_TEST(
        MixtureCubicExpansion,
        ThermalExpansionT<IKelvin>,
        &Water::CubicExpansion,
        9,
        std::make_tuple(kPressure, 0.0, kCubicExpansion.liquid),
        std::make_tuple(
            kPressure, 0.5, kVoidFraction.liquid *kCubicExpansion.liquid + kVoidFraction.vapor * kCubicExpansion.vapor
        ),
        std::make_tuple(kPressure, 1.0, kCubicExpansion.vapor)
    );

    IAPWSIF97_MIXTURE_TEST(
        MixtureCompressibility,
        CompressibilityT<IMegaPascal>,
        &Water::Compressibility,
        9,
        std::make_tuple(kPressure, 0.0, kCompressibility.liquid),
        std::make_tuple(
            kPressure, 0.5, kVoidFraction.liquid *kCompressibility.liquid + kVoidFraction.vapor * kCompressibility.vapor
        ),
        std::make_tuple(kPressure, 1.0, kCompressibility.vapor)
    );
} // namespace fluid_props::testing::water
