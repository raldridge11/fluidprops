#include "matchers.hpp"
#include <exceptions/exceptions.hpp>
#include <water/iapwsif97/curves/steam_boundary.hpp>

using ::testing::Test;

namespace fluid_props::testing::water
{
    using SteamBoundaryCurve = fluid_props::water::SteamBoundaryCurve;

    const auto kPressure = PressureT<MegaPascal>{100.0};
    const auto kEnthalpy = SpecificEnergyT<KiloJoule_KiloGram>{3516.004323};

    class IAPWSIF97SteamBoundaryCurve : public Test
    {
    public:
        SteamBoundaryCurve curve = SteamBoundaryCurve{};
    };

    TEST_F(IAPWSIF97SteamBoundaryCurve, MeetsPressureGivenEnthalpy)
    {
        this->curve.SetState(kEnthalpy);
        EXPECT_THAT(this->curve.Pressure(), PropertyTEq(kPressure, 9));
    }

    TEST_F(IAPWSIF97SteamBoundaryCurve, MeetsEnthalpyGivenPressure)
    {
        this->curve.SetState(kPressure);
        EXPECT_THAT(this->curve.Enthalpy(), PropertyTEq(kEnthalpy, 9));
    }

    TEST_F(IAPWSIF97SteamBoundaryCurve, PropertyOutOfBoundsIsThrownIfTemperatureIsInvalid)
    {
        this->curve.SetState(SpecificEnergyT<KiloJoule_KiloGram>{300.0});
        ASSERT_THROW((void)this->curve.Pressure(), fluid_props::PropertyOutOfBounds);
    }

    TEST_F(IAPWSIF97SteamBoundaryCurve, PropertyOutOfBoundsIsThrownIfPressureIsInvalid)
    {
        this->curve.SetState(PressureT<MegaPascal>{300.0});
        ASSERT_THROW((void)this->curve.Enthalpy(), fluid_props::PropertyOutOfBounds);
    }

} // namespace fluid_props::testing::water
