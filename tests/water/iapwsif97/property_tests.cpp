#include "bivariant_fixture.hpp"
#include "fluid_fixture.hpp"
#include <water/iapwsif97/iapwsif97.hpp>

namespace fluid_props::testing::water
{

    using Water = fluid_props::water::IAPWSIF97;

#define IAPWSIF97_PRESSSURE_TEMPERATURE_TEST(PropertyName, PropertyUnit, Method, SigFigs, ...)                         \
    DEFINE_BIVARIATE_PROPERTY_TESTS(                                                                                   \
        IAPWSIF97,                                                                                                     \
        PropertyName,                                                                                                  \
        FromPressureAndTemperature,                                                                                    \
        Water,                                                                                                         \
        PressureT<MegaPascal>,                                                                                         \
        TemperatureT<Kelvin>,                                                                                          \
        PropertyUnit,                                                                                                  \
        Method,                                                                                                        \
        SigFigs,                                                                                                       \
        __VA_ARGS__                                                                                                    \
    );

    class IAPWSIF97RangeOfValidity : public FluidFixture<Water>
    {
    };

    TEST_F(IAPWSIF97RangeOfValidity, ThrowsPropertyErrorWhenOutOfRange)
    {
        ASSERT_THROW(
            this->fluid.SetState(PressureT<MegaPascal>{75.0}, TemperatureT<Kelvin>{2000.0}),
            fluid_props::PropertyOutOfBounds
        );
    }

    IAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        Enthalpy,
        SpecificEnergyT<KiloJoule_KiloGram>,
        &Water::Enthalpy,
        9,
        std::make_tuple(3.0, 300.0, 115.331273),
        std::make_tuple(80.0, 300.0, 184.142828),
        std::make_tuple(3.0, 500.0, 975.542239),
        std::make_tuple(0.0035, 300.0, 2549.91145),
        std::make_tuple(0.0035, 700.0, 3335.68375),
        std::make_tuple(30.0, 700.0, 2631.49474),
        std::make_tuple(0.5, 1500.0, 5219.76855),
        std::make_tuple(30.0, 1500.0, 5167.23514),
        std::make_tuple(30.0, 2000.0, 6571.22604),
        std::make_tuple(25.5837018, 650.0, 1863.43019),
        std::make_tuple(22.2930643, 650.0, 2375.1240),
        std::make_tuple(78.3095639, 750.0, 2258.68845)
    );

    IAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        SpecificVolume,
        SpecificVolumeT<CubicMeter_KiloGram>,
        &Water::SpecificVolume,
        8,
        std::make_tuple(3.0, 300.0, 0.100215168e-2),
        std::make_tuple(80.0, 300.0, 0.971180894e-3),
        std::make_tuple(3.0, 500.0, 0.120241800e-2),
        std::make_tuple(0.0035, 300.0, 0.394913866e2),
        std::make_tuple(0.0035, 700.0, 0.923015898e2),
        std::make_tuple(30.0, 700.0, 0.542946619e-2),
        std::make_tuple(0.5, 1500.0, 0.138455090e1),
        std::make_tuple(30.0, 1500.0, 0.230761299e-1),
        std::make_tuple(30.0, 2000.0, 0.311385219e-1),
        std::make_tuple(25.5837018, 650.0, 1.0 / 500.0),
        std::make_tuple(22.2930643, 650.0, 1.0 / 200.0),
        std::make_tuple(78.3095639, 750.0, 1.0 / 500.0),
        std::make_tuple(60.0, 673.15, 1.0 / 612.391201)
    );

    IAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        Density,
        DensityT<KiloGram_CubicMeter>,
        &Water::Density,
        8,
        std::make_tuple(3.0, 300.0, 1.0 / 0.100215168e-2),
        std::make_tuple(80.0, 300.0, 1.0 / 0.971180894e-3),
        std::make_tuple(3.0, 500.0, 1.0 / 0.120241800e-2),
        std::make_tuple(0.0035, 300.0, 1.0 / 0.394913866e2),
        std::make_tuple(0.0035, 700.0, 1.0 / 0.923015898e2),
        std::make_tuple(30.0, 700.0, 1.0 / 0.542946619e-2),
        std::make_tuple(0.5, 1500.0, 1.0 / 0.138455090e1),
        std::make_tuple(30.0, 1500.0, 1.0 / 0.230761299e-1),
        std::make_tuple(30.0, 2000.0, 1.0 / 0.311385219e-1),
        std::make_tuple(25.5837018, 650.0, 500.0),
        std::make_tuple(22.2930643, 650.0, 200.0),
        std::make_tuple(78.3095639, 750.0, 500.0),
        std::make_tuple(60.0, 673.15, 612.391201)
    );

    IAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        InternalEnergy,
        SpecificEnergyT<KiloJoule_KiloGram>,
        &Water::InternalEnergy,
        9,
        std::make_tuple(3.0, 300.0, 112.324818),
        std::make_tuple(80.0, 300.0, 106.448356),
        std::make_tuple(3.0, 500.0, 971.934985),
        std::make_tuple(0.0035, 300.0, 0.241169160e4),
        std::make_tuple(0.0035, 700.0, 0.301262819e4),
        std::make_tuple(30.0, 700.0, 0.246861076e4),
        std::make_tuple(0.5, 1500.0, 0.452749310e4),
        std::make_tuple(30.0, 1500.0, 0.447495124e4),
        std::make_tuple(30.0, 2000.0, 0.563707038e4),
        std::make_tuple(25.5837018, 650.0, 0.181226279e4),
        std::make_tuple(22.2930643, 650.0, 0.226365868e4),
        std::make_tuple(78.3095639, 750.0, 0.210206932e4)

    );

    IAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        Entropy,
        SpecificEntropyT<KiloJoule_KiloGram_Kelvin>,
        &Water::Entropy,
        9,
        std::make_tuple(3.0, 300.0, 0.392294792),
        std::make_tuple(80.0, 300.0, 0.368563852),
        std::make_tuple(3.0, 500.0, 2.58041912),
        std::make_tuple(0.0035, 300.0, 0.852238967e1),
        std::make_tuple(0.0035, 700.0, 0.101749996e2),
        std::make_tuple(30.0, 700.0, 0.517540298e1),
        std::make_tuple(0.5, 1500.0, 0.965408875e1),
        std::make_tuple(30.0, 1500.0, 0.772970133e1),
        std::make_tuple(30.0, 2000.0, 0.853640523e1),
        std::make_tuple(25.5837018, 650.0, 0.405427273e1),
        std::make_tuple(22.2930643, 650.0, 0.48543879e1),
        std::make_tuple(78.3095639, 750.0, 0.446971906e1)
    );

    IAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        IsobaricHeatCapacity,
        HeatCapacityT<KiloJoule_KiloGram_Kelvin>,
        &Water::IsobaricHeatCapacity,
        9,
        std::make_tuple(3.0, 300.0, 4.17301218),
        std::make_tuple(80.0, 300.0, 4.01008987),
        std::make_tuple(3.0, 500.0, 4.65580682),
        std::make_tuple(0.0035, 300.0, 0.191300162e1),
        std::make_tuple(0.0035, 700.0, 0.208141274e1),
        std::make_tuple(30.0, 700.0, 0.103505092e2),
        std::make_tuple(0.5, 1500.0, 0.261609445e1),
        std::make_tuple(30.0, 1500.0, 0.272724317e1),
        std::make_tuple(30.0, 2000.0, 0.288569882e1),
        std::make_tuple(25.5837018, 650.0, 13.8935717),
        std::make_tuple(22.2930643, 650.0, 44.6579372),
        std::make_tuple(78.3095639, 750.0, 6.34165359)
    );

    IAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        IsochoricHeatCapacity,
        HeatCapacityT<KiloJoule_KiloGram_Kelvin>,
        &Water::IsochoricHeatCapacity,
        9,
        std::make_tuple(3.0, 300.0, 4.12120160),
        std::make_tuple(80.0, 300.0, 3.91736606),
        std::make_tuple(3.0, 500.0, 3.22139223),
        std::make_tuple(0.0035, 300.0, 1.44132662),
        std::make_tuple(0.0035, 700.0, 1.61978333),
        std::make_tuple(30.0, 700.0, 2.97553837),
        std::make_tuple(0.5, 1500.0, 2.15337784),
        std::make_tuple(30.0, 1500.0, 2.19274829),
        std::make_tuple(30.0, 2000.0, 2.39589436),
        std::make_tuple(25.5837018, 650.0, 3.19131787),
        std::make_tuple(22.2930643, 650.0, 4.0411808),
        std::make_tuple(78.3095639, 750.0, 2.71701677)
    );

    IAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        SpeedOfSound,
        VelocityT<Meter_Second>,
        &Water::SpeedOfSound,
        6,
        std::make_tuple(3.0, 300.0, 1507.7321),
        std::make_tuple(80.0, 300.0, 1634.69054),
        std::make_tuple(3.0, 500.0, 1240.71337),
        std::make_tuple(0.0035, 300.0, 0.427920172e3),
        std::make_tuple(0.0035, 700.0, 0.644289068e3),
        std::make_tuple(30.0, 700.0, 0.480386523e3),
        std::make_tuple(0.5, 1500.0, 0.917068690e3),
        std::make_tuple(30.0, 1500.0, 0.928548002e3),
        std::make_tuple(30.0, 2000.0, 0.106736948e4),
        std::make_tuple(25.5837018, 650.0, 0.502005554e3),
        std::make_tuple(22.2930643, 650.0, 0.383444594e3),
        std::make_tuple(78.3095639, 750.0, 0.760696041e3)
    );

    IAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        CubicExpansion,
        ThermalExpansionT<IKelvin>,
        &Water::CubicExpansion,
        9,
        std::make_tuple(3.0, 300.0, 0.277354533e-3),
        std::make_tuple(80.0, 300.0, 0.344095843e-3),
        std::make_tuple(3.0, 500.0, 0.164118128e-2),
        std::make_tuple(0.0035, 300.0, 0.337578289e-2),
        std::make_tuple(0.0035, 700.0, 0.142878736e-2),
        std::make_tuple(30.0, 700.0, 0.126019688e-1),
        std::make_tuple(0.5, 1500.0, 0.667539000e-3),
        std::make_tuple(30.0, 1500.0, 0.716950754e-3),
        std::make_tuple(30.0, 2000.0, 0.508830641e-3),
        std::make_tuple(25.5837018, 650.0, 0.168653107e-1),
        std::make_tuple(22.2930643, 650.0, 0.68531228e-1),
        std::make_tuple(78.3095639, 750.0, 0.441515098e-2)
    );

    IAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        Compressibility,
        CompressibilityT<IMegaPascal>,
        &Water::Compressibility,
        9,
        std::make_tuple(3.0, 300.0, 0.446382123e-3),
        std::make_tuple(80.0, 300.0, 0.372039437e-3),
        std::make_tuple(3.0, 500.0, 0.112892188e-2),
        std::make_tuple(0.0035, 300.0, 0.286239651e3),
        std::make_tuple(0.0035, 700.0, 0.285725461e3),
        std::make_tuple(30.0, 700.0, 0.818411389e-1),
        std::make_tuple(0.5, 1500.0, 0.200003859e1),
        std::make_tuple(30.0, 1500.0, 0.332881253e-1),
        std::make_tuple(30.0, 2000.0, 0.329193892e-1),
        std::make_tuple(25.5837018, 650.0, 0.345506959e-1),
        std::make_tuple(22.2930643, 650.0, 0.3757985876),
        std::make_tuple(78.3095639, 750.0, 0.806710817e-2)
    );

    IAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        Viscosity,
        ViscosityT<PascalSecond>,
        &Water::Viscosity,
        9,
        std::make_tuple(0.1, 298.15, 0.890022551e-3),
        std::make_tuple(20.0, 873.15, 0.339743835e-4),
        std::make_tuple(60.0, 673.15, 0.726093560e-4)
    );

    IAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        ThermalConductivity,
        ThermalConductivityT<Watts_Meter_Kelvin>,
        &Water::ThermalConductivity,
        6,
        std::make_tuple(0.1, 298.15, 0.607509806),
        std::make_tuple(10.0, 873.15, 0.867196197e-1),
        std::make_tuple(40.0, 673.15, 0.394226634)
    );

} // namespace fluid_props::testing::water
