#include "bivariant_fixture.hpp"
#include <water/iapwsif97/iapwsif97m.hpp>

namespace fluid_props::testing::water
{
    using Water = fluid_props::water::IAPWSIF97M;

#define METASTABLEIAPWSIF97_PRESSSURE_TEMPERATURE_TEST(PropertyName, PropertyUnit, Method, SigFigs, ...)               \
    DEFINE_BIVARIATE_PROPERTY_TESTS(                                                                                   \
        IAPWSIF97M,                                                                                                    \
        PropertyName,                                                                                                  \
        FromPressureAndTemperature,                                                                                    \
        Water,                                                                                                         \
        PressureT<MegaPascal>,                                                                                         \
        TemperatureT<Kelvin>,                                                                                          \
        PropertyUnit,                                                                                                  \
        Method,                                                                                                        \
        SigFigs,                                                                                                       \
        __VA_ARGS__                                                                                                    \
    );

    METASTABLEIAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        EnthalpyIF97M,
        SpecificEnergyT<KiloJoule_KiloGram>,
        &Water::Enthalpy,
        9,
        std::make_tuple(1.0, 450.0, 2768.81115),
        std::make_tuple(1.0, 440.0, 2740.15123),
        std::make_tuple(1.5, 450.0, 2721.34539)
    );

    METASTABLEIAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        SpecificVolumeIF97M,
        SpecificVolumeT<CubicMeter_KiloGram>,
        &Water::SpecificVolume,
        9,
        std::make_tuple(1.0, 450.0, 0.192516540),
        std::make_tuple(1.0, 440.0, 0.186212297),
        std::make_tuple(1.5, 450.0, 0.121685206)
    );

    METASTABLEIAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        DensityIF97M,
        DensityT<KiloGram_CubicMeter>,
        &Water::Density,
        8,
        std::make_tuple(1.0, 450.0, 1.0 / 0.192516540),
        std::make_tuple(1.0, 440.0, 1.0 / 0.186212297),
        std::make_tuple(1.5, 450.0, 1.0 / 0.121685206)
    );

    METASTABLEIAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        InternalEnergyIF97M,
        SpecificEnergyT<KiloJoule_KiloGram>,
        &Water::InternalEnergy,
        9,
        std::make_tuple(1.0, 450.0, 2576.29461),
        std::make_tuple(1.0, 440.0, 2553.93894),
        std::make_tuple(1.5, 450.0, 2538.81758)
    );

    METASTABLEIAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        EntropyIF97M,
        SpecificEntropyT<KiloJoule_KiloGram_Kelvin>,
        &Water::Entropy,
        9,
        std::make_tuple(1.0, 450.0, 6.56660377),
        std::make_tuple(1.0, 440.0, 6.50218759),
        std::make_tuple(1.5, 450.0, 6.29170440)
    );

    METASTABLEIAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        IsobaricHeatCapacityIF97M,
        HeatCapacityT<KiloJoule_KiloGram_Kelvin>,
        &Water::IsobaricHeatCapacity,
        9,
        std::make_tuple(1.0, 450.0, 2.76349265),
        std::make_tuple(1.0, 440.0, 2.98166443),
        std::make_tuple(1.5, 450.0, 3.62795578)
    );

    METASTABLEIAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        IsochoricHeatCapacityIF97M,
        HeatCapacityT<KiloJoule_KiloGram_Kelvin>,
        &Water::IsochoricHeatCapacity,
        9,
        std::make_tuple(1.0, 450.0, 1.95830730),
        std::make_tuple(1.0, 440.0, 2.08622142),
        std::make_tuple(1.5, 450.0, 2.41213708)
    );

    METASTABLEIAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        SpeedOfSoundIF97M,
        VelocityT<Meter_Second>,
        &Water::SpeedOfSound,
        9,
        std::make_tuple(1.0, 450.0, 498.408101),
        std::make_tuple(1.0, 440.0, 489.363295),
        std::make_tuple(1.5, 450.0, 481.941819)
    );

    METASTABLEIAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        CubicExpansionIF97M,
        ThermalExpansionT<IKelvin>,
        &Water::CubicExpansion,
        9,
        std::make_tuple(1.0, 450.0, 0.318819824e-2),
        std::make_tuple(1.0, 440.0, 0.348506136e-2),
        std::make_tuple(1.5, 450.0, 0.418276571e-2)
    );

    METASTABLEIAPWSIF97_PRESSSURE_TEMPERATURE_TEST(
        CompressibilityIF97M,
        CompressibilityT<IMegaPascal>,
        &Water::Compressibility,
        9,
        std::make_tuple(1.0, 450.0, 1.09364239),
        std::make_tuple(1.0, 440.0, 1.11133230),
        std::make_tuple(1.5, 450.0, 0.787967952)
    );
} // namespace fluid_props::testing::water
