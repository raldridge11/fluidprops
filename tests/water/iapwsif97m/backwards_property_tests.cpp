#include "bivariant_fixture.hpp"
#include <water/iapwsif97/iapwsif97m.hpp>

namespace fluid_props::testing::water
{
    using Water = fluid_props::water::IAPWSIF97M;

    DEFINE_BIVARIATE_PROPERTY_TESTS(
        IAPWSIF97M,
        EnthalpicTemperatureMetaStable,
        FromPressureAndEnthlapy,
        Water,
        PressureT<MegaPascal>,
        SpecificEnergyT<KiloJoule_KiloGram>,
        TemperatureT<Kelvin>,
        &Water::Temperature,
        9,
        std::make_tuple(1.0, 2768.81115, 450.0),
        std::make_tuple(1.0, 2740.15123, 440.0),
        std::make_tuple(1.5, 2721.34539, 450.0)
    );

    DEFINE_BIVARIATE_PROPERTY_TESTS(
        IAPWSIF97M,
        EntropicTemperatureMetaStable,
        FromPressureAndEntropy,
        Water,
        PressureT<MegaPascal>,
        SpecificEntropyT<KiloJoule_KiloGram_Kelvin>,
        TemperatureT<Kelvin>,
        &Water::Temperature,
        9,
        std::make_tuple(1.0, 6.56660377, 450.0),
        std::make_tuple(1.0, 6.50218759, 440.0),
        std::make_tuple(1.5, 6.29170440, 450.0)
    );

} // namespace fluid_props::testing::water
