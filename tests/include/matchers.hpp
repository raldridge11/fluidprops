#pragma once
#include "utility.hpp"
#include <gmock/gmock.h>

namespace fluid_props::testing
{
    /**
     * @brief Custom matcher for @see PropertyT
     *
     */
    // NOLINTNEXTLINE(modernize-use-trailing-return-type)
    MATCHER_P2(PropertyTEq, expected, significant_figures, "")
    {
        auto tolerance = CalculateTolerance(Tolerance{significant_figures, expected.Value()});
        return expected.AreEqual(arg, tolerance);
    }
} // namespace fluid_props::testing
