#pragma once
#include <cxxabi.h>
#include <fmt/core.h>
#include <typeinfo>
namespace fluid_props::testing
{

    /**
     * @brief Custom name generator to be used for conversion value tests
     *
     */
    class PropertyConversionNameGenerator
    {
        template <typename T> struct GetFirstTwoTypes;
        template <typename T1, typename T2, typename... Rest> struct GetFirstTwoTypes<std::tuple<T1, T2, Rest...>>
        {
            using FirstType = T1;
            using SecondType = T2;
        };

    public:
        template <typename T> static auto GetName(int index) -> std::string
        {
            using FirstType = typename GetFirstTwoTypes<T>::FirstType;
            using SecondType = typename GetFirstTwoTypes<T>::SecondType;

            auto firstTypeName = PropertyConversionNameGenerator::getCleanTypeName(typeid(FirstType));
            auto secondTypeName = PropertyConversionNameGenerator::getCleanTypeName(typeid(SecondType));

            return fmt::format("{}_to_{}", firstTypeName, secondTypeName);
        }

    private:
        static auto getCleanTypeName(const std::type_info &type_info) -> std::string
        {
            int status;
            auto demangled = abi::__cxa_demangle(type_info.name(), nullptr, nullptr, &status);
            if (status != 0)
            {
                return std::string{};
            }

            auto fullQualifiedName = std::string{demangled};
            auto pos = fullQualifiedName.find_last_of(':');
            if (pos == std::string::npos)
            {
                return fullQualifiedName;
            }

            return fullQualifiedName.substr(pos + 1);
        }
    };
} // namespace fluid_props::testing
