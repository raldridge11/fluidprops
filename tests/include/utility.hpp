#pragma once
#include <cmath>

namespace fluid_props::testing
{
    /**
     * @brief Defines a tolerance data set
     *
     */
    struct Tolerance
    {
        /**
         * @brief Number of significant figures allowed in tolerance.
         *
         */
        int significant_figures;

        /**
         * @brief expected value.
         *
         */
        double expected;
    };

    /**
     * @brief Calculates tolerance base on the amount of significant figures
     *
     * @param tolerance data set
     * @return double
     */
    [[maybe_unused]] static auto CalculateTolerance(Tolerance tolerance) -> double
    {
        auto expected = std::abs(tolerance.expected);
        auto base_tolerance = 10.0 * std::pow(10, -tolerance.significant_figures);
        auto magnitude = std::floor(std::log10(expected > 0.0 ? expected : 1.0));
        return base_tolerance * std::pow(10, magnitude);
    }
} // namespace fluid_props::testing
