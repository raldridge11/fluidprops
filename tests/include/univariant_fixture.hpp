#pragma once
#include "matchers.hpp"
#include <gtest/gtest.h>

namespace fluid_props::testing
{
    /**
     * @brief Test fixture for a fluid properties given one state variable
     *
     * @tparam FluidType Fluid properties type to test
     * @tparam PropertyUnit Independent state variable type
     * @tparam DependentPropertyUnit Type of property under test
     * @tparam (FluidType::*StateSetter)(PropertyUnit) Pointer to method that sets the independent state variable
     * @tparam (FluidType::*PropertyMethod)()  Pointer to method that calculates the desired property
     * @tparam SigFigs Number of significant figures to assert out to
     */
    template <
        typename FluidType,
        typename PropertyUnit,
        typename DependentPropertyUnit,
        void (FluidType::*StateSetter)(PropertyUnit),
        DependentPropertyUnit (FluidType::*PropertyMethod)(),
        int SigFigs = 9>
    class UniVariatePropertyTest : public ::testing::TestWithParam<std::tuple<PropertyUnit, DependentPropertyUnit>>
    {
    protected:
        void SetUp() override
        {
            this->fluid = std::make_unique<FluidType>();
            std::tie(this->property, this->expected) = this->GetParam();
        }

        void TestPropertyCalculation()
        {
            (this->fluid.get()->*StateSetter)(this->property);
            auto result = (this->fluid.get()->*PropertyMethod)();
            EXPECT_THAT(result, PropertyTEq(this->expected, SigFigs));
        }

    public:
        std::unique_ptr<FluidType> fluid;
        PropertyUnit property;
        DependentPropertyUnit expected;
    };

/**
 * @brief Defines parameterized test for a fluid property that is a function of one independent variable.
 * The parameters to the macro are same as the template arguments for the @see UniVariatePropertyTest.
 * The variadic arguments is expected to be a tuple of double values of the form,
 * (independent_value, expected_value)
 *
 */
#define DEFINE_UNIVARIATE_PROPERTY_TESTS(                                                                              \
    FluidName,                                                                                                         \
    PropertyName,                                                                                                      \
    TestName,                                                                                                          \
    FluidType,                                                                                                         \
    PropertyUnit,                                                                                                      \
    DependentPropertyUnit,                                                                                             \
    PropertySetter,                                                                                                    \
    PropertyMethod,                                                                                                    \
    SignificantFigures,                                                                                                \
    ...                                                                                                                \
)                                                                                                                      \
    using PropertyName = UniVariatePropertyTest<                                                                       \
        FluidType,                                                                                                     \
        PropertyUnit,                                                                                                  \
        DependentPropertyUnit,                                                                                         \
        PropertySetter,                                                                                                \
        PropertyMethod,                                                                                                \
        SignificantFigures>;                                                                                           \
    TEST_P(PropertyName, TestName)                                                                                     \
    {                                                                                                                  \
        this->TestPropertyCalculation();                                                                               \
    }                                                                                                                  \
    INSTANTIATE_TEST_SUITE_P(FluidName, PropertyName, ::testing::Values(__VA_ARGS__));
} // namespace fluid_props::testing
