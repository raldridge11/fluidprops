#pragma once
#include "matchers.hpp"
#include <gtest/gtest.h>

namespace fluid_props::testing
{
    /**
     * @brief Fixture for testing a fluid
     *
     * @tparam FluidType type of fluid to test
     */
    template <typename FluidType> class FluidFixture : public ::testing::Test
    {
    public:
        FluidType fluid = FluidType();
    };
} // namespace fluid_props::testing
