#pragma once
#include "matchers.hpp"
#include <gtest/gtest.h>

namespace fluid_props::testing
{
    /**
     * @brief Test fixture for a fluid properties given two state variable.
     *
     * @tparam FluidType Fluid properties type to test
     * @tparam FirstPropertyUnit First independent state variable type
     * @tparam SecondPropertyUnit Second independent state variable type
     * @tparam DependentPropertyUnit Type of property under test
     * @tparam (FluidType::*PropertyMethod)() Pointer to method that calculates the desired property
     * @tparam SigFigs Number of significant figures to assert out to
     */
    template <
        typename FluidType,
        typename FirstPropertyUnit,
        typename SecondPropertyUnit,
        typename DependentPropertyUnit,
        DependentPropertyUnit (FluidType::*PropertyMethod)(),
        int SigFigs = 9>
    class BiVariatePropertyTest
        : public ::testing::TestWithParam<std::tuple<FirstPropertyUnit, SecondPropertyUnit, DependentPropertyUnit>>
    {
    protected:
        void SetUp() override
        {
            this->fluid = std::make_unique<FluidType>();
            std::tie(this->first, this->second, this->expected) = this->GetParam();
        }

        void TestPropertyCalculation()
        {
            this->fluid->SetState(this->first, this->second);
            auto result = (this->fluid.get()->*PropertyMethod)();
            EXPECT_THAT(result, PropertyTEq(this->expected, SigFigs));
        }

    public:
        std::unique_ptr<FluidType> fluid;
        FirstPropertyUnit first;
        SecondPropertyUnit second;
        DependentPropertyUnit expected;
    };

/**
 * @brief Defines parameterized test for a fluid property that is a function of two independent variables.
 * The parameters to the macro are same as the template arguments for the @see BiVariatePropertyTest.
 * The variadic arguments is expected to be a tuple of double values of the form,
 * (first_independent_value, second_independent value, expected_value)
 *
 */
#define DEFINE_BIVARIATE_PROPERTY_TESTS(                                                                               \
    FluidName,                                                                                                         \
    PropertyName,                                                                                                      \
    TestName,                                                                                                          \
    FluidType,                                                                                                         \
    FirstPropertyUnit,                                                                                                 \
    SecondPropertyUnit,                                                                                                \
    PropertyUnit,                                                                                                      \
    PropertyMethod,                                                                                                    \
    SignificantFigures,                                                                                                \
    ...                                                                                                                \
)                                                                                                                      \
    using PropertyName = BiVariatePropertyTest<                                                                        \
        FluidType,                                                                                                     \
        FirstPropertyUnit,                                                                                             \
        SecondPropertyUnit,                                                                                            \
        PropertyUnit,                                                                                                  \
        PropertyMethod,                                                                                                \
        SignificantFigures>;                                                                                           \
    TEST_P(PropertyName, TestName)                                                                                     \
    {                                                                                                                  \
        this->TestPropertyCalculation();                                                                               \
    }                                                                                                                  \
    INSTANTIATE_TEST_SUITE_P(FluidName, PropertyName, ::testing::Values(__VA_ARGS__));
} // namespace fluid_props::testing
