#include "matchers.hpp"
#include "name_generators.hpp"
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <units/units.hpp>

using ::testing::DoubleEq;
using ::testing::Test;
using ::testing::Types;

namespace fluid_props::testing
{
    // clang-format off
    struct PressureTag {};
    struct TemperatureTag {};
    struct SpecificEnergyTag {};
    struct SpecificEntropyTag {};
    struct SpecificVolumeTag {};
    struct DensityTag {};
    struct VelocityTag {};
    struct ThermalExpansionTag {};
    struct CompressibilityTag {};
    struct ThermalConductivityTag {};
    struct SurfaceTensionTag {};
    struct ViscosityTag {};
    struct QualityTag {};

    // clang-format on

    using ConversionUnits = Types<
        // Pressures
        std::tuple<Pascal, Pascal, PressureTag>,
        std::tuple<Pascal, KiloPascal, PressureTag>,
        std::tuple<Pascal, MegaPascal, PressureTag>,
        std::tuple<Pascal, PSI, PressureTag>,
        //
        std::tuple<KiloPascal, Pascal, PressureTag>,
        std::tuple<KiloPascal, KiloPascal, PressureTag>,
        std::tuple<KiloPascal, MegaPascal, PressureTag>,
        std::tuple<KiloPascal, PSI, PressureTag>,
        //
        std::tuple<MegaPascal, Pascal, PressureTag>,
        std::tuple<MegaPascal, KiloPascal, PressureTag>,
        std::tuple<MegaPascal, MegaPascal, PressureTag>,
        std::tuple<MegaPascal, PSI, PressureTag>,
        //
        std::tuple<PSI, Pascal, PressureTag>,
        std::tuple<PSI, KiloPascal, PressureTag>,
        std::tuple<PSI, MegaPascal, PressureTag>,
        std::tuple<PSI, PSI, PressureTag>,
        // Temperatures
        std::tuple<Kelvin, Kelvin, TemperatureTag>,
        std::tuple<Kelvin, Celsius, TemperatureTag>,
        std::tuple<Kelvin, Fahrenheit, TemperatureTag>,
        std::tuple<Kelvin, Rankine, TemperatureTag>,
        //
        std::tuple<Celsius, Kelvin, TemperatureTag>,
        std::tuple<Celsius, Celsius, TemperatureTag>,
        std::tuple<Celsius, Fahrenheit, TemperatureTag>,
        std::tuple<Celsius, Rankine, TemperatureTag>,
        //
        std::tuple<Fahrenheit, Kelvin, TemperatureTag>,
        std::tuple<Fahrenheit, Celsius, TemperatureTag>,
        std::tuple<Fahrenheit, Fahrenheit, TemperatureTag>,
        std::tuple<Fahrenheit, Rankine, TemperatureTag>,
        //
        std::tuple<Rankine, Kelvin, TemperatureTag>,
        std::tuple<Rankine, Celsius, TemperatureTag>,
        std::tuple<Rankine, Fahrenheit, TemperatureTag>,
        std::tuple<Rankine, Rankine, TemperatureTag>,
        // Specific Energies
        std::tuple<KiloJoule_KiloGram, BTU_PoundMass, SpecificEnergyTag>,
        std::tuple<BTU_PoundMass, KiloJoule_KiloGram, SpecificEnergyTag>,
        // Specific Entropies
        std::tuple<KiloJoule_KiloGram_Kelvin, BTU_PoundMass_Rankine, SpecificEntropyTag>,
        std::tuple<BTU_PoundMass_Rankine, KiloJoule_KiloGram_Kelvin, SpecificEntropyTag>,
        // Specific Volumes
        std::tuple<CubicMeter_KiloGram, CubicFoot_PoundMass, SpecificVolumeTag>,
        std::tuple<CubicFoot_PoundMass, CubicMeter_KiloGram, SpecificVolumeTag>,
        // Densities
        std::tuple<KiloGram_CubicMeter, PoundMass_CubicFoot, DensityTag>,
        std::tuple<PoundMass_CubicFoot, KiloGram_CubicMeter, DensityTag>,
        // Velocities
        std::tuple<Meter_Second, Feet_Second, VelocityTag>,
        std::tuple<Feet_Second, Meter_Second, VelocityTag>,
        // Thermal Expansions
        std::tuple<IKelvin, IRankine, ThermalExpansionTag>,
        std::tuple<IRankine, IKelvin, ThermalExpansionTag>,
        // Compressibilities
        std::tuple<IPascal, IPascal, CompressibilityTag>,
        std::tuple<IPascal, IKiloPascal, CompressibilityTag>,
        std::tuple<IPascal, IMegaPascal, CompressibilityTag>,
        std::tuple<IPascal, IPSI, CompressibilityTag>,
        //
        std::tuple<IKiloPascal, IPascal, CompressibilityTag>,
        std::tuple<IKiloPascal, IKiloPascal, CompressibilityTag>,
        std::tuple<IKiloPascal, IMegaPascal, CompressibilityTag>,
        std::tuple<IKiloPascal, IPSI, CompressibilityTag>,
        //
        std::tuple<IMegaPascal, IPascal, CompressibilityTag>,
        std::tuple<IMegaPascal, IKiloPascal, CompressibilityTag>,
        std::tuple<IMegaPascal, IMegaPascal, CompressibilityTag>,
        std::tuple<IMegaPascal, IPSI, CompressibilityTag>,
        //
        std::tuple<IPSI, IPascal, CompressibilityTag>,
        std::tuple<IPSI, IKiloPascal, CompressibilityTag>,
        std::tuple<IPSI, IMegaPascal, CompressibilityTag>,
        std::tuple<IPSI, IPSI, CompressibilityTag>,
        // Thermal Conductivities
        std::tuple<Watts_Meter_Kelvin, BTU_Hour_Foot_Rankine, ThermalConductivityTag>,
        std::tuple<BTU_Hour_Foot_Rankine, Watts_Meter_Kelvin, ThermalConductivityTag>,
        // Surface Tensions
        std::tuple<Newton_Meter, PoundForce_Foot, SurfaceTensionTag>,
        std::tuple<PoundForce_Foot, Newton_Meter, SurfaceTensionTag>,
        // Viscosities
        std::tuple<PascalSecond, PoundMass_Foot_Hour, ViscosityTag>,
        std::tuple<PoundMass_Foot_Hour, PascalSecond, ViscosityTag>,
        // Quality
        std::tuple<Dimensionless, Percent, QualityTag>,
        std::tuple<Percent, Dimensionless, QualityTag>>;

    template <typename FromUnit, typename ToUnit, typename Tag> class PropertyConversionTest : public Test
    {
    public:
        void TestConversion()
        {
            auto expected = GetInitialValue<FromUnit>(Tag{});
            auto converted = expected.As(ToUnit{});

            EXPECT_THAT(converted.As(FromUnit{}), PropertyTEq(expected, 9));
        }

    private:
        template <typename T> auto GetInitialValue(PressureTag) { return PressureT<T>(1.0); }
        template <typename T> auto GetInitialValue(TemperatureTag) { return TemperatureT<T>(373.15); }
        template <typename T> auto GetInitialValue(SpecificEnergyTag) { return SpecificEnergyT<T>(1.0); }
        template <typename T> auto GetInitialValue(SpecificEntropyTag) { return SpecificEntropyT<T>(1.0); }
        template <typename T> auto GetInitialValue(SpecificVolumeTag) { return SpecificVolumeT<T>(1.0); }
        template <typename T> auto GetInitialValue(DensityTag) { return DensityT<T>(1000.0); }
        template <typename T> auto GetInitialValue(VelocityTag) { return VelocityT<T>(1.0); }
        template <typename T> auto GetInitialValue(ThermalExpansionTag) { return ThermalExpansionT<T>(1.0); }
        template <typename T> auto GetInitialValue(CompressibilityTag) { return CompressibilityT<T>(1.0); }
        template <typename T> auto GetInitialValue(ThermalConductivityTag) { return ThermalConductivityT<T>(1.0); }
        template <typename T> auto GetInitialValue(SurfaceTensionTag) { return SurfaceTensionT<T>(1.0); }
        template <typename T> auto GetInitialValue(ViscosityTag) { return ViscosityT<T>(1.0); }
        template <typename T> auto GetInitialValue(QualityTag) { return QualityT<T>(1.0); }
    };

    template <typename T>
    using TestPropertyConversion = PropertyConversionTest<
        typename std::tuple_element<0, T>::type,
        typename std::tuple_element<1, T>::type,
        typename std::tuple_element<2, T>::type>;

    TYPED_TEST_SUITE_P(TestPropertyConversion);

    TYPED_TEST_P(TestPropertyConversion, IsCorrect)
    {
        this->TestConversion();
    }

    REGISTER_TYPED_TEST_SUITE_P(TestPropertyConversion, IsCorrect);

    INSTANTIATE_TYPED_TEST_SUITE_P(
        UnitConversion, TestPropertyConversion, ConversionUnits, PropertyConversionNameGenerator
    );
} // namespace fluid_props::testing
