#include "matchers.hpp"
#include <fmt/core.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <units/units.hpp>

using ::testing::DoubleEq;
using ::testing::Eq;
using ::testing::Test;

namespace fluid_props::testing
{

    class PropertyTest : public Test
    {
    protected:
        void SetUp() override
        {
            this->property = PressureT<Pascal>{this->propertyValue};
            this->other = PressureT<Pascal>{this->otherPropertyValue};
        }

    public:
        const double propertyValue = 100000.0;
        const double otherPropertyValue = 200000.0;
        PressureT<Pascal> property;
        PressureT<Pascal> other;
    };

    TEST_F(PropertyTest, CanGetValue)
    {

        ASSERT_THAT(this->property.Value(), DoubleEq(this->propertyValue));
    }

    TEST_F(PropertyTest, CanSetValue)
    {
        auto newPressure = 1.0;
        this->property.Value(newPressure);

        ASSERT_THAT(this->property.Value(), DoubleEq(newPressure));
    }

    TEST_F(PropertyTest, CanBeAString)
    {
        std::string expected = fmt::format("{:0.4g} Pa", this->propertyValue);

        auto result = std::string(this->property);

        ASSERT_THAT(result, Eq(expected));
    }

    TEST_F(PropertyTest, CanBeEqual)
    {
        PressureT<Pascal> other{this->propertyValue};

        ASSERT_TRUE(this->property.AreEqual(other));
    }

    TEST_F(PropertyTest, CanNotBeEqual)
    {
        PressureT<Pascal> other{1.0};

        ASSERT_FALSE(this->property.AreEqual(other));
    }

    TEST_F(PropertyTest, CanBeEqualViaOperator)
    {
        PressureT<Pascal> other{this->propertyValue};

        ASSERT_TRUE(this->property == other);
    }

    TEST_F(PropertyTest, CanNotBeEqualViaOperator)
    {
        PressureT<Pascal> other{1.0};

        ASSERT_FALSE(this->property == other);
    }

    TEST_F(PropertyTest, NonEqualOperatorFalseCondition)
    {
        PressureT<Pascal> other{this->propertyValue};

        ASSERT_FALSE(this->property != other);
    }

    TEST_F(PropertyTest, NonEqualOperatorTrueCondition)
    {
        PressureT<Pascal> other{1.0};

        ASSERT_TRUE(this->property != other);
    }

    TEST_F(PropertyTest, LessThanOperatorTrueCondition)
    {
        PressureT<KiloPascal> other{200.0};

        ASSERT_TRUE(this->property < other);
    }

    TEST_F(PropertyTest, LessThanOperatorFalseCondition)
    {
        PressureT<KiloPascal> other{0.9};

        ASSERT_FALSE(this->property < other);
    }

    TEST_F(PropertyTest, GreaterThanOperatorTrueCondition)
    {
        PressureT<KiloPascal> other{90.0};

        ASSERT_TRUE(this->property > other);
    }

    TEST_F(PropertyTest, GreaterThanOperatorFalseCondition)
    {
        PressureT<KiloPascal> other{200.0};

        ASSERT_FALSE(this->property > other);
    }

    TEST_F(PropertyTest, CanAddTwoPropertyT)
    {

        PressureT<Pascal> expected{this->propertyValue + this->otherPropertyValue};

        EXPECT_THAT(this->property + this->other, PropertyTEq(expected, 9));
    }

    TEST_F(PropertyTest, CanAddDoubleToPropertyT)
    {
        PressureT<Pascal> expected{this->propertyValue + this->otherPropertyValue};

        EXPECT_THAT(this->property + this->otherPropertyValue, PropertyTEq(expected, 9));
        EXPECT_THAT(this->otherPropertyValue + this->property, PropertyTEq(expected, 9));
    }

    TEST_F(PropertyTest, CanSubtractTwoPropertyT)
    {

        PressureT<Pascal> expected{this->propertyValue - this->otherPropertyValue};

        EXPECT_THAT(this->property - this->other, PropertyTEq(expected, 9));
    }

    TEST_F(PropertyTest, CanSubtractDoubleFromPropertyT)
    {
        PressureT<Pascal> expected{this->propertyValue - this->otherPropertyValue};

        EXPECT_THAT(this->property - this->otherPropertyValue, PropertyTEq(expected, 9));
    }

    TEST_F(PropertyTest, CanSubtractPropertyTFromDouble)
    {
        PressureT<Pascal> expected{this->otherPropertyValue - this->propertyValue};

        EXPECT_THAT(this->otherPropertyValue - this->property, PropertyTEq(expected, 9));
    }

    TEST_F(PropertyTest, CanMultiplyTwoPropertyT)
    {

        auto expected = this->propertyValue * this->otherPropertyValue;

        ASSERT_THAT(this->property * this->other, DoubleEq(expected));
    }

    TEST_F(PropertyTest, CanMultiplyDoubleToPropertyT)
    {
        auto expected = this->propertyValue * this->otherPropertyValue;

        ASSERT_THAT(this->property * this->otherPropertyValue, DoubleEq(expected));
        ASSERT_THAT(this->otherPropertyValue * this->property, DoubleEq(expected));
    }

    TEST_F(PropertyTest, CanDivideTwoPropertyT)
    {

        auto expected = this->propertyValue / this->otherPropertyValue;

        ASSERT_THAT(this->property / this->other, DoubleEq(expected));
    }

    TEST_F(PropertyTest, CanDivideDoubleByPropertyT)
    {
        auto expected = this->propertyValue / this->otherPropertyValue;

        ASSERT_THAT(this->property / this->otherPropertyValue, DoubleEq(expected));
    }

    TEST_F(PropertyTest, CanDividePropertyTByDouble)
    {
        auto expected = this->otherPropertyValue / this->propertyValue;

        ASSERT_THAT(this->otherPropertyValue / this->property, DoubleEq(expected));
    }

    TEST(PropertyT, VaporAndLiquidPropertiesCanBeMixedFromVaporFraction)
    {
        constexpr auto vapor_fraction = QualityT<Dimensionless>{0.25};
        constexpr auto liquid_phase = SpecificEnergyT<KiloJoule_KiloGram>{1.0};
        constexpr auto vapor_phase = SpecificEnergyT<KiloJoule_KiloGram>{2.0};
        constexpr auto expected =
            SpecificEnergyT<KiloJoule_KiloGram>{vapor_fraction * vapor_phase + (1.0 - vapor_fraction) * liquid_phase};

        auto result = MixProperties(vapor_fraction, liquid_phase, vapor_phase);
        EXPECT_THAT(result, PropertyTEq(expected, 9));
    }

} // namespace fluid_props::testing
